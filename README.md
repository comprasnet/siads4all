![Alt text](siads4allreadme.png)

## Sobre

Prover o acesso ao cidadão à relatórios dos bens públicos de forma a promover maior transparência para o patrimônio dos entes. Assim viabilizando o monitoramento, fiscalização e controle por parte da sociedade, planejamento e gestão compartilhados, participação cívica e consciência patrimonial para a preservação e conservação desses bens.

Para o servidor facilitar a consulta de bens, movimentações, valores, etc. de patrimônio de seu ente, sobre sua responsabilidade, etc.