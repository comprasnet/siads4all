<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Auth Login Route
Route::post('/login', [\App\Http\Controllers\Api\Auth\AuthApiController::class, 'auth']);

Route::middleware(['auth:sanctum'])->group(function () {
    //Auth Routes
    Route::post('/logout', [\App\Http\Controllers\Api\Auth\AuthApiController::class, 'logout']);
    Route::get('/me', [\App\Http\Controllers\Api\Auth\AuthApiController::class, 'me']);

    //Patrimonio Siads Route
    Route::get('/siads/patrimonio',[\App\Http\Controllers\Api\PatrimonioController::class,'buscaSiadsPatrimonioComFiltro']);
});

