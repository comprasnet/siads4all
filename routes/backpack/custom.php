<?php

use App\Helper\Routes\Resource;
use App\Http\Controllers\Administracao\OrgaosController;
use App\Http\Controllers\Administracao\UnidadeController;
use App\Http\Controllers\MunicipioController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PatrimonyController;
use App\Http\Controllers\UnidadeController as BaseUnidadeController;
use App\Http\Controllers\UnitAccessRequestController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers',
], function () { // custom admin routes
    Route::get('dashboard', [DashboardController::class,'index']);
    Route::get('home', function (){
        return response()->redirectTo('/dashboard');
    });
    Route::crud('code', 'CodeCrudController');
    Route::crud('code/{code}/item', 'CodeItemCrudController');
    Route::crud('siads-patrimonio', 'SiadsPatrimonioCrudController');

    //Exports routes
    Route::get('export/siads/patrim', [\App\Http\Controllers\SiadsPatrimonioCrudController::class, 'exportSiadsPatrimOrgao']);

    //Route::resource('users', UserController::class);
    Route::get('users',[UserController::class,'index'])->name('users.index');
    Route::get('users/create',[UserController::class,'create'])->name('users.create');
    Route::get('users/{id}/edit-item',[UserController::class,'edit'])->name('users.edit');
    Route::post('users/{id}/delete',[UserController::class,'destroy'])->name('users.destroy');
    Route::post('users/create',[UserController::class,'store'])->name('users.create-save');

    Route::get('unit/by-user', [BaseUnidadeController::class,'getUnitsCurrentUser'])->name('request-permission.index');
    Route::get('unit/uorgs', [BaseUnidadeController::class,'getUorgByUnit'])->name('request-permission.index');
    Route::get('unit/approve', [UnitAccessRequestController::class,'index'])->name('request-permission.index');
    Route::get('unit/solicitar', [UnitAccessRequestController::class,'solicitar'])->name('request-permission.solicitar');
    Route::get('unit/load-modal-content', [UnitAccessRequestController::class,'modalPermission'])->name('load.modal.content');

    Route::get('unit/block', [UnitAccessRequestController::class,'blockUser'])->name('load.block.user');
    Route::get('unit/permission-by-roles', [UnitAccessRequestController::class,'getPermissionsByRole'])->name('request-permission.by-role');
    Route::post('unit/save-permission', [UnitAccessRequestController::class,'savePermission'])->name('request-permission.save');


    Route::get('/patrimony', [PatrimonyController::class,'index'])->name('patrimony.index');
    Route::post('/patrimony/list', [PatrimonyController::class,'datatable']);
    Route::post('/patrimony/export/{type}', [PatrimonyController::class,'export']);
    Route::post('/patrimony/schedule/export/{type}', [PatrimonyController::class,'exportSchedule']);
    Route::post('/patrimony/csv', [PatrimonyController::class,'csv']);
    Route::post('/patrimony/pdf', [PatrimonyController::class,'pdf']);
    Route::post('/patrimony/pdf-table', [PatrimonyController::class,'pdfTable']);

    Resource::resource('/administracao/orgaos',OrgaosController::class,'orgaos');
    Resource::resource('/administracao/unidades', UnidadeController::class,'unidades');
    Route::get('/administracao/orgaos/show/{id}', [OrgaosController::class,'show'])->name('orgaos.show');
    Route::get('/administracao/unidades/show/{id}', [UnidadeController::class,'show'])->name('unidades.show');

    Route::get('/municipios/get-municipios/{uf}', [MunicipioController::class, 'getMunicipios']);

    Route::get('/patrimony/conta-contabil', [PatrimonyController::class,'contasContabeis'])->name('patrimony.conta-contabil');
    Route::post('/patrimony/conta-contabil/sum', [PatrimonyController::class,'contasContabeisCalc'])->name('patrimony.conta-contabil');

    Route::post('notification/clear', [NotificationController::class,'clearNotifications'])->name('notification.clear');
    Route::get('notification/bell', [NotificationController::class,'bell'])->name('notification.bell');
    Route::get('notifications/unsent-push', [NotificationController::class, 'getUnsentPushNotifications']);

    Route::get('files',[FilesController::class,'index'])->name('files.index');
    Route::get('/files/remove/{file}',[FilesController::class,'remove'])->name('files.remove');
    Route::get('/files/cancela-agendamento/{file}',[FilesController::class,'cancela'])->name('files.remove');
    Route::get('/files/download/{file}',[FilesController::class,'download'])->name('files.remove');

    Route::get('/dasboard/edit', [DashboardController::class, 'editForm']);
    Route::post('/dasboard/update-order', [DashboardController::class, 'updateOrder']);
    Route::post('/dasboard/update-quick-access', [DashboardController::class, 'update']);
    Route::post('/dasboard/add-quick-access', [DashboardController::class, 'add']);
    Route::get('files/progress/{id}',[FilesController::class,'progress'])->name('files.index');
}); // this should be the absolute last line of this file
