<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MunicipioService;

class MunicipioController extends Controller
{
    protected $municipioService;

    public function __construct(MunicipioService $municipioService)
    {
        $this->municipioService = $municipioService;
    }

    public function getMunicipios($uf)
    {
        $municipios = $this->municipioService->getMunicipiosByUF($uf);
        return response()->json($municipios);
    }
}
