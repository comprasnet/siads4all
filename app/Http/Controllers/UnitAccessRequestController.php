<?php

namespace App\Http\Controllers;

use App\DataTables\UnitAccessMediatorDataTable;
use App\Exceptions\ExceptionWithUserMensage;
use App\Models\Role;
use App\Models\Unidade;
use App\Models\UnitAccessRequest;
use App\Models\User;
use App\Repositories\UnidadesRepository;
use App\Services\NotificationService;
use App\Services\PermissionsService;
use App\Services\RolesService;
use App\Services\UnidadesService;
use App\Services\UnitAccessRequestService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;


class UnitAccessRequestController extends Controller
{
    public function index(UnitAccessMediatorDataTable $dataTable)
    {
        return $dataTable->render('unit-access-mediator.index');
    }


    public function solicitar(
        UnitAccessRequestService $unitAccessRequestService,
        Request $request
    ) {

        $cod = $request->input('cod');

        try {
            $jsonResponse = $unitAccessRequestService->request($cod, auth()->user());
            return $jsonResponse->getMessageResonseJson();
        } catch (ExceptionWithUserMensage $e) {
            return $e->toJsonResponse();
        }
    }


    public function modalPermission(Request $request)
    {
        $id = $request->input('id');
        $groups = Role::all();
        $permissions = Permission::all();

        return view('unit-access-mediator.modal', compact('permissions', 'groups', 'id'));
    }

    public function getPermissionsByRole(Request $request, PermissionsService $permissionsService)
    {
        $roles = $request->input('roles');
        if (!empty($roles)) {
            return response()->json($permissionsService->getPermissionByRole($roles));
        }
        return response()->json([]);
    }

    public function savePermission(
        Request $request,
        UnitAccessRequestService $unitAccessRequestService
    ) {
        try {
            $unitAccessRequest = $request->input('unit_access');
            $permissions = $request->input('permissions');
            $roles = $request->input('roules');

            $jsonResponse = $unitAccessRequestService->approve(
                $unitAccessRequest,
                $permissions,
                $roles
            );

            return $jsonResponse->getMessageResonseJson();
        } catch (ExceptionWithUserMensage $e) {
            return $e->toJsonResponse();
        }
    }

    public function blockUser(
        Request $request,
        UnitAccessRequestService $unitAccessRequest
    ) {
        try {
            $accessRequestId = $request->input('id');
            $jsonResponse = $unitAccessRequest->block($accessRequestId);
            return $jsonResponse->getMessageResonseJson();
        } catch (ExceptionWithUserMensage $e) {
            return $e->toJsonResponse();
        }
    }

}
