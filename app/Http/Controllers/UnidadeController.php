<?php

namespace App\Http\Controllers;

use App\Models\Unidade;
use App\Repositories\UnidadesRepository;
use App\Services\Datalake;
use App\Services\UnidadeChangerService;
use App\Services\UnidadesService;

class UnidadeController extends Controller
{

    public function setDefault()
    {
        /** @var UnidadeChangerService $unidadeService * */
        $unidadeService = app(UnidadeChangerService::class);
        $unidade = request()->input('unidade');
        $unidadeService->setCurrentUnidade($unidade);
        return redirect('/');
    }

    public function getUnitsCurrentUser(UnidadesService $service)
    {
        $search = request()->input('search');
        $unidade = $service->getUnidadesByCurrentUserSelect2(
            $search
        );
        return response()->json($unidade);
    }

    public function getUorgByUnit(Datalake $datalake, UnidadesRepository $unidadesRepository)
    {
        $unidades = request()->get('ug');
        if(empty($unidades) or is_null($unidades)){
            $unidades = [];
        }
        if (!is_array($unidades)) {
            $unidades = [$unidades];
        }
        $uorgs = $datalake->getUorgsByUgsSelect2(
            $unidadesRepository->getCodesByUnitIds($unidades)
        );
        return response()->json($uorgs);
    }
}
