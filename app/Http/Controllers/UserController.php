<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests\CreateUserRequest;
use App\Models\Role;
use App\Services\UnidadesService;
use App\Services\User\UserUnitsSave;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class UserController
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(
        UsersDataTable $dataTable,
        UnidadesService $UnidadeService
    )
    {
        $roles = Role::all();
        return $dataTable->render('users.index',
            compact('roles')
        );
    }

    public function create(
        UnidadesService $UnidadeService
    )
    {
        $unidadesCadastradas = [];//$UnidadeService->getUnitsWithRolesAndPermissions(auth()->id());
        $groups = Role::all();
        $permissions = Permission::all();
        $unidades = [];
        return view('users.form', compact(
            'unidadesCadastradas',
            'groups',
            'permissions',
            'unidades'
        ));
    }

    public function store(CreateUserRequest $request, UserUnitsSave $userUnitsSave)
    {
        DB::beginTransaction();
        try {
            $id = $request->input('id');

            $unidades = $request->input("unidades");
            $userData = $request->only(['name', 'email', 'cpf', 'super_admin', 'matricula_siape','default_unit_admin','unit-default']);

            $user = $this->userService->createOrUpdateUser($userData, $id);
            if (!is_null($unidades) && !empty($unidades) && !$user->super_admin) {
                $userUnitsSave->setUser($user);
                $userUnitsSave->setUnits(json_decode($unidades, true));
                $userUnitsSave->save();
            }

            DB::commit();
            return redirect()->route('users.index')->with('success', 'Usuário criado com sucesso.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors(['msg' => 'Erro ao cadastrar usuário ' . $e->getMessage()]);
        }

    }

    public function show($id)
    {
        $user = $this->userService->getUserById($id);
        return view('users.form', compact('user'));
    }

    public function edit($id, UnidadesService $UnidadeService)
    {
        $user = $this->userService->getUserById($id);
        $unidadesCadastradas = $UnidadeService->getUnitsWithRolesAndPermissions($id);
        $groups = Role::all();
        $permissions = Permission::all();
        $unidades = [];
        return view('users.form', compact(
            'unidadesCadastradas',
            'groups',
            'permissions',
            'unidades',
            'user'
        ));
    }

    public function update(Request $request, $id)
    {
        $userData = $request->only(['name', 'email', 'cpf', 'situation', 'uasg_id', 'group_id']);
        $user = $this->userService->updateUser($id, $userData);

        // Atualize as permissões do usuário
        $user->permissions()->sync($request->input('permission_ids', []));

        return redirect()->route('users.index')->with('success', 'Usuário atualizado com sucesso.');
    }

    public function destroy($id)
    {
        $this->userService->deleteUser($id);
        return redirect()->route('users.index')->with('success', 'Usuário excluído com sucesso.');
    }
}
