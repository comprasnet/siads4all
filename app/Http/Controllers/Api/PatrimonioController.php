<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SiadsExtPatrim;
use App\Models\SiadsExtPatrimApi;
use Illuminate\Http\Request;

class PatrimonioController extends Controller
{
    public function buscaSiadsPatrimonioComFiltro(Request $request)
    {
        $dados = new SiadsExtPatrimApi();

        return response()->json($dados->getPatrimonioApiComFiltro($request));
    }
}
