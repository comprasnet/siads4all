<?php

namespace App\Http\Controllers;

use App\DataTables\UnitAccessMediatorDataTable;
use App\Models\QuickAccess;
use App\Repositories\QuickAccessRepository;
use App\Services\QuickAccessService;
use App\Services\UnidadeChangerService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(
        UnidadeChangerService $unidadeChangerService,
        UnitAccessMediatorDataTable $unitAccessMediatorDataTable,
        QuickAccessRepository $quickAccessRepository,
        QuickAccessService $quickAccessService
    )
    {
        $quickAccesses = $quickAccessRepository->getAllOrderedByOrder();
        $current = $unidadeChangerService->getCurrentUnidade();
        $countUnitAccessRequest = $unitAccessMediatorDataTable->count();
        return view('dashboard',[
            'currentUnit'=>$current,
            'countUnitAccessRequest'=>$countUnitAccessRequest,
            'quickAccesses'=>$quickAccesses,
            'quickAccessService'=> $quickAccessService
        ]);
    }

    public function editForm(
        UnidadeChangerService $unidadeChangerService,
        UnitAccessMediatorDataTable $unitAccessMediatorDataTable,
        QuickAccessRepository $quickAccessRepository
    )
    {
        $quickAccesses = $quickAccessRepository->getAllOrderedByOrder();
        $current = $unidadeChangerService->getCurrentUnidade();
        $countUnitAccessRequest = $unitAccessMediatorDataTable->count();
        return view('dashboard.edit',[
            'currentUnit'=>$current,
            'countUnitAccessRequest'=>$countUnitAccessRequest,
            'quickAccesses'=>$quickAccesses
        ]);
    }



    public function updateOrder(Request $request)
    {
        $order = $request->input('order');
        foreach ($order as $index => $id) {
            QuickAccess::where('id', $id)->update(['order' => $index + 1]);
        }
        return response()->json(['success' => true]);
    }

    public function update(Request $request)
    {
        $data = $request->only(['id', 'title', 'subtitle', 'icon', 'url']);
        QuickAccess::where('id', $data['id'])->update($data);
        return response()->json(['success' => true]);
    }

    public function add(Request $request)
    {
        $data = $request->only(['title', 'subtitle', 'icon', 'url']);
        $quickAccess = QuickAccess::create($data);
        return response()->json($quickAccess);
    }
}
