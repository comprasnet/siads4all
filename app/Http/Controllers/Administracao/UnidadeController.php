<?php

namespace App\Http\Controllers\Administracao;

use App\DataTables\UnidadeDataTable;
use App\Http\Requests\UnidadeRequest;
use App\Http\Controllers\Controller;
use App\Models\Municipio;
use App\Repositories\MunicipioRepository;
use App\Repositories\OrgaoRepository;
use App\Services\UnidadesService;
use App\Models\Unidade;
use App\Services\EstadoService;
use App\Services\UnidadeServiceData;

class UnidadeController extends Controller
{
    protected $unidadeService;

    public function __construct(UnidadesService $unidadeService)
    {
        $this->unidadeService = $unidadeService;
    }

    public function index(UnidadeDataTable $dataTable)
    {
        return $dataTable->render('administracao.unidades.index',);
    }

    public function show(Unidade $id,MunicipioRepository $municipioRepository)
    {
        $unidade = $id;
        $uf = '';
        $municipio = '';
        if($unidade->municipio_id){
            $municipioModel = $municipioRepository->getMunicipio($unidade->municipio_id);
            if($municipioModel) {
                $uf = $municipioModel->estado->sigla;
                $municipio = $municipioModel->nome;
            }
        }
        return view('administracao.unidades.show', compact('unidade','uf','municipio'));
    }

    public function create(EstadoService $estadoService)
    {
        $unidade = new Unidade();
        $estadoId = '';
        return $this->former(compact(
            'unidade',
            'estadoId'
        ));
    }



    public function store(UnidadeRequest $request)
    {

        $this->unidadeService->createUnidade($request->validated());
        return redirect()->route('unidades.index');
    }

    public function edit(int $id)
    {
        /** @var Unidade $unidade */
        $unidade = Unidade::find($id);

        $estadoId = "";
        if((int)$unidade->municipio_id > 0){
            /** @var Municipio $municipio */
            $municipio = Municipio::find($unidade->municipio_id);
            if($municipio){
                $estadoId = $municipio->estado_id;
            }
        }
        return $this->former(compact('unidade','estadoId'));
    }

    public function update(UnidadeRequest $request, Unidade $id)
    {
        $this->unidadeService->updateUnidade($id, $request->validated());
        return redirect()->route('unidades.index');
    }

    public function destroy(Unidade $id)
    {
        $this->unidadeService->deleteUnidade($id);;
    }

    private function former($data = [])
    {
        $estadoService = app(EstadoService::class);
        $orgaos = app(OrgaoRepository::class);
        $unidadeServiceData = new UnidadeServiceData();

        $data['administracoes'] = $unidadeServiceData->getAdministracaoOptions();
        $data['poderes'] = $unidadeServiceData->getPoderOptions();
        $data['esferas'] = $unidadeServiceData->getEsferaOptions();
        $data['tipos'] = $unidadeServiceData->getTipoOptions();
        $data['estados'] = $estadoService->getAll();
        $data['orgaos'] = $orgaos->all();
        return view('administracao.unidades.form', $data);
    }
}

