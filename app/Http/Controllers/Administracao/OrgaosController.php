<?php

namespace App\Http\Controllers\Administracao;

use App\DataTables\OrgaosDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrgaoRequest;
use App\Models\Orgao;
use App\Models\OrgaosSuperior;
use App\Repositories\OrgaoSuperiorRepository;
use App\Services\OrgaoService;
use App\Traits\Mask;
use Illuminate\Http\Request;

class OrgaosController extends Controller
{
    use Mask;

    public function __construct(private OrgaoService $service)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index(
        OrgaosDataTable $dataTable
    )
    {
        return $dataTable->render('administracao.orgaos.index');
    }

    public function show(Orgao $id)
    {
        $orgao = $id;
        $orgaoSuperior = new OrgaosSuperior();
        if($orgao->orgaosuperior_id){
            $orgaoSuperior = OrgaosSuperior::find($orgao->orgaosuperior_id);
        }
        $orgao->cnpj = $this->maskCNPJ($orgao->cnpj);
        return view('administracao.orgaos.show', compact('orgao','orgaoSuperior'));
    }

    public function create(OrgaoSuperiorRepository $orgaoSuperiorRepository)
    {
        $orgaosSuperior = $orgaoSuperiorRepository->getAll();
        return view('administracao.orgaos.create', compact('orgaosSuperior'));
    }

    public function edit($id,OrgaoSuperiorRepository $orgaoSuperiorRepository)
    {
        $orgao = $this->service->find($id);
        $orgaosSuperior = $orgaoSuperiorRepository->getAll();
        return view('administracao.orgaos.edit', compact('orgao','orgaosSuperior'));
    }

    public function store(OrgaoRequest $request)
    {

        // Se a validação for bem-sucedida, cria o órgão
        $this->service->create($request->all());

        // Redireciona para a listagem com mensagem de sucesso
        return redirect()->route('orgaos.index')->with('success', 'Órgão criado com sucesso!');
    }

    public function update(OrgaoRequest $request, $id)
    {

        // Se a validação for bem-sucedida, atualiza o órgão
        $this->service->update($id, $request->all());

        // Redireciona para a listagem com mensagem de sucesso
        return redirect()->route('orgaos.index')->with('success', 'Órgão atualizado com sucesso!');
    }

    public function destroy($id)
    {
        $this->service->delete($id);
        return response()->json(null, 204);
    }
}
