<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use App\Repositories\NotificationRepository;
use App\Services\NotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Cria uma notificação para o grupo de administradores e retorna um JSON.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createNotificationForAdmins(Request $request)
    {
        $message = $request->input('message');
        $admins = User::where('role', 'admin')->get();

        $this->notificationService->createNotification($message, $admins);

        return response()->json(['message' => 'Notificação criada para administradores com sucesso']);
    }

    public function clearNotifications(NotificationService $notificationService)
    {
        $notificationService->setReadAllNotification(auth()->user());
    }

    public function getUnsentPushNotifications(NotificationRepository $notificationRepository)
    {
        $notifications = $notificationRepository->getUnsentPushNotificationsForCurrentUser();
        if (count($notifications) > 0) {
            $notificationRepository->markPushSentForNotificationId($notifications[0]->id);
            $notification = Notification::find($notifications[0]->notification_id);
            return response()->json(['notification' => $notification]);
        }

        return response()->json(['notification' => false]);
    }

    public function bell()
    {
        return view('gov-theme.inc.bell-notify');
    }
}
