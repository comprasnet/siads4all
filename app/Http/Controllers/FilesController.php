<?php

namespace App\Http\Controllers;

use App\DataTables\FileDataTable;
use App\Models\File;
use App\Models\ScheduledReport;
use App\Services\FilesService;

class FilesController extends Controller
{

    public function index(FileDataTable $dataTable)
    {
        return $dataTable->render('files.index');
    }

    public function progress($id, FilesService $filesService)
    {
        /** @var File $file * */
        $file = File::find($id);
        return response()->json([
            'pct' => $filesService->getPercentageProgressFile($id),
            'complete' => $file->complete
        ]);
    }

    public function remove(File $file)
    {
        if (file_exists($file->base_path)) {
            unlink($file->base_path);
        }
        $file->delete();
        return redirect()->back();
    }


    public function cancela(File $file)
    {
        if (file_exists($file->base_path)) {
            unlink($file->base_path);
        }
        ScheduledReport::query()
            ->where('file_id',$file->id)
            ->delete();

        $file->delete();
        return redirect()->back();
    }

    public function download(File $file)
    {
        $fileName = basename($file->file_location);
        $fileContents = file_get_contents($file->file_location);

        if(!is_dir(storage_path('app/temp/'))){
            mkdir(storage_path('app/temp/'),0777, true);
        }

        $tempPath = storage_path('app/temp/' . $fileName);
        file_put_contents($tempPath, $fileContents);
        $mimeType = mime_content_type($tempPath);

        return response()->download($tempPath, $fileName, [
            'Content-Type' => $mimeType,
        ])->deleteFileAfterSend(true);
    }
}
