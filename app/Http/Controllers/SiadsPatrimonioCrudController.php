<?php

namespace App\Http\Controllers;

use App\Exports\SiadsExtPatrimExport;
use App\Services\UnidadeChangerService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SiadsPatrimonioCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        /** @var UnidadeChangerService $unidadeService * */
        $unidadeService = app(UnidadeChangerService::class);
        CRUD::setModel(\App\Models\SiadsExtPatrim::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/siads-patrimonio');
        CRUD::setEntityNameStrings('siads patrimonio', 'siads patrimonios');
        CRUD::addClause('where', 'it_co_ug', '=', $unidadeService->getCurrentUnidade()->codigo);
        CRUD::enableExportButtons();

    }

    public function exportSiadsPatrimOrgao(Request $request)
    {
        return Excel::download(new SiadsExtPatrimExport($request), 'patrimonio_siads.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setOperationSetting('showEntryCount', false);
//        CRUD::setFromDb(); // set columns from db columns.
        CRUD::addColumns([
            [
                'name' => 'orgao',
                'label' => 'Órgão',
                'type' => 'string',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_orgao',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'unidade',
                'label' => 'Unidade',
                'type' => 'string',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_ug',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'gestao',
                'label' => 'Gestão',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'abbreviation',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'uorg',
                'label' => 'Uorg',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_uorg',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'conta_contabil',
                'label' => 'Conta contábil',
                'type' => 'string',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_conta_contabil',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'numero_patrimonial',
                'label' => 'Número patrimonial',
                'type' => 'string',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_nu_patrimonial',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'numero_patrimonial_antigo',
                'label' => 'Número patrimonial antigo',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_nu_patrimonial_ant',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'data_tombamento',
                'label' => 'Data tombamento',
                'type' => 'date',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_nu_patrimonial_ant',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'codigo_material',
                'label' => 'Código material',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_item_material',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_material',
                'label' => 'Nome material',
                'type' => 'textarea',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_material',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'descricao_complementar',
                'label' => 'Descrição complementar',
                'type' => 'textarea',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_tx_descricao_complementar',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'marca',
                'label' => 'Marca',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_marca',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'modelo',
                'label' => 'Modelo',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_modelo',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'serie',
                'label' => 'Núm. série',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_nu_serie',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'fabricante',
                'label' => 'Fabricante',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_fabricante',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'codigo_terceiro',
                'label' => 'Cód. terceiro',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_terceiros',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'valor_inicial_bem',
                'label' => 'Vlr. inicial bem (R$)',
                'type' => 'number',
//                'prefix'        => 'R$',
//                'suffix'        => ' EUR',
                'decimals' => 2,
                'dec_point' => ',',
                'thousands_sep' => '.',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_co_terceiros',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'valor_bem',
                'label' => 'Vlr. bem (R$)',
                'type' => 'number',
//                'prefix'        => 'R$',
//                'suffix'        => ' EUR',
                'decimals' => 2,
                'dec_point' => ',',
                'thousands_sep' => '.',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_co_terceiros',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'valor_depreciado',
                'label' => 'Vlr. depreciado (R$)',
                'type' => 'number',
//                'prefix'        => 'R$',
//                'suffix'        => ' EUR',
                'decimals' => 2,
                'dec_point' => ',',
                'thousands_sep' => '.',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_co_terceiros',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'valor_liquido',
                'label' => 'Vlr. liquido do bem (R$)',
                'type' => 'number',
//                'prefix'        => 'R$',
//                'suffix'        => ' EUR',
                'decimals' => 2,
                'dec_point' => ',',
                'thousands_sep' => '.',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_co_terceiros',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'valor_docao_venda',
                'label' => 'Vlr. doação/venda (R$)',
                'type' => 'number',
//                'prefix'        => 'R$',
//                'suffix'        => ' EUR',
                'decimals' => 2,
                'dec_point' => ',',
                'thousands_sep' => '.',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
//                'searchLogic' => function (Builder $query, $column, $searchTerm) {
//                    $query->orWhere(
//                        'it_co_terceiros',
//                        'iLike',
//                        '%' . $searchTerm . '%'
//                    );
//                }
            ],
            [
                'name' => 'codigo_tipo_bem',
                'label' => 'Cód. tipo bem',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_tipo_bem',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_tipo_bem',
                'label' => 'Nome tipo bem',
                'type' => 'strig',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_tipo_bem',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'codigo_situacao',
                'label' => 'Cód. situação',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_situacao',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_situacao',
                'label' => 'Nome situação',
                'type' => 'strig',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_situacao',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'codigo_destinacao',
                'label' => 'Cód. desticação',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_destinacao',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_destinacao',
                'label' => 'Nome destinação',
                'type' => 'strig',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_destinacao',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'cpf_responsavel',
                'label' => 'CPF responsável',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_nu_cpf_responsavel',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_responsavel',
                'label' => 'Nome responsável',
                'type' => 'textarea',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_responsavel',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'cpf_responsavel_uorg',
                'label' => 'CPF responsável UORG',
                'type' => 'string',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_co_resp_uorg',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
            [
                'name' => 'nome_responsavel_uorg',
                'label' => 'Nome responsável UROG',
                'type' => 'textarea',
                'visibleInTable' => false,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'searchLogic' => function (Builder $query, $column, $searchTerm) {
                    $query->orWhere(
                        'it_no_resp_uorg',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                }
            ],
        ]);

        $this->addCustomCrudFilters();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();

    }

    protected function addCustomCrudFilters()
    {
//        $this->crud->addFilter(
//            [ // add a "simple" filter called Draft
//                'type' => 'simple',
//                'name' => 'checkbox',
//                'label' => 'Simple',
//            ],
//            false, // the simple filter has no values, just the "Draft" label specified above
//            function () { // if the filter is active (the GET parameter "draft" exits)
//                $this->crud->addClause('where', 'checkbox', '1');
//            }
//        );
//
//        $this->crud->addFilter([ // dropdown filter
//            'name' => 'select_from_array',
//            'type' => 'dropdown',
//            'label' => 'Dropdown',
//        ], ['one' => 'One', 'two' => 'Two', 'three' => 'Three'], function ($value) {
//            // if the filter is active
//            $this->crud->addClause('where', 'select_from_array', $value);
//        });

        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'orgao',
                'label' => 'Órgão',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'it_co_orgao', 'LIKE', "%$value%");
            }
        );


        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'unidade',
                'label' => 'Unidade',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'it_co_ug', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'uorg',
                'label' => 'Uorg',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'it_co_uorg', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'numero_patrimonial',
                'label' => 'Número Patrimonial',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'it_nu_patrimonial', 'LIKE', "%$value%");
            }
        );


        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'conta_contabil',
                'label' => 'Conta Contábil',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'it_co_conta_contabil', 'LIKE', "%$value%");
            }
        );
    }
}
