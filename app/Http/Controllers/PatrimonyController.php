<?php

namespace App\Http\Controllers;

use App\Exceptions\ExceptionWithUserMensage;
use App\Helper\JsonResponse;
use App\Jobs\ReportPatrimony;
use App\Repositories\JobRepository;
use App\Services\Datalake;
use App\Services\FilesService;
use App\Services\ScheduledReportService;
use App\Services\SiadsPatrimFilter;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PatrimonyController extends Controller
{


    public function index(
        Datalake $datalake
    ) {
        $unidades = [];
        $situacaoList = $datalake->getSituacaoList();
        $destinacaoList = $datalake->getDestinacaoList();
        $uorgs = [];

        return view(
            'patrim.index',
            compact(
                'unidades',
                'situacaoList',
                'uorgs',
                'destinacaoList',
            )
        );
    }

    public function datatable(
        SiadsPatrimFilter $filter
    ) {
        return response()->json($filter->getFilteredData());
    }

    public function export(FilesService $filesService, $type)
    {
        $filesService->createReportPatrimonyFileServiceByDataRequest($type,auth()->id(), request()->all());
    }

    public function exportSchedule(ScheduledReportService $scheduledReportService, $type)
    {
        try {
            $data =  request()->all();
            $schedule = $scheduledReportService->scheduleReport(
                $data['filename'],
                $type,
                auth()->id(),
                $data,
                Carbon::parse($data['data_agendamento'])
            );
            $jsonResponse = new JsonResponse();
            return $jsonResponse->jsonSuccess(['schedule'=>$schedule,'date'=>Carbon::parse($data['data_agendamento'])->format('d/m/Y')]);
        }catch (ExceptionWithUserMensage $e){
            return $e->toJsonResponse();
        }

    }

    public function contasContabeis(
        Datalake $datalake,
        Request $request
    ) {
        $ugId = $request->input('ug');
        $search = $request->input('search');
        $contasList = $datalake->getContasContabilSelect2($search, $ugId);
        return response()->json($contasList);
    }

    public function contasContabeisCalc(
        Request $request,
        Datalake $datalake,
        SiadsPatrimFilter $siadsPatrimFilter
    )
    {
        $soma = $siadsPatrimFilter->sumValor($request->all());
        return (new JsonResponse)->responseDataSuccess(number_format((float)$soma, 2, ',', '.'));
    }
}
