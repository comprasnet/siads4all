<?php

namespace App\Http\Middleware;

use App\Services\UnidadesService;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        if(auth()->id !== null){
            $this->setUnidadesByUser();
        }
        return $request->expectsJson() ? null : route('login');
    }

    private function setUnidadesByUser(){
        /** @var UnidadesService $unidadeService **/
        $unidadeService = app(UnidadesService::class);
        view()->share('unidadesUsuario', []);
    }
}
