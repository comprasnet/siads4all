<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrgaoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $orgaoId = $this->route('id'); // Obtém o ID do órgão atual da rota

        return [
            'codigo' => [
                'required',
                Rule::unique('orgaos', 'codigo')
                    ->ignore($orgaoId)
                    ->whereNull('deleted_at'), // Ignora o atual em caso de update
            ],
            'nome' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'codigo.required' => 'O campo código é obrigatório.',
            'codigo.unique' => 'O código informado já está em uso.',
            'nome.required' => 'O campo nome é obrigatório.',
        ];
    }
}
