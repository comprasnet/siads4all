<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnidadeRequest extends FormRequest
{
    public function rules()
    {
        return [
            'orgao_id' => 'required|integer',
            'codigo' => 'required|string|max:255',
            'gestao' => 'required|string|max:5',
            'codigosiasg' => 'nullable|string|max:255',
            'nome' => 'required|string|max:255',
            'nomeresumido' => 'required|string|max:255',
            'telefone' => 'nullable|string|max:255',
            'tipo' => 'required|string|max:255',
            'situacao' => 'required|boolean',
            'sisg' => 'required|boolean',
            'municipio_id' => 'nullable|integer',
            'esfera' => 'nullable|string|max:255',
            'poder' => 'nullable|string|max:255',
            'tipo_adm' => 'nullable|string|max:255',
            'aderiu_siasg' => 'boolean',
            'utiliza_siafi' => 'boolean',
            'codigo_siorg' => 'nullable|string|max:255',
            'sigilo' => 'boolean',
            'despacho_autorizatorio' => 'boolean',
            'cnpj' => 'nullable|string|max:255',
            'utiliza_custos' => 'required|boolean',
            'codigosiafi' => 'nullable|string|max:255',
            'exclusivo_gestao_atas' => 'nullable|boolean',
        ];
    }

    public function messages()
    {
        return [
            'orgao_id.required' => 'O campo órgão é obrigatório.',
            'orgao_id.integer' => 'O campo órgão deve ser um número inteiro.',
            'codigo.required' => 'O campo código é obrigatório.',
            'codigo.string' => 'O campo código deve ser uma string.',
            'codigo.max' => 'O campo código não pode ter mais de 255 caracteres.',
            'gestao.required' => 'O campo gestão é obrigatório.',
            'gestao.string' => 'O campo gestão deve ser uma string.',
            'gestao.max' => 'O campo gestão não pode ter mais de 5 caracteres.',
            'codigosiasg.string' => 'O campo código SIASG deve ser uma string.',
            'codigosiasg.max' => 'O campo código SIASG não pode ter mais de 255 caracteres.',
            'nome.required' => 'O campo nome é obrigatório.',
            'nome.string' => 'O campo nome deve ser uma string.',
            'nome.max' => 'O campo nome não pode ter mais de 255 caracteres.',
            'nomeresumido.required' => 'O campo nome resumido é obrigatório.',
            'nomeresumido.string' => 'O campo nome resumido deve ser uma string.',
            'nomeresumido.max' => 'O campo nome resumido não pode ter mais de 255 caracteres.',
            'telefone.string' => 'O campo telefone deve ser uma string.',
            'telefone.max' => 'O campo telefone não pode ter mais de 255 caracteres.',
            'tipo.required' => 'O campo tipo é obrigatório.',
            'tipo.string' => 'O campo tipo deve ser uma string.',
            'tipo.max' => 'O campo tipo não pode ter mais de 255 caracteres.',
            'situacao.required' => 'O campo situação é obrigatório.',
            'situacao.boolean' => 'O campo situação deve ser verdadeiro ou falso.',
            'sisg.required' => 'O campo SISG é obrigatório.',
            'sisg.boolean' => 'O campo SISG deve ser verdadeiro ou falso.',
            'municipio_id.integer' => 'O campo município deve ser um número inteiro.',
            'esfera.string' => 'O campo esfera deve ser uma string.',
            'esfera.max' => 'O campo esfera não pode ter mais de 255 caracteres.',
            'poder.string' => 'O campo poder deve ser uma string.',
            'poder.max' => 'O campo poder não pode ter mais de 255 caracteres.',
            'tipo_adm.string' => 'O campo tipo administrativo deve ser uma string.',
            'tipo_adm.max' => 'O campo tipo administrativo não pode ter mais de 255 caracteres.',
            'aderiu_siasg.required' => 'O campo adesão ao SIASG é obrigatório.',
            'aderiu_siasg.boolean' => 'O campo adesão ao SIASG deve ser verdadeiro ou falso.',
            'utiliza_siafi.boolean' => 'O campo utilização do SIAFI deve ser verdadeiro ou falso.',
            'codigo_siorg.string' => 'O campo código SIORG deve ser uma string.',
            'codigo_siorg.max' => 'O campo código SIORG não pode ter mais de 255 caracteres.',
            'sigilo.required' => 'O campo sigilo é obrigatório.',
            'sigilo.boolean' => 'O campo sigilo deve ser verdadeiro ou falso.',
            'cnpj.string' => 'O campo CNPJ deve ser uma string.',
            'cnpj.max' => 'O campo CNPJ não pode ter mais de 255 caracteres.',
            'utiliza_custos.required' => 'O campo utilização de custos é obrigatório.',
            'utiliza_custos.boolean' => 'O campo utilização de custos deve ser verdadeiro ou falso.',
            'codigosiafi.string' => 'O campo código SIAFI deve ser uma string.',
            'codigosiafi.max' => 'O campo código SIAFI não pode ter mais de 255 caracteres.',
            'exclusivo_gestao_atas.boolean' => 'O campo exclusivo para gestão de atas deve ser verdadeiro ou falso.',
        ];
    }
}
