<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Municipio
 *
 * @property int $id
 * @property int $codigo_ibge
 * @property string $nome
 * @property string|null $latitude
 * @property string|null $longitude
 * @property bool $capital
 * @property int $estado_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Estado $estado
 *
 * @package App\Models
 */
class Municipio extends Model
{
    use SoftDeletes;

    protected $table = 'municipios';

    protected $casts = [
        'codigo_ibge' => 'int',
        'capital' => 'bool',
        'estado_id' => 'int'
    ];

    protected $fillable = [
        'codigo_ibge',
        'nome',
        'latitude',
        'longitude',
        'capital',
        'estado_id'
    ];

    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }
}
