<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estado
 *
 * @property int $id
 * @property string $sigla
 * @property string $nome
 * @property int|null $regiao_id
 * @property string|null $latitude
 * @property string|null $longitude
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Municipio[] $municipios
 *
 * @package App\Models
 */
class Estado extends Model
{
    use SoftDeletes;

    protected $table = 'estados';

    protected $casts = [
        'regiao_id' => 'int'
    ];

    protected $fillable = [
        'sigla',
        'nome',
        'regiao_id',
        'latitude',
        'longitude'
    ];

    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    }
}
