<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Unidadesuser
 *
 * @property int $user_id
 * @property int $unidade_id
 *
 * @property User $user
 * @property Unidade $unidade
 *
 * @package App\Models
 */
class Unidadesuser extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $table = 'unidadesusers';
    protected $fillable = [
        'user_id',
        'unidade_id'
    ];

    protected $casts = [
        'user_id' => 'int',
        'unidade_id' => 'int'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function unidade()
    {
        return $this->belongsTo(Unidade::class);
    }
}
