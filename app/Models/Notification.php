<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 *
 * @property int $id
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $link
 * @property bool $notification_read
 *
 * @property Collection|NotificationTo[] $notification_tos
 *
 * @package App\Models
 */
class Notification extends Model
{
	protected $table = 'notifications';

	protected $casts = [
		'notification_read' => 'bool'
	];

	protected $fillable = [
		'message',
		'link',
		'notification_read'
	];


    public function notification_to()
	{
		return $this->hasMany(NotificationTo::class);
	}
}
