<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 * 
 * @property int $id
 * @property string $file_location
 * @property string $filters_hash
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $user_id
 * @property float $porcentagem
 * @property int|null $queue_position
 * @property int|null $queue_start
 * @property string|null $job_id
 * @property bool $complete
 * @property int|null $total_register
 * @property int|null $current_register
 * @property string|null $base_path
 * @property Carbon|null $data_agendamento
 * 
 * @property User|null $user
 *
 * @package App\Models
 */
class File extends Model
{
	protected $table = 'files';

	protected $casts = [
		'user_id' => 'int',
		'porcentagem' => 'float',
		'queue_position' => 'int',
		'queue_start' => 'int',
		'complete' => 'bool',
		'total_register' => 'int',
		'current_register' => 'int',
		'data_agendamento' => 'datetime'
	];

	protected $fillable = [
		'file_location',
		'filters_hash',
		'user_id',
		'porcentagem',
		'queue_position',
		'queue_start',
		'job_id',
		'complete',
		'total_register',
		'current_register',
		'base_path',
		'data_agendamento'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
