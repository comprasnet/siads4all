<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ScheduledReport
 * 
 * @property int $id
 * @property string|null $name
 * @property string $type
 * @property int $user_id
 * @property string $data_request
 * @property Carbon $date
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $file_id
 * 
 * @property User $user
 *
 * @package App\Models
 */
class ScheduledReport extends Model
{
	protected $table = 'scheduled_reports';

	protected $casts = [
		'user_id' => 'int',
		'date' => 'datetime',
		'file_id' => 'int'
	];

	protected $fillable = [
		'name',
		'type',
		'user_id',
		'data_request',
		'date',
		'status',
		'file_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
