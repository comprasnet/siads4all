<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orgao
 *
 * @property int $id
 * @property int|null $orgaosuperior_id
 * @property string $codigo
 * @property string|null $codigosiasg
 * @property string $nome
 * @property bool|null $situacao
 * @property string|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $cnpj
 * @property string|null $codigo_siorg
 * @property string|null $gestao
 * @property bool $daas
 * @property string|null $utiliza_centro_custo
 * @property string|null $tipo_administracao
 * @property string|null $poder
 * @property string|null $esfera
 * @property string $id_sistema_origem
 *
 * @property OrgaosSuperior|null $orgaossuperiore
 * @property Collection|Unidade[] $unidades
 *
 * @package App\Models
 */
class Orgao extends Model
{
    use SoftDeletes;

    protected $table = 'orgaos';

    protected $casts = [
        'orgaosuperior_id' => 'int',
        'situacao' => 'bool',
        'daas' => 'bool'
    ];

    protected $fillable = [
        'orgaosuperior_id',
        'codigo',
        'codigosiasg',
        'nome',
        'situacao',
        'cnpj',
        'codigo_siorg',
        'gestao',
        'daas',
        'utiliza_centro_custo',
        'tipo_administracao',
        'poder',
        'esfera',
        'id_sistema_origem'
    ];

    public function orgaossuperiore()
    {
        return $this->belongsTo(OrgaosSuperior::class, 'orgaosuperior_id');
    }

    public function unidades()
    {
        return $this->hasMany(Unidade::class);
    }
}
