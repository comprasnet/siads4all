<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unidade
 *
 * @property int $id
 * @property int $orgao_id
 * @property string $codigo
 * @property string $gestao
 * @property string|null $codigosiasg
 * @property string $nome
 * @property string $nomeresumido
 * @property string|null $telefone
 * @property string $tipo
 * @property bool $situacao
 * @property string|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $sisg
 * @property int|null $municipio_id
 * @property string|null $esfera
 * @property string|null $poder
 * @property string|null $tipo_adm
 * @property bool $aderiu_siasg
 * @property bool $utiliza_siafi
 * @property string|null $codigo_siorg
 * @property bool $sigilo
 * @property string|null $cnpj
 * @property bool $utiliza_custos
 * @property string|null $codigosiafi
 * @property bool|null $exclusivo_gestao_atas
 *
 * @property Orgao $orgao
 *
 * @package App\Models
 */
class Unidade extends Model
{
    use SoftDeletes;

    protected $table = 'unidades';

    protected $casts = [
        'orgao_id' => 'int',
        'situacao' => 'bool',
        'sisg' => 'bool',
        'municipio_id' => 'int',
        'aderiu_siasg' => 'bool',
        'utiliza_siafi' => 'bool',
        'sigilo' => 'bool',
        'utiliza_custos' => 'bool',
        'exclusivo_gestao_atas' => 'bool'
    ];

    protected $fillable = [
        'orgao_id',
        'codigo',
        'gestao',
        'codigosiasg',
        'nome',
        'nomeresumido',
        'telefone',
        'tipo',
        'situacao',
        'sisg',
        'municipio_id',
        'esfera',
        'poder',
        'tipo_adm',
        'aderiu_siasg',
        'utiliza_siafi',
        'codigo_siorg',
        'sigilo',
        'cnpj',
        'utiliza_custos',
        'codigosiafi',
        'exclusivo_gestao_atas',
        'despacho_autorizatorio'
    ];

    public function orgao()
    {
        return $this->belongsTo(Orgao::class);
    }
}
