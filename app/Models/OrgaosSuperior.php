<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orgaossuperiore
 *
 * @property int $id
 * @property string $codigo
 * @property string $nome
 * @property bool $situacao
 * @property string|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Orgao[] $orgaos
 *
 * @package App\Models
 */
class OrgaosSuperior extends Model
{
	use SoftDeletes;
	protected $table = 'orgaossuperiores';

	protected $casts = [
		'situacao' => 'bool'
	];

	protected $fillable = [
		'codigo',
		'nome',
		'situacao'
	];

	public function orgaos()
	{
		return $this->hasMany(Orgao::class, 'orgaosuperior_id');
	}
}
