<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UnitAccessRequest
 * 
 * @property int $id
 * @property int $user_id
 * @property int $unit_id
 * @property string|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class UnitAccessRequest extends Model
{
	use SoftDeletes;
	protected $table = 'unit_access_requests';

	protected $casts = [
		'user_id' => 'int',
		'unit_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'unit_id'
	];
}
