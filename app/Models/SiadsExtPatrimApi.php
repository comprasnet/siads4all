<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiadsExtPatrimApi extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $connection = 'pgsql2';
    protected $table = 'siads_ext_patrim';
    protected $primaryKey = 'it_nu_patrimonial';
    protected $casts = [
        'it_co_orgao' => 'string',
        'it_co_ug' => 'string',
        'it_co_gestao' => 'string',
        'it_nu_patrimonial_ant' => 'integer',
        'it_nu_patrimonial' => 'integer',
        'it_no_marca' => 'string',
        'it_no_modelo' => 'string',
        'it_nu_serie' => 'string',
        'it_no_fabricante' => 'string',
        'it_nu_processo' => 'string',
        'it_co_terceiros' => 'string',
        'it_va_inicial_bem' => 'decimal:2',
        'it_va_bem' => 'decimal:2',
        'it_va_bem_depre' => 'decimal:2',
        'it_va_liq_bem' => 'decimal:2',
        'it_va_doacao_venda' => 'decimal:2',
        'it_co_uorg' => 'string',
        'it_co_item_material' => 'string',
        'it_co_conta_contabil' => 'integer',
        'it_co_tipo_bem' => 'string',
        'it_no_tipo_bem' => 'string',
        'it_co_situacao' => 'string',
        'it_no_situacao' => 'string',
        'it_tp_plaqueta' => 'string',
        'it_no_plaqueta' => 'string',
        'it_no_aquisicao' => 'string',
        'it_ed_endereco' => 'string',
        'it_no_garantidor' => 'string',
        'it_nu_contrato_garan_comp' => 'string',
        'it_nu_cpf_responsavel' => 'integer',
        'it_no_responsavel' => 'string',
        'it_qt_vida_util' => 'integer',
        'it_co_destinacao' => 'string',
        'it_no_destinacao' => 'string',
        'it_no_terceiros' => 'string',
        'it_no_material' => 'string',
        'it_co_resp_uorg' => 'integer',
        'it_no_resp_uorg' => 'string',
        'it_tx_descricao_complementar' => 'string',
        'it_da_atualizacao' => 'date:d/m/Y',
    ];


    public function getPatrimonioApiComFiltro(Request $request)
    {
        if (!$request->offset) {
            $dados = $this->skip(0)->take(1000);
        } else {
            $dados = $this->skip($request->offset)->take(1000);
        }

        if ($request->orgao) {
            $dados->where('it_co_orgao', $request->orgao)->orderBy('it_co_ug', 'ASC');
        }

        if ($request->unidade) {
            $dados->where('it_co_ug', $request->unidade)->orderBy('it_co_uorg', 'ASC');
        }

        if ($request->uorg) {
            $dados->where('it_co_uorg', $request->uorg)->orderBy('it_nu_patrimonial', 'ASC');
        }

        if ($request->numero_patrimonio) {
            $dados->where('it_nu_patrimonial', $request->numero_patrimonio);
        }

        $dados->select(
            DB::Raw('it_co_orgao::varchar as orgao_siads'),
            DB::Raw("LPAD(it_co_ug::varchar,6,'0') as unidade_siads"),
            DB::Raw("LPAD(it_co_gestao::varchar,5,'0') as gestao_siads"),
            DB::Raw("trim(it_co_uorg::varchar) as uorg_siads"),
            DB::Raw('it_co_conta_contabil::bigint as conta_contabil_siads'),
            DB::Raw('it_nu_patrimonial_ant::bigint as numero_patrimonial_antigo_siads'),
            DB::Raw('it_nu_patrimonial::bigint as numero_patrimonial_siads'),
            DB::Raw("(substring(it_da_tombamento::varchar,1,4) ||'-'|| substring(it_da_tombamento::varchar,5,2) ||'-'|| substring(it_da_tombamento::varchar,7,2))::date as data_tombamento_siads"),
            DB::Raw('it_co_item_material::varchar as codigo_material_siads'),
            DB::Raw('TRIM(it_no_material::varchar) as nome_material_siads'),
            DB::Raw('trim(it_tx_descricao_complementar::varchar) as descricao_complementar_siads'),
            DB::Raw('trim(it_no_marca::varchar) as marca_siads'),
            DB::Raw('trim(it_no_modelo::varchar) as modelo_siads'),
            DB::Raw('trim(it_nu_serie::varchar) as serie_siads'),
            DB::Raw('trim(it_no_fabricante::varchar) as fabricante_siads'),
            DB::Raw('trim(it_nu_processo::varchar) as processo_siads'),
            DB::Raw('trim(it_co_terceiros::varchar) as codigo_terceiros_siads'),
            DB::Raw('round(it_va_inicial_bem::numeric,2) as valor_inicial_bem_siads'),
            DB::Raw('round(it_va_bem::decimal,2) as valor_bem_siads'),
            DB::Raw('round(it_va_bem_depre::decimal,2) as valor_depreciado_siads'),
            DB::Raw('round(it_va_liq_bem::decimal,2) as valor_liquido_siads'),
            DB::Raw('round(it_va_doacao_venda::decimal,2) as valor_doacao_venda_siads'),
            DB::Raw('trim(it_co_tipo_bem::varchar) as codigo_tipo_bem_siads'),
            DB::Raw('trim(it_no_tipo_bem::varchar) as nome_tipo_bem_siads'),
            DB::Raw('trim(it_co_situacao::varchar) as codigo_situacao_siads'),
            DB::Raw('trim(it_no_situacao::varchar) as nome_situacao_siads'),
            DB::Raw('trim(it_co_destinacao::varchar) as codigo_destinacao_siads'),
            DB::Raw('trim(it_no_destinacao::varchar) as nome_destinacao_siads'),
            DB::Raw('it_nu_cpf_responsavel::bigint as cpf_responsavel_siads'),
            DB::Raw('trim(it_no_responsavel::varchar) as nome_responsavel_siads'),
            DB::Raw('it_co_resp_uorg::bigint as cpf_responsavel_uorg_siads'),
            DB::Raw('trim(it_no_resp_uorg::varchar) as nome_responsavel_uorg_siads'),
        );

        return $dados->orderBy('it_nu_patrimonial')->get()->toArray();
    }


    public function getOrgaoAttribute()
    {
        return $this->it_co_orgao;
    }

    public function getUnidadeAttribute()
    {
        return str_pad($this->it_co_ug,6,'0',STR_PAD_LEFT);
    }

    public function getGestaoAttribute()
    {
        return str_pad($this->it_co_gestao,5,'0',STR_PAD_LEFT);
    }

    public function getUorgAttribute()
    {
        return $this->it_co_uorg;
    }

    public function getContaContabilAttribute()
    {
        return $this->it_co_conta_contabil;
    }

    public function getNumeroPatrimonialAntigoAttribute()
    {
        return $this->it_nu_patrimonial_ant;
    }

    public function getNumeroPatrimonialAttribute()
    {
        return $this->it_nu_patrimonial;
    }

    public function getDataTombamentoAttribute()
    {
        return ($this->it_da_tombamento !== '0') ? date('Y-m-d', strtotime($this->it_da_tombamento)) : null;
    }

    public function getCodigoMaterialAttribute()
    {
        return $this->it_co_item_material;
    }

    public function getNomeMaterialAttribute()
    {
        return $this->it_no_material;
    }

    public function getDescricaoComplementarAttribute()
    {
        return $this->it_tx_descricao_complementar;
    }

    public function getMarcaAttribute()
    {
        return $this->it_no_marca;
    }

    public function getModeloAttribute()
    {
        return $this->it_no_modelo;
    }

    public function getSerieAttribute()
    {
        return $this->it_nu_serie;
    }

    public function getFabricanteAttribute()
    {
        return $this->it_no_fabricante;
    }

    public function getProcessoAttribute()
    {
        return $this->it_nu_processo;
    }

    public function getCodigoTerceirosAttribute()
    {
        return $this->it_co_terceiros;
    }

    public function getValorInicialBemAttribute()
    {
        return $this->it_va_inicial_bem;
    }

    public function getValorBemAttribute()
    {
        return $this->it_va_bem;
    }

    public function getValorDepreciadoAttribute()
    {
        return $this->it_va_bem_depre;
    }

    public function getValorLiquidoAttribute()
    {
        return $this->it_va_liq_bem;
    }

    public function getValorDoacaoVendaAttribute()
    {
        return $this->it_va_doacao_venda;
    }

    public function getCodigoTipoBemAttribute()
    {
        return $this->it_co_tipo_bem;
    }

    public function getNomeTipoBemAttribute()
    {
        return $this->it_no_tipo_bem;
    }

    public function getCodigoSituacaoAttribute()
    {
        return $this->it_co_situacao;
    }

    public function getNomeSituacaoAttribute()
    {
        return $this->it_no_situacao;
    }

    public function getCodigoDestinacaoAttribute()
    {
        return $this->it_co_destinacao;
    }

    public function getNomeDestinacaoAttribute()
    {
        return $this->it_no_destinacao;
    }

    public function getCpfResponsavelAttribute()
    {
        return $this->it_nu_cpf_responsavel;
    }

    public function getNomeResponsavelAttribute()
    {
        return $this->it_no_responsavel;
    }

    public function getCpfResponsavelUorgAttribute()
    {
        return $this->it_co_resp_uorg;
    }

    public function getNomeResponsavelUorgAttribute()
    {
        return $this->it_no_resp_uorg;
    }


}






