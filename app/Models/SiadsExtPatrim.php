<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiadsExtPatrim extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $connection = 'pgsql2';
    protected $table = 'siads_ext_patrim';
    protected $primaryKey = 'id';


    public function getOrgaoAttribute()
    {
        return $this->it_co_orgao;
    }

    public function getUnidadeAttribute()
    {
        return str_pad($this->it_co_ug, 6, '0', STR_PAD_LEFT);
    }

    public function getGestaoAttribute()
    {
        return str_pad($this->it_co_gestao, 5, '0', STR_PAD_LEFT);
    }

    public function getUorgAttribute()
    {
        return $this->it_co_uorg;
    }

    public function getContaContabilAttribute()
    {
        return $this->it_co_conta_contabil;
    }

    public function getNumeroPatrimonialAntigoAttribute()
    {
        return $this->it_nu_patrimonial_ant;
    }

    public function getNumeroPatrimonialAttribute()
    {
        return $this->it_nu_patrimonial;
    }

    public function getDataTombamentoAttribute()
    {
        return ($this->it_da_tombamento !== '0') ? date('Y-m-d', strtotime($this->it_da_tombamento)) : null;
    }

    public function getCodigoMaterialAttribute()
    {
        return $this->it_co_item_material;
    }

    public function getNomeMaterialAttribute()
    {
        return $this->it_no_material;
    }

    public function getDescricaoComplementarAttribute()
    {
        return $this->it_tx_descricao_complementar;
    }

    public function getMarcaAttribute()
    {
        return $this->it_no_marca;
    }

    public function getModeloAttribute()
    {
        return $this->it_no_modelo;
    }

    public function getSerieAttribute()
    {
        return $this->it_nu_serie;
    }

    public function getFabricanteAttribute()
    {
        return $this->it_no_fabricante;
    }

    public function getProcessoAttribute()
    {
        return $this->it_nu_processo;
    }

    public function getCodigoTerceirosAttribute()
    {
        return $this->it_co_terceiros;
    }

    public function getValorInicialBemAttribute()
    {
        return $this->it_va_inicial_bem;
    }

    public function getValorBemAttribute()
    {
        return $this->it_va_bem;
    }

    public function getValorDepreciadoAttribute()
    {
        return $this->it_va_bem_depre;
    }

    public function getValorLiquidoAttribute()
    {
        return $this->it_va_liq_bem;
    }

    public function getValorDoacaoVendaAttribute()
    {
        return $this->it_va_doacao_venda;
    }

    public function getCodigoTipoBemAttribute()
    {
        return $this->it_co_tipo_bem;
    }

    public function getNomeTipoBemAttribute()
    {
        return $this->it_no_tipo_bem;
    }

    public function getCodigoSituacaoAttribute()
    {
        return $this->it_co_situacao;
    }

    public function getNomeSituacaoAttribute()
    {
        return $this->it_no_situacao;
    }

    public function getCodigoDestinacaoAttribute()
    {
        return $this->it_co_destinacao;
    }

    public function getNomeDestinacaoAttribute()
    {
        return $this->it_no_destinacao;
    }

    public function getCpfResponsavelAttribute()
    {
        return $this->it_nu_cpf_responsavel;
    }

    public function getNomeResponsavelAttribute()
    {
        return $this->it_no_responsavel;
    }

    public function getCpfResponsavelUorgAttribute()
    {
        return $this->it_co_resp_uorg;
    }

    public function getNomeResponsavelUorgAttribute()
    {
        return $this->it_no_resp_uorg;
    }

}






