<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NotificationTo
 *
 * @property int $id
 * @property int $notification_id
 * @property int $user_id
 * @property bool $read
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Notification $notification
 * @property User $user
 *
 * @package App\Models
 */
class NotificationTo extends Model
{
	protected $table = 'notification_to';

	protected $casts = [
		'notification_id' => 'int',
		'user_id' => 'int',
		'read' => 'bool'
	];

	protected $fillable = [
		'notification_id',
		'user_id',
		'read'
	];

	public function notification()
	{
		return $this->belongsTo(Notification::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
