<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRoleUnit
 *
 * @property int $id
 * @property int $unit_id
 * @property int $user_id
 * @property int $role_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Unidade $unidade
 * @property User $user
 * @property Role $role
 *
 * @package App\Models
 */
class UserRoleUnit extends Model
{
    protected $table = 'user_role_unit';

    protected $casts = [
        'unit_id' => 'int',
        'user_id' => 'int',
        'role_id' => 'int'
    ];

    protected $fillable = [
        'unit_id',
        'user_id',
        'role_id'
    ];

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unit_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
