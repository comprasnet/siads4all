<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPermission
 * 
 * @property int $id
 * @property int|null $user_id
 * @property int|null $permission_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $unit_id
 *
 * @package App\Models
 */
class UserPermission extends Model
{
	protected $table = 'user_permission';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'user_id' => 'int',
		'permission_id' => 'int',
		'unit_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'permission_id',
		'unit_id'
	];
}
