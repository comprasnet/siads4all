<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class QuickAccess
 *
 * @property int $id
 * @property string $title
 * @property string|null $subtitle
 * @property string $icon
 * @property string $url
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $order
 * @property string|null $permissions
 *
 * @package App\Models
 */
class QuickAccess extends Model
{
	protected $table = 'quick_accesses';

	protected $casts = [
		'order' => 'int'
	];

	protected $fillable = [
		'title',
		'subtitle',
		'icon',
		'url',
		'slug',
		'order',
		'permissions'
	];

}
