<?php

namespace App\Enums;

enum FileTypeEnum:string
{
    case EXCEL = 'excel';
    case CSV = 'csv';
    case PDF = 'pdf';
    case TXT = 'txt';
    case PDF_TABLE = 'pdfTable';
}
