<?php

namespace App\Enums;

enum EnumPermissions: string
{
    case ADMIN_USERS = 'admin_users';
    case ADMIN_ORGAOS = 'admin_orgaos';
    case ADMIN_UNIT = 'admin_unit';
}
