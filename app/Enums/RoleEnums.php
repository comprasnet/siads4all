<?php

namespace App\Enums;

enum RoleEnums:string
{
    case ADMINISTRADOR = 'Administrador';
    case ADMINISTRADOR_ORGAO = 'Administrador Órgão';
    case ADMINISTRADOR_UNIDADE = 'Administrador Unidade';
    case API = 'Api';
    case CONSULTA = 'Consulta';
}
