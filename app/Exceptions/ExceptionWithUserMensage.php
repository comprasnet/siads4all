<?php

namespace App\Exceptions;

use App\Helper\JsonResponse as HelperJsonResponse;
use App\Traits\JsonResponse;
use Exception;

class ExceptionWithUserMensage extends Exception
{
    public function toArray()
    {
        return (new HelperJsonResponse)->jsonError($this->message);
    }

    public function toJsonResponse(){
        return (new HelperJsonResponse)->responseJsonError($this->message);
    }
}
