<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function getAllUsers()
    {
        return User::all();
    }

    public function getUserById($id)
    {
        return User::findOrFail($id);
    }

    public function createUser($data)
    {
        $user = User::create($data);
        return $user;
    }

    public function updateUser($id, $data)
    {
        /* @var User $user */
        $user = User::find($id);

        if ($user) {
            $user->update($data);
        }
        return $user;
    }


    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->roles()->detach();
        $user->permissions()->detach();
        $user->delete();
    }

    public function getUsersUnitAdmin($unit)
    {
        $users = User::join('unidadesusers', 'unidadesusers.user_id', '=', 'users.id')
            ->where('unidadesusers.unidade_id', 23)
            ->select('users.*')
            ->distinct()
            ->get();
        return $users;
    }

    public function getAllUsersByPermissionNameAndUnit($permission, $unit)
    {
        return User::select('users.*')
            ->leftJoin('user_role_unit', 'user_role_unit.user_id', '=', 'users.id')
            ->leftJoin('role_has_permissions', 'role_has_permissions.role_id', '=', 'user_role_unit.role_id')
            ->leftJoin('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->where(function ($query) use($permission,$unit) {
                $query->where('user_role_unit.unit_id', $unit)
                    ->where('permissions.guard_name', $permission);
            })
            ->orWhere('users.super_admin', true)
            ->get();
    }
}
