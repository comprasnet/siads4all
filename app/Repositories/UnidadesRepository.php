<?php

namespace App\Repositories;

use App\Models\Unidade;
use App\Models\Unidadesuser;
use Illuminate\Support\Facades\DB;

class UnidadesRepository
{
    public function getAllUnidades()
    {
        return Unidade::all();
    }

    public function getUnidadesByUserId($userId)
    {
        return Unidade::query()->join('unidadesusers', 'unidadesusers.unidade_id', '=', 'unidades.id')
            ->join('users', 'unidadesusers.user_id', '=', 'users.id')
            ->where('unidadesusers.user_id', $userId)
            ->select(['unidades.*', 'users.unidade_id as default_unit'])
            ->get();
    }

    public function getFirstUnidadeByUserId($userId)
    {
        return Unidade::query()->join('unidadesusers', 'unidadesusers.unidade_id', '=', 'unidades.id')
            ->join('users', 'unidadesusers.user_id', '=', 'users.id')
            ->where('unidadesusers.user_id', $userId)
            ->select(['unidades.*', 'users.unidade_id as default_unit'])
            ->first();
    }

    public function checkPermissionUser($userId, $unidadeId)
    {
        return Unidadesuser::query()
                ->where('unidadesusers.user_id', $userId)
                ->where('unidadesusers.unidade_id', $unidadeId)
                ->count() > 0;
    }

    public function approveUser($unitAccessRequest)
    {
        $userId = $unitAccessRequest->user_id;
        $unidadeId = $unitAccessRequest->unit_id;

        $exists = Unidadesuser::query()
                ->where('user_id', $userId)
                ->where('unidade_id', $unidadeId)
                ->count() > 0;
        if (!$exists) {
            $unidadeUSer = new Unidadesuser();
            $unidadeUSer->user_id = $userId;
            $unidadeUSer->unidade_id = $unidadeId;
            $unidadeUSer->save();
        }

    }

    public function getCodesByUnitIds($unitIds)
    {
        $unidades = Unidade::query();
        if (!empty($unitIds)) {
            $unidades->whereIn('id', $unitIds);
        }
        $unidades = $unidades->get()->toArray();

        return array_column($unidades, 'codigo');
    }

    public function checkExistsUserByUnit($userId, $unitId)
    {
        return DB::table('unidadesusers')
            ->where('user_id', $userId)
            ->where('unidade_id', $unitId)
            ->exists();
    }


    public function create(array $data)
    {
        /** @var Unidade $existingRecord */
        $existingRecord = Unidade::withTrashed()->where('codigo', $data['codigo'])->first();

        if ($existingRecord) {
            // Se o registro existir e estiver soft-deletado
            if ($existingRecord->trashed()) {
                // Restaura o registro
                $existingRecord->restore();
            }

            // Atualiza o registro com os novos dados
            $existingRecord->update($data);
            return $existingRecord;
        }
        return Unidade::create($data);
    }

    public function update(Unidade $unidade, array $data)
    {
        return $unidade->update($data);
    }

    public function delete(Unidade $unidade)
    {
        return $unidade->delete();
    }

}
