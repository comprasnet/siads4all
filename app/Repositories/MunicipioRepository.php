<?php
namespace App\Repositories;

use App\Models\Municipio;

class MunicipioRepository
{
    public function findByUF($uf)
    {
        return Municipio::where('estado_id', $uf)
            ->pluck('municipios.nome', 'municipios.id')
            ->toArray();
    }

    /**
     * @param $id
     * @return Municipio
     */
    public function getMunicipio($id)
    {
        return Municipio::query()->where('id', $id)
            ->first();
    }
}
