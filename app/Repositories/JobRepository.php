<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Support\Facades\DB;

class JobRepository
{
    public function getJobIdPosition($jobId,$queue){
        return DB::table('jobs')
                ->where('id', '<', $jobId)
                ->where('queue', $queue)
                ->count()+1;
    }

    public function getJobByUUid($uuid){
        return Job::query()
            ->where('payload', 'like',"%{$uuid}%")
            ->first();
    }
}
