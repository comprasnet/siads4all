<?php

namespace App\Repositories;

use App\Models\Unidadesuser;

class UserUnitsRepository
{
    public function getUnitByUser($userId,$unitId){
        return Unidadesuser::query()
            ->where('user_id',$userId)
            ->where('unit_id',$unitId)
            ->first();
    }

    public function saveUnitByUser($userId,$unitId){
        return Unidadesuser::create([
            'user_id'=>$userId,
            'unidade_id'=>$unitId
        ]);
    }

    public function removeUnitByUser($userId,$unitId){
        Unidadesuser::query()
            ->where('user_id',$userId)
            ->where('unidade_id',$unitId)
        ->delete();
    }

    public function getOrCreateUnitByUser($userId,$unitId){
        $unidadeUser = $this->getUnitByUser($userId,$unitId);
        if(is_null($unidadeUser)){
            $unidadeUser = $this->saveUnitByUser($userId,$unitId);
        }
        return $unidadeUser;
    }
}
