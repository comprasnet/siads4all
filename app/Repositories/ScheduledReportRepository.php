<?php
namespace App\Repositories;

use App\Exceptions\ExceptionWithUserMensage;
use App\Models\File;
use App\Models\ScheduledReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ScheduledReportRepository
{
    public function reportExists($date, $dataRequest)
    {
        return ScheduledReport::where(DB::raw('data_request'),'=', json_encode($dataRequest))
            ->where('date',$date)
            ->exists();
    }

    public function createSchedule($name, $type, $userId, $dataRequest, $date)
    {
        if ($this->reportExists($date,$dataRequest)) {
            throw new ExceptionWithUserMensage('Relatório com o mesmos filtros já foi agendado para mesma data.');
        }
        $file = File::create([
            'file_location' => $name??'Relatório agendado sem nome',
            'filters_hash' => '',
            'user_id' => $userId,
            'data_agendamento'=>$date
        ]);

        return ScheduledReport::create([
            'name' => $name,
            'type' => $type,
            'user_id' => $userId,
            'data_request' => json_encode($dataRequest),
            'date' => $date,
            'status' => 'pending',
            'file_id'=>$file->id
        ]);
    }

    public function getSchedulesForToday()
    {
        return ScheduledReport::where('status', 'pending')
            ->where('date', Carbon::today())
            ->get();
    }

    public function updateStatus(ScheduledReport $report, $status)
    {
        $report->status = $status;
        $report->save();
    }
}
