<?php
namespace App\Repositories;

use App\Models\Orgao;

class OrgaoRepository
{
    protected $model;

    public function __construct(Orgao $orgao)
    {
        $this->model = $orgao;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $data)
    {
        /** @var Orgao $existingRecord */
        $existingRecord = $this->model->withTrashed()->where('codigo', $data['codigo'])
            ->first();

        if ($existingRecord) {
            // Se o registro existir e estiver soft-deletado
            if ($existingRecord->trashed()) {
                // Restaura o registro
                $existingRecord->restore();
            }

            // Atualiza o registro com os novos dados
            $existingRecord->update($data);
            return $existingRecord;
        }
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $orgao = $this->find($id);
        $orgao->update($data);
        return $orgao;
    }

    public function delete($id)
    {
        $orgao = $this->find($id);
        return $orgao->delete();
    }
}
