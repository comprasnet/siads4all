<?php

namespace App\Repositories;

use App\Models\QuickAccess;

class QuickAccessRepository
{
    public function getAllOrderedByOrder()
    {
        return QuickAccess::orderBy('order')->get();
    }
}
