<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\UserPermission;
use App\Models\UserRoleUnit;
use Spatie\Permission\Models\Permission;

class RolesRepository
{

    public function saveRoleByUnitAndUser($roles,$unit,$user){
        if(!is_array($roles)){
            $roles = [$roles];
        }
        foreach($roles as $role){
            $userPermission = new UserRoleUnit();
            $userPermission->user_id = $user;
            $userPermission->unit_id = $unit;
            $userPermission->role_id = $role;
            $userPermission->save();
        }
    }

    public function removeAllRolesByUserId($userId,$unit){

        UserRoleUnit::query()->where('user_id',$userId)
            ->where('unit_id',$unit)
            ->delete();
    }

    public function getRolesByIdOrAbove($userId)
    {
        return Role::where('id', '>=', $userId)->get();
    }


    public function getRolesByUserAndUnit($userId, $unitId){
        return UserRoleUnit::query()
            ->where('user_id', $userId)
            ->where('unit_id', $unitId)
            ->select(['id','unit_id','user_id','role_id'])
            ->get();
    }
}
