<?php

namespace App\Repositories;

use App\Models\Notification;
use App\Models\NotificationTo;
use App\Models\User;
use App\Models\UserPermission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationRepository
{
    /**
     * Obter a lista de notificações não lidas de um determinado usuário.
     *
     * @param User $user
     * @return int
     */
    public function getCountUnreadNotifications(User $user)
    {
        $date = date('Y-m-d H:i:s', strtotime('-1 days'));
        return Notification::query()->join('notification_to', 'notifications.id', '=', 'notification_to.notification_id')
            ->where('notification_to.user_id', $user->id)
            ->where('notification_to.created_at', '>=', $date)
            ->where('notification_to.read', false)
            ->orderByDesc('notifications.created_at')
            ->count();
    }

    public function setReadAllNotification(User $user)
    {
        DB::table('notification_to')
            ->where('read', false)
            ->where('user_id', $user->id)
            ->update(['read' => true]);
    }


    /**
     * Obter a lista de notificações não lidas de um determinado usuário.
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNoExpiredNotifications(User $user)
    {
        $date = date('Y-m-d H:i:s', strtotime('-1 days'));
        return Notification::query()
            ->join('notification_to', 'notifications.id', '=', 'notification_to.notification_id')
            ->where('notification_to.user_id', $user->id)
            ->where('notification_to.created_at', '>=', $date)
            ->orderByDesc('notifications.created_at')
            ->get(['notifications.*', 'notification_to.read']);
    }

    /**
     * Marca uma notificação como lida.
     *
     * @param int $notificationId
     * @return bool
     */
    public function markNotificationAsRead($notificationId)
    {
        $notificationTo = NotificationTo::find($notificationId);

        if ($notificationTo) {
            $notificationTo->read = true;
            return $notificationTo->save();
        }

        return false;
    }

    /**
     * Cria uma notificação para um ou para um grupo de usuários.
     *
     * @param string $message
     * @param string $link
     * @param mixed $recipients
     * @return bool
     */
    public function createNotification($message, $link = '', $recipients = [])
    {
        $notification = Notification::create([
            'message' => $message,
            'link' => $link
        ]);

        foreach ($recipients as $recipient) {
            NotificationTo::create([
                'notification_id' => $notification->id,
                'user_id' => $recipient instanceof User ? $recipient->id : $recipient,
                'read' => false,
            ]);
        }

        return true;
    }


    public function getRolesByUserAndUnit($userId, $unitId)
    {
        return UserPermission::query()
            ->where('user_id', $userId)
            ->where('unit_id', $unitId)
            ->get();
    }

    /**
     * Atualiza o status de leitura das notificações para o usuário atual.
     *
     * @return void
     */
    public function updateReadStatusForCurrentUser()
    {
        $userId = Auth::id(); // Obter o ID do usuário atual

        if ($userId) {
            DB::table('notification_to')
                ->where('read', false)
                ->where('user_id', $userId)
                ->update(['read' => true]);
        }
    }

    /**
     * Marca notificações como enviadas por push notification para o usuário atual.
     *
     * @return void
     */
    public function markPushSentForCurrentUser()
    {
        $userId = Auth::id(); // Obter o ID do usuário atual

        if ($userId) {
            DB::table('notification_to')
                ->where('push_sent', false)
                ->where('user_id', $userId)
                ->update(['push_sent' => true]);
        }
    }


    /**
     * Marca notificações como enviadas por push notification para o usuário atual.
     *
     * @return void
     */
    public function markPushSentForNotificationId($norificationId)
    {
        DB::table('notification_to')
            ->where('push_sent', false)
            ->where('id', $norificationId)
            ->update(['push_sent' => true]);
    }

    /**
     * Retorna notificações onde o push ainda não foi enviado para o usuário atual.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getUnsentPushNotificationsForCurrentUser()
    {
        $userId = Auth::id(); // Obter o ID do usuário atual

        if ($userId) {
            return DB::table('notification_to')
                ->where('push_sent', false)
                ->where('user_id', $userId)
                ->get();
        }

        return collect(); // Retorna uma coleção vazia se não houver usuário autenticado
    }
}
