<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class ContaContabilRepository
{
    public function getPluckContas(){
        return DB::table('contas')->pluck('nome', 'codigo')->toArray();
    }
}
