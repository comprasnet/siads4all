<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\UserPermission;
use App\Models\UserRoleUnit;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionsRepository
{
    public function __construct()
    {
    }

    public function getPermissionByRole($roles)
    {
        return Permission::query()
            ->join('role_has_permissions as r', 'r.permission_id', '=', 'permissions.id')
            ->whereIn('r.role_id', $roles)
            ->pluck('permissions.id');
    }

    public function removeAllPermissionsByUserId($userId, $unit)
    {
        UserPermission::query()->where('user_id', $userId)
            ->where('unit_id', $unit)
            ->delete();
    }

    public function setPermissionByUnitAndUser($permissions, $unit, $user)
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }
        foreach ($permissions as $permission) {
            $userPermission = new UserPermission();
            $userPermission->user_id = $user;
            $userPermission->unit_id = $unit;
            $userPermission->permission_id = (int) $permission;

            $userPermission->save();
        }
    }

    public function hasPermission(array|string $permission, int $userId, int $unitId)
    {
        if ($permission) {
            $permission = [$permission];
        }
        return Permission::join('user_permission', 'user_permission.permission_id', '=', 'permissions.id')
                ->where('user_permission.user_id', $userId)
                ->where('user_permission.unit_id', $unitId)
                ->whereIn('permissions.guard_name', $permission)
                ->count() > 0;
    }


    public function hasPermissions(array $permissions, int $userId, int $unitId){
        return Permission::join('user_permission', 'user_permission.permission_id', '=', 'permissions.id')
                ->where('user_permission.user_id', $userId)
                ->where('user_permission.unit_id', $unitId)
                ->whereIn('permissions.guard_name', $permissions)
                ->count() > 0;
    }


    public function getPermissionByUserAndUnit($userId, $unitId)
    {
        return UserPermission::query()
            ->where('user_id', $userId)
            ->where('unit_id', $unitId)
            ->select(['id', 'unit_id', 'user_id', 'permission_id'])
            ->get();
    }

    public function createPermissionAndAddAllAdmins($permission)
    {
         // Adiciona uma nova permissão na tabela permissions
         DB::table('permissions')->insert([
            'name' => $permission,
            'guard_name' => 'web'
        ]);

        // Recupera o ID da nova permissão
        $permission = DB::table('permissions')->where('name', 'new_permission')->first();

        if ($permission) {
            $units = UserRoleUnit::join('roles', 'user_role_unit.role_id', '=', 'roles.id')
                ->where('roles.name', 'Administrador')
                ->distinct()
                ->pluck('user_role_unit.unit_id');

            foreach ($units as $unit) {
                // Recupera todos os usuários administradores (super_admin = true)
                $adminUsers = DB::table('users')
                ->join('user_role_unit', 'users.id', '=', 'user_role_unit.user_id')
                ->join('roles', 'user_role_unit.role_id', '=', 'roles.id')
                ->where('roles.name','=', 'Administrador')
                ->where('user_role_unit.unit_id',$unit)
                ->select('users.id', 'users.unidade_id')
                ->get();

                foreach ($adminUsers as $user) {
                    // Atribui a nova permissão a cada usuário administrador na tabela user_permission
                    DB::table('user_permission')->insert([
                        'user_id' => $user->id,
                        'permission_id' => $permission->id,
                        'unit_id' => $unit,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
            }
        }
    }

    public function removePermission($permission){

        // Remove a permissão atribuída a cada usuário administrador na tabela user_permission
        $permission = DB::table('permissions')->where('name', $permission)->first();

        if ($permission) {
            DB::table('user_permission')->where('permission_id', $permission->id)->delete();
        }

        // Remove a permissão da tabela permissions
        DB::table('permissions')->where('name', $permission)->delete();

    }
}
