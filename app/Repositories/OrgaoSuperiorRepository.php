<?php

namespace App\Repositories;

use App\Models\OrgaosSuperior;

class OrgaoSuperiorRepository
{
    /**
     * @param $id
     * @return OrgaosSuperior
     */
    public function getById($id){
        return OrgaosSuperior::find($id);
    }

    public function getAll(){
        return OrgaosSuperior::all();
    }
}
