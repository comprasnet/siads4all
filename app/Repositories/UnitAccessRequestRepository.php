<?php

namespace App\Repositories;

use App\Models\UnitAccessRequest;

class UnitAccessRequestRepository
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REFUSED = 2;

    public function createUnitAccessRequest($userId, $unitId)
    {
        $unitAccessRequest = new UnitAccessRequest();
        $unitAccessRequest->user_id = $userId;
        $unitAccessRequest->unit_id = $unitId;
        return $unitAccessRequest->save();
    }


    public function getAccessRequest($userId, $unitId)
    {
        return UnitAccessRequest::where('user_id', $userId)
            ->where('unit_id', $unitId)
            ->where('status', 0)
            ->first();
    }

    /**
     * Retorna o model de unidade
     *
     * @param int $id
     * @return UnitAccessRequest|null
     */
    public function find($id)
    {
        return UnitAccessRequest::find($id);
    }

    public function updateStatusByUserAndUnitWithPendingStatus($unitId,$userId,$newStatus)
    {
        UnitAccessRequest::where('unit_id', $unitId)
            ->where('user_id', $userId)
            ->where('status', self::STATUS_PENDING)
            ->update(['status' => $newStatus]);
    }

}
