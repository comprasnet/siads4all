<?php

namespace App\Repositories;

use App\Models\Estado;

class EstadoRepository
{
    public function all()
    {
        return Estado::all(['id', 'nome']);
    }
}
