<?php

namespace App\Repositories;

use App\Models\File;

class FilesRepository
{
    public function addFile($location, $name,$userId)
    {
        return File::create([
            'file_location' => $location,
            'filters_hash' => '',
            'user_id' => $userId
        ]);
    }

    public function startFileCreate($fileUrl, $basePath, $arrFilter): File
    {
        return File::create([
            'file_location' => $fileUrl,
            'filters_hash' => md5(json_encode($arrFilter)),
            'base_path'=>$basePath
        ]);
    }

    public function listUserFiles($userId)
    {
        return File::query()
            ->where('user_id', $userId)
            ->get();
    }
}
