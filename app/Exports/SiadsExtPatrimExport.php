<?php

namespace App\Exports;

use App\Models\SiadsExtPatrim;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;

class SiadsExtPatrimExport implements FromArray
{
    protected $ug;

    public function __construct($request)
    {
        $this->unidade = $request->unidade;
        $this->uorg = $request->uorg;
        $this->numero_patrimonial = $request->numero_patrimonial;
        $this->conta_contabil = $request->conta_contabil;

    }

    /**
     * @return array
     */
    public function array() : array
    {

        if(!$this->unidade){
            return [];
        }

        $row = [
            [
            "orgao_siads",
            "unidade_siads",
            "gestao_siads",
            "uorg_siads",
            "conta_contabil_siads",
            "numero_patrimonial_antigo_siads",
            "numero_patrimonial_siads",
            "data_tombamento_siads",
            "codigo_material_siads",
            "nome_material_siads",
            "descricao_complementar_siads",
            "marca_siads",
            "modelo_siads",
            "serie_siads",
            "fabricante_siads",
            "processo_siads",
            "codigo_terceiros_siads",
            "valor_inicial_bem_siads",
            "valor_bem_siads",
            "valor_depreciado_siads",
            "valor_liquido_siads",
            "valor_doacao_venda_siads",
            "codigo_tipo_bem_siads",
            "nome_tipo_bem_siads",
            "codigo_situacao_siads",
            "nome_situacao_siads",
            "codigo_destinacao_siads",
            "nome_destinacao_siads",
            "cpf_responsavel_siads",
            "nome_responsavel_siads",
            "cpf_responsavel_uorg_siads",
            "nome_responsavel_uorg_siads"
            ]
        ];

        $dados = SiadsExtPatrim::where('it_co_ug', $this->unidade)
            ->select(
            DB::Raw('it_co_orgao::varchar as orgao_siads'),
            DB::Raw("LPAD(it_co_ug::varchar,6,'0') as unidade_siads"),
            DB::Raw("LPAD(it_co_gestao::varchar,5,'0') as gestao_siads"),
            DB::Raw("trim(it_co_uorg::varchar) as uorg_siads"),
            DB::Raw('it_co_conta_contabil::bigint as conta_contabil_siads'),
            DB::Raw('it_nu_patrimonial_ant::bigint as numero_patrimonial_antigo_siads'),
            DB::Raw('it_nu_patrimonial::bigint as numero_patrimonial_siads'),
            DB::Raw("substring(it_da_tombamento::varchar,7,2) ||'/'|| substring(it_da_tombamento::varchar,5,2) ||'/'|| (substring(it_da_tombamento::varchar,1,4)) as data_tombamento_siads"),
            DB::Raw('it_co_item_material::varchar as codigo_material_siads'),
            DB::Raw('TRIM(it_no_material::varchar) as nome_material_siads'),
            DB::Raw('trim(it_tx_descricao_complementar::varchar) as descricao_complementar_siads'),
            DB::Raw('trim(it_no_marca::varchar) as marca_siads'),
            DB::Raw('trim(it_no_modelo::varchar) as modelo_siads'),
            DB::Raw('trim(it_nu_serie::varchar) as serie_siads'),
            DB::Raw('trim(it_no_fabricante::varchar) as fabricante_siads'),
            DB::Raw('trim(it_nu_processo::varchar) as processo_siads'),
            DB::Raw('trim(it_co_terceiros::varchar) as codigo_terceiros_siads'),
            DB::Raw('round(it_va_inicial_bem::numeric,2) as valor_inicial_bem_siads'),
            DB::Raw('round(it_va_bem::decimal,2) as valor_bem_siads'),
            DB::Raw('round(it_va_bem_depre::decimal,2) as valor_depreciado_siads'),
            DB::Raw('round(it_va_liq_bem::decimal,2) as valor_liquido_siads'),
            DB::Raw('round(it_va_doacao_venda::decimal,2) as valor_doacao_venda_siads'),
            DB::Raw('trim(it_co_tipo_bem::varchar) as codigo_tipo_bem_siads'),
            DB::Raw('trim(it_no_tipo_bem::varchar) as nome_tipo_bem_siads'),
            DB::Raw('trim(it_co_situacao::varchar) as codigo_situacao_siads'),
            DB::Raw('trim(it_no_situacao::varchar) as nome_situacao_siads'),
            DB::Raw('trim(it_co_destinacao::varchar) as codigo_destinacao_siads'),
            DB::Raw('trim(it_no_destinacao::varchar) as nome_destinacao_siads'),
            DB::Raw('it_nu_cpf_responsavel::bigint as cpf_responsavel_siads'),
            DB::Raw('trim(it_no_responsavel::varchar) as nome_responsavel_siads'),
            DB::Raw('it_co_resp_uorg::bigint as cpf_responsavel_uorg_siads'),
            DB::Raw('trim(it_no_resp_uorg::varchar) as nome_responsavel_uorg_siads'),
        )
            ->orderBy('it_nu_patrimonial');

        if ($this->uorg) {
            $dados->where('it_co_uorg', $this->uorg);
        }

        if ($this->numero_patrimonial) {
            $dados->where('it_nu_patrimonial', $this->numero_patrimonial);
        }

        if ($this->conta_contabil) {
            $dados->where('it_co_conta_contabil', $this->conta_contabil);
        }

        $retorno = array_merge($row,$dados->get()->toArray());

        return $retorno;
    }
}
