<?php

namespace App\Services;

use App\Enums\EnumPermissions;

class PermissionRouteService
{
    private $routesPermission;

    public function __construct(
        private PermissionsService $permissionsService
    )
    {
        $this->routesPermission = [
            '/unit/aprove' => EnumPermissions::ADMIN_USERS->value
        ];
    }

    public function checkRoutePermission($userId, $unitId, $route){
        if(!isset($this->routesPermission[$route])){
            return true;
        }
        return $this->permissionsService->hasPermission(
            $this->routesPermission[$route],$userId, $unitId
        );
    }
}
