<?php

namespace App\Services;

use App\Repositories\ContaContabilRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Datalake
{
    const SITUACAO_CACHE = 'situacao_cache';
    const DESTINACAO_CACHE = 'destinacao_cache';
    public function __construct(
        private UnidadesService $unidadesRepository
    )
    {
    }

    public function getUorgsByUgs($unidades){
        return DB::connection('pgsql2')
            ->table('siorg_uorg')
            ->select('it_co_uorg', 'it_no_uorg')
            ->whereIn('it_co_ug_vinc', $unidades)
            ->get();
    }


    public function getUorgsByUgsSelect2($unidades)
    {
        $orgs = $this->getUorgsByUgs($unidades);
        $results = [];
        foreach ($orgs as $item) {
            $results[] = ['id' => $item->it_co_uorg, 'text' => $item->it_co_uorg.' - '.$item->it_no_uorg];
        }
        return $results;
    }

    public function getSituacaoList()
    {
        if(Cache::has(self::SITUACAO_CACHE)){
            return Cache::get(self::SITUACAO_CACHE);
        }
        $situacaoList = DB::connection('pgsql2')
            ->table('siads_ext_patrim')
            ->select('it_no_situacao')
            ->distinct()
            ->where('it_no_situacao', '<>', '')
            ->whereNotNull('it_no_situacao')
            ->get();
        Cache::put(self::SITUACAO_CACHE,$situacaoList,now()->addHour(1));
        return $situacaoList;
    }

    public function getDestinacaoList()
    {
        if(Cache::has(self::DESTINACAO_CACHE)){
            return Cache::get(self::DESTINACAO_CACHE);
        }
        $destinacao = DB::connection('pgsql2')
            ->table('siads_ext_patrim')
            ->select('it_co_destinacao', 'it_no_destinacao')
            ->distinct()
            ->where('it_no_destinacao', '<>', '')
            ->whereNotNull('it_co_destinacao')
            ->get();
        Cache::put(self::DESTINACAO_CACHE,$destinacao,now()->addHour(1));
        return $destinacao;
    }



    public function getContasList($search=null,$ugId=null)
    {
        $contasQuery = DB::connection('pgsql2')
            ->table('siads_ext_patrim')
            ->select('it_co_conta_contabil', 'it_co_conta_contabil')
            ->distinct()
            ->whereNotNull('it_co_conta_contabil');
            if(!is_null($ugId)){
                $ugCod = $this->unidadesRepository->idToCode($ugId);
                $contasQuery->where('it_co_ug','=',$ugCod);
            }
            if(!is_null($search) and !empty($search)){
                $contasQuery->where('it_co_conta_contabil','like',$search.'%');
            }
        $contas = $contasQuery->get();
        return $contas;
    }

    public function getContasContabilSelect2($search=null,$ugId=null)
    {
        $contas = $this->getContasList($search,$ugId);
        $contasPluck = (new ContaContabilRepository())->getPluckContas();
        $results = [];
        foreach ($contas as $item) {
            $name = $item->it_co_conta_contabil;
            if(isset($contasPluck[$name])){
                $name = $name . ' - '. $contasPluck[$name];
            }
            $results[] = ['id' => $item->it_co_conta_contabil, 'text' => $name];
        }
        return $results;
    }

    public function getSumContaContabil($conta){
        $total = DB::connection('pgsql2')
        ->table('siads_ext_patrim')
        ->where('it_co_conta_contabil', $conta)
        ->sum('it_va_bem');
        return $total;
    }

}
