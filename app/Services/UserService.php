<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAllUsers()
    {
        return $this->userRepository->getAllUsers();
    }

    public function getUserById($id)
    {
        return $this->userRepository->getUserById($id);
    }

    public function createUser($data)
    {
        $data = $this->normalizeDataForSave($data);
        return $this->userRepository->createUser($data);
    }

    public function createOrUpdateUser($data, $id = null)
    {
        $data = $this->normalizeDataForSave($data);
        if ((int)$id > 0) {
            return $this->userRepository->updateUser($id, $data);
        }
        return $this->userRepository->createUser($data);
    }

    public function updateUser($id, $data)
    {
        $data = $this->normalizeDataForSave($data);
        return $this->userRepository->updateUser($id, $data);
    }

    public function deleteUser($id)
    {
        $this->userRepository->deleteUser($id);
    }

    public function getUsersUnitAdmin($unit)
    {
        return $this->userRepository->getUsersUnitAdmin($unit);
    }

    private function normalizeDataForSave($data)
    {
        $data['matricula_siape'] = str_pad($data['matricula_siape'],7,'0',STR_PAD_LEFT);
        $data['super_admin'] = (isset($data['super_admin']) && (int)$data['super_admin']);
        if($data['super_admin'] and $data['default_unit_admin']){
            $data['unidade_id'] = $data['default_unit_admin'];
            unset($data['default_unit_admin']);
        }
        if(!$data['super_admin'] and isset($data['unit-default'])){
            $data['unidade_id'] = $data['unit-default'];
            unset($data['unit-default']);
        }
        return $data;
    }

    public function getUserWithAdminUserPermissionByUnit($unit)
    {
        return $this->userRepository->getAllUsersByPermissionNameAndUnit(PermissionsService::ADMIN_USERS,$unit);
    }
}
