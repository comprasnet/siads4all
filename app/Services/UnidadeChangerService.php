<?php

namespace App\Services;

use App\Models\Unidade;

class UnidadeChangerService
{
    private $unidadeSession;

    public function __construct(
        private UnidadesService $UnidadeService
    )
    {
        $user = auth()->user();
        if(!is_null($user)) {
            $this->unidadeSession = 'unidade_' . auth()->user()->id;
        }
    }

    public function getCurrentUnidadeId()
    {
        $default = auth()->user()->unidade_id;
        if (!\session()->has($this->unidadeSession)) {
            session([$this->unidadeSession => $default]);
        }
        return session($this->unidadeSession);
    }

    public function getCurrentUnidade():Unidade|null
    {
        $unidadeId = $this->getCurrentUnidadeId();
        if(is_null($unidadeId) or empty($unidadeId)){
            $unidade = $this->UnidadeService->getFirstUnidadeByUser();

            if($unidade){
                return $unidade;
            }
           return null;
        }
        return Unidade::find($unidadeId);
    }

    public function setCurrentUnidade($unidadeId)
    {
        session([$this->unidadeSession => $unidadeId]);
    }
}
