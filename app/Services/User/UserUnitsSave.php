<?php

namespace App\Services\User;

use App\Models\User;
use App\Repositories\PermissionsRepository;
use App\Repositories\RolesRepository;
use App\Repositories\UserUnitsRepository;

class UserUnitsSave
{
    private array $units = [];
    private User $user;

    public function __construct(
        private UserUnitsRepository $userUnitsRepository,
        private PermissionsRepository $permissionsRepository,
        private RolesRepository $roleRepository
    )
    {

    }

    public function save(){
        foreach ($this->units as $unit){
            if(!$unit){
                $this->removeUnit($unit);
                continue;
            }
            $this->saveUnit($unit);
        }
    }

    /**
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param mixed $units
     * @return UserUnitsSave
     */
    public function setUnits($units)
    {
        $this->units = $units;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return UserUnitsSave
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    private function removeUnit($unitData){
        $this->userUnitsRepository->removeUnitByUser($this->user->id,$unitData['id']);
        $this->permissionsRepository->removeAllPermissionsByUserId($this->user->id,$unitData['id']);
        $this->roleRepository->removeAllRolesByUserId($this->user->id,$unitData['id']);
    }

    private function saveUnit($unitData)
    {
        $this->userUnitsRepository->saveUnitByUser($this->user->id,$unitData['id']);

        $this->permissionsRepository->removeAllPermissionsByUserId($this->user->id,$unitData['id']);
        $this->roleRepository->removeAllRolesByUserId($this->user->id,$unitData['id']);

        $this->permissionsRepository->setPermissionByUnitAndUser(
            $unitData['permissions'],$unitData['id'],$this->user->id
        );
        $this->roleRepository->saveRoleByUnitAndUser(
            $unitData['roles'],$unitData['id'],$this->user->id
        );

    }

}
