<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\NotificationRepository;
use Illuminate\Support\Facades\Auth;

class NotificationService
{
    protected $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Obter a lista de notificações não lidas de um determinado usuário.
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNoExpiredNotifications(?User $user)
    {
        if (!$user) {
            return [];
        }
        return $this->notificationRepository->getNoExpiredNotifications($user);
    }


    /**
     * Obter a lista de notificações não lidas de um determinado usuário.
     *
     * @param User $user
     * @return int
     */
    public function getCountUnreadNotifications(?User $user)
    {
        if (!$user) {
            return 0;
        }
        return $this->notificationRepository->getCountUnreadNotifications($user);
    }

    public function setReadAllNotification(?User $user)
    {
        if (!$user) {
            $user = Auth::user();
        }
        $this->notificationRepository->setReadAllNotification($user);
    }

    /**
     * Marca uma notificação como lida.
     *
     * @param int $notificationId
     * @return bool
     */
    public function markNotificationAsRead($notificationId)
    {
        return $this->notificationRepository->markNotificationAsRead($notificationId);
    }

    /**
     * Cria uma notificação para um ou para um grupo de usuários.
     *
     * @param string $message
     * @param string $link
     * @param mixed $recipients
     * @return bool
     */
    public function createNotification($message, $link = '', $recipients = [])
    {
        return $this->notificationRepository->createNotification($message, $link, $recipients);
    }
}
