<?php

namespace App\Services;

use App\Models\Unidade;
use App\Repositories\PermissionsRepository;
use App\Repositories\RolesRepository;
use App\Repositories\UnidadesRepository;
use Illuminate\Support\Facades\Auth;

class UnidadesService
{
    const TIPO = [
        'Executora', 'Controle', 'Credora'
    ];

    const ADMINISTRACAO = [
        1 => 'ADMINISTRAÇÃO DIRETA',
        2 => 'ESTATAL',
        3 => 'AUTARQUIA',
        4 => 'FUNDAÇÃO',
        5 => 'EMPRESA PÚBLICA COM. E FIN.',
        6 => 'ECONOMIA MISTA',
        7 => 'FUNDOS',
        8 => 'EMPRESA PUBLICA INDUSTRIAL E AGRICOLA',
        11 => 'ADMINISTRACAO DIRETA ESTADUAL',
        12 => 'ADMINISTRACAO DIRETA MUNICIPAL',
        13 => 'ADMINISTRACAO INDIRETA ESTADUAL',
        14 => 'ADMINISTRACAO INDIRETA MUNICIPAL',
        15 => 'EMPRESA PRIVADA',
    ];

    const PODER = [
        'Executivo', 'Legislativo', 'Judiciário'
    ];

    const ESFERAS = [
        'F' => 'Federal',
        'E' => 'Estadual',
        'M' => 'Municipal'
    ];

    private static $unidadeCurrentUser = [];

    public function __construct(
        private UnidadesRepository    $unidadeRepository,
        private PermissionsRepository $permissionsRepository,
        private RolesRepository       $rolesRepository
    )
    {
    }

    public function getFirstUnidadeByUser()
    {
        $user = Auth::user();

        if ($user->super_admin) {
            return self::$unidadeCurrentUser = Unidade::query()
                ->where('situacao', true)
                ->first();
        }
        return $this->unidadesRepository->getFirstUnidadeByUserId($user->id);
    }

    public function getUnidadesByCurrentUser()
    {
        $user = Auth::user();

        if ($user->super_admin) {
            self::$unidadeCurrentUser = Unidade::query()
                ->where('situacao', true)
                ->get();
            return $this->unidadesRepository->getUnidadesByUserId($user->id);
        }

        return $this->unidadesRepository->getFirstUnidadeByUserId($user->id);
    }

    public function getUnidadesByCurrentUserSelect2($search = null, $limit = 50)
    {
        $unidades = $this->getUnidadesByCurrentUserSearchLimit($search, $limit);
        $results = [];
        foreach ($unidades as $item) {
            $results[] = ['id' => $item->id, 'text' => $item->codigo . ' - ' . $item->nome];
        }
        return $results;
    }

    public function getUnidadesByCurrentUserSearchLimit($search = null, $limit = 50)
    {
        $user = Auth::user();

        if ($user->super_admin) {
            $unidadeCurrentUserQuery = Unidade::query()
                ->where('situacao', true);
            if (!empty($search) and !is_null($search)) {
                $unidadeCurrentUserQuery->where(function ($query) use ($search) {
                    $query->where('codigo', 'like', "%{$search}%")
                        ->orWhere('nome', 'ilike', "%{$search}%");
                });
            }
            $unidadeCurrentUserQuery->limit($limit);

            return $unidadeCurrentUserQuery->get();
        }

        self::$unidadeCurrentUser = $this->unidadeRepository->getUnidadesByUserId($user->id);
        return self::$unidadeCurrentUser;
    }

    public function aprovaUser($userId, $unidade, $roles, $permissions)
    {
        $this->unidadeRepository->approveUser($userId, $unidade);
    }

    public function getUnitsWithRolesAndPermissions($userId)
    {

        $units = $this->unidadeRepository->getUnidadesByUserId($userId)->toArray();
        foreach ($units as $k => $unit) {
            $units[$k]['permissions'] = $this->permissionsRepository
                ->getPermissionByUserAndUnit($userId, $unit['id'])->toArray();
            $units[$k]['roles'] = $this->rolesRepository
                ->getRolesByUserAndUnit($userId, $unit['id'])->toArray();
        }
        return $units;
    }

    public function idToCode($id)
    {
        $unidade = Unidade::find($id);
        if ($unidade) {
            return $unidade->codigo;
        }
        return null;
    }


    public function createUnidade(array $data)
    {
        $data = $this->normalize($data);
        return $this->unidadeRepository->create($data);
    }

    public function updateUnidade(Unidade $unidade, array $data)
    {
        $data = $this->normalize($data);
        $update =  $this->unidadeRepository->update($unidade, $data);
    }

    private function normalize($data)
    {
        $data['despacho_autorizatorio'] = isset($data['despacho_autorizatorio']);
        return $data;
    }

    public function deleteUnidade(Unidade $unidade)
    {
        return $this->unidadeRepository->delete($unidade);
    }
}
