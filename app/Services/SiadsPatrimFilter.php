<?php

namespace App\Services;

use App\Models\Unidade;
use App\Models\User;
use App\Repositories\ContaContabilRepository;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SiadsPatrimFilter extends FilterQueryRequest
{
    private $cols = [
        "uasg" => "UG",
        "uorg" => "UORG",
        "gestao" => "Gestão",
        "conta" => "Conta",
        "numero_patrimonial" => "Número Patrimonial",
        "descricao_material" => "Descrição do material",
        "valor_liquido" => "Valor Liquido",
        "valor" => "Valor",
        "valor_depreciado" => "Valor Depreciado",
        "data_tombamento" => "Data do tombamento",
        "situacao" => "Situação",
        "destinacao" => "Destinação",
        "endereco" => "Endereço",
        "responsavel" => "Responsável",
        "coresponsavel" => "CO-Responsável",
        "forma_aquisicao" => "Forma de aquisição",
        "contrato_garantia_compra" => "Garantia de Compra"
    ];

    private $userId = 0;

    private $fileNameExport = '';

    private $observable;

    private \closure $observablePreExportFile;
    private \closure $observablePosExportFile;
    private \closure $observableCurrentExportFile;
    private $contas;

    public function __construct(
        Request                 $request,
        ContaContabilRepository $contabilRepository
    )
    {
        $this->contas = $contabilRepository->getPluckContas();
        $this->observablePreExportFile = function ($totalData) {
        };
        $this->observablePosExportFile = function () {
        };
        $this->observableCurrentExportFile = function ($totalData, $currentData) {
        };
        $this->observable = function ($current, $total) {
        };
        $this->defaultDbDate = 'Ymd';
        $this->query = DB::connection('pgsql2')->table('siads_ger_pt_me.siads_ext_patrim')
            ->join('siorg_unidade_gestora', 'siads_ext_patrim.it_co_ug', '=', 'siorg_unidade_gestora.it_co_ug')
            ->join('siorg_orgao', 'siorg_orgao.it_co_orgao', '=', 'siads_ext_patrim.it_co_orgao')
            ->join('siorg_uorg', 'siads_ext_patrim.it_co_uorg', '=', 'siorg_uorg.it_co_uorg')
            ->where('siads_ext_patrim.it_co_ug', '>', 0)
            ->selectRaw("
            CONCAT(siads_ext_patrim.it_co_ug, ' - ', siorg_unidade_gestora.it_no_ug) as uasg,
            CONCAT(siads_ext_patrim.it_co_uorg, ' - ', siorg_uorg.it_no_uorg) as uorg,
            siads_ext_patrim.it_co_gestao as gestao,
            siads_ext_patrim.it_nu_patrimonial as numero_patrimonial,
            siads_ext_patrim.it_no_material as descricao_material,
            siads_ext_patrim.it_va_liq_bem as valor_liquido,
            siads_ext_patrim.it_va_bem as valor,
            siads_ext_patrim.it_va_bem_depre as valor_depreciado,
            CASE
                    WHEN LENGTH(CAST(siads_ext_patrim.it_da_tombamento AS VARCHAR)) = 8
                    THEN TO_DATE(CAST(siads_ext_patrim.it_da_tombamento AS VARCHAR), 'YYYYMMDD')
                    ELSE NULL
            END AS data_tombamento,
            siads_ext_patrim.it_no_situacao as situacao,
            siads_ext_patrim.it_no_destinacao as destinacao,
            siads_ext_patrim.it_ed_endereco as endereco,
            siads_ext_patrim.it_no_responsavel as coresponsavel,
            siads_ext_patrim.it_no_resp_uorg as responsavel,
            siads_ext_patrim.it_no_aquisicao as forma_aquisicao,
            CONCAT(siads_ext_patrim.it_nu_contrato_garan_comp, ' ', siads_ext_patrim.it_da_inicio_garantia, ' - ', siads_ext_patrim.it_da_fim_garantia, ' ', siads_ext_patrim.it_no_garantidor) as contrato_garantia_compra,
	        CONCAT ( siads_ext_patrim.it_co_conta_contabil) as conta
        ")
            ->distinct();


        parent::__construct($request);
        $this->addFieldFilter('uasg', self::TYPE_DEFAULT, 'siads_ext_patrim.it_co_ug');
        $this->addFieldFilter('situacao', self::TYPE_RAW_DEFAULT, 'trim(siads_ext_patrim.it_no_situacao)');
        $this->addFieldFilter('uorg', self::TYPE_DEFAULT, 'siads_ext_patrim.it_co_uorg');
        $this->addFieldFilter('tombamento', self::TYPE_DATE_BETWEEN, 'siads_ext_patrim.it_da_tombamento');
        $this->addFieldFilter('descricao', self::TYPE_LIKE, 'siads_ext_patrim.it_no_material');
        $this->addFieldFilter('destinacao', self::TYPE_DEFAULT, 'siads_ext_patrim.it_co_destinacao');
        $this->addFieldFilter('numero_patrimonial', self::TYPE_DEFAULT, 'siads_ext_patrim.it_nu_patrimonial');
        $this->addFieldFilter('conta_contabil', self::TYPE_DEFAULT, 'siads_ext_patrim.it_co_conta_contabil');
        $this->addFieldFilter('responsavel', self::TYPE_LIKE, 'siads_ext_patrim.it_no_resp_uorg');
        $this->addFieldFilter('coresponsavel', self::TYPE_LIKE, 'siads_ext_patrim.it_no_responsavel');

        if (isset($this->filterData['uasg']) and !empty($this->filterData['uasg'])) {
            $unidade = Unidade::find($this->filterData['uasg']);
            if ($unidade) {
                $this->filterData['uasg'] = $unidade->codigo;
            }
        }
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setFileNameExport($fileNameExport)
    {
        $this->fileNameExport = $fileNameExport;
    }

    protected function applyFilters($query)
    {
        if (isset($this->filterData['uasg']) and !empty($this->filterData['uasg'])) {
            $unidade = Unidade::find($this->filterData['uasg']);
            if ($unidade) {
                $this->filterData['uasg'] = $unidade->codigo;
            }
        }
        parent::applyFilters($query); // TODO: Change the autogenerated stub
    }

    public function changeData($line)
    {
        $line->data_tombamento = date('d/m/Y', strtotime($line->data_tombamento));
        $line->numero_patrimonial = (int)$line->numero_patrimonial;
        $line->valor_liquido = number_format($line->valor_liquido, 2, ',', '.');
        $line->valor = number_format($line->valor, 2, ',', '.');
        $line->valor_depreciado = number_format($line->valor_depreciado, 2, ',', '.');

        if (isset($this->contas[$line->conta])) {
            $line->conta = $line->conta . ' - ' . $this->contas[$line->conta];
        }

        return parent::changeData($line);
    }

    private function getCollsVisible()
    {
        $visibleCols = $this->filterData['visible_cols'];
        $cols = [];
        foreach ($visibleCols as $col) {
            $cols[$col] = $this->cols[$col];
        }
        return $cols;
    }

    private function getFileName()
    {
        $user = '';
        if ($this->userId) {
            $userModel = User::find($this->userId);
            if ($userModel) {
                $user = $userModel->name . '_';
            }
        }
        $fileNameExport = $this->fileNameExport;
        if (empty($fileNameExport)) {
            $fileNameExport = $user . 'relatorio_patrimonio_' . date('d-m-y_h:i');
        }
        return $fileNameExport;
    }

    public function countData()
    {
        $query = $this->getQueryReady();
        return $query->count();
    }

    public function excel()
    {
        $query = $this->getQueryReady();
        $results = $query->get();
        $total = counT($results);
        ($this->observablePreExportFile)($total);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $cols = $this->getCollsVisible();
        $columnsTitle = array_values($cols);
        $columnsName = array_keys($cols);

        $rowNumber = 1;
        $colNumber = 'A';

// Escreva os títulos no arquivo Excel
        foreach ($columnsTitle as $columnTitle) {
            $sheet->setCellValue($colNumber . $rowNumber, $columnTitle);
            $colNumber++;
        }

// Escreva os dados no arquivo Excel
        $rowNumber = 2;
        $total = count($results);
        foreach ($results as $current => $result) {
            ($this->observableCurrentExportFile)($total, $current);
            $cordinate1 = 'A';
            foreach ($columnsName as $colName) {
                $sheet->setCellValue($cordinate1 . $rowNumber, $result->$colName);
                $cordinate1++;
            }
            ($this->observable)($rowNumber, $total);
            $rowNumber++;
        }

        $writer = new Xlsx($spreadsheet);
        $name = $this->getFileName();
        $local = public_path('report-xls');
        if (!is_dir($local)) {
            mkdir($local, 0777, true);
        }
        if (file_exists($local . '/' . $name)) {
            return '/report-xls/' . $name;
        }

        $writer->save($this->file->base_path);
        ($this->observablePosExportFile)();
        return $this->file;
    }

    public function txt()
    {
        $query = $this->getQueryReady();
        $results = $query->get();
        $total = count($results);
        ($this->observablePreExportFile)($total);

        // Pegue as colunas visíveis
        $cols = $this->getCollsVisible();
        $columnsTitle = array_values($cols); // Não será usado, mas está aqui se precisar
        $columnsName = array_keys($cols);

        // Crie o diretório se não existir
        $local = public_path('report-txt');
        if (!is_dir($local)) {
            mkdir($local, 0777, true);
        }


        // Abre o arquivo para escrita
        $file = fopen($this->file->base_path, 'w');

        // Percorra os resultados e escreva no formato Coluna1: valor1
        foreach ($results as $current => $result) {
            ($this->observableCurrentExportFile)($total, $current);

            foreach ($columnsName as $colName) {
                $line = $colName . ':' . $result->$colName . PHP_EOL;
                fwrite($file, $line);
            }

            // Adicione uma linha em branco entre os registros (opcional)
            fwrite($file, PHP_EOL);

            ($this->observable)($current + 1, $total);
        }

        fclose($file);

        ($this->observablePosExportFile)();

        return $this->file;
    }



    public function csv()
    {
        $query = $this->getQueryReady();
        $results = $query->get();
        $total = counT($results);
        ($this->observablePreExportFile)($total);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $cols = $this->getCollsVisible();
        $columnsTitle = array_values($cols);
        $columnsName = array_keys($cols);

        $rowNumber = 1;
        $colNumber = 'A';

// Escreva os títulos no arquivo Excel
        foreach ($columnsTitle as $columnTitle) {
            $sheet->setCellValue($colNumber . $rowNumber, $columnTitle);
            $colNumber++;
        }

// Escreva os dados no arquivo Excel
        $rowNumber = 2;
        $total = count($results);
        foreach ($results as $current => $result) {
            ($this->observableCurrentExportFile)($total, $current);
            $cordinate1 = 'A';
            foreach ($columnsName as $colName) {
                $sheet->setCellValue($cordinate1 . $rowNumber, $result->$colName);
                $cordinate1++;
            }
            ($this->observable)($rowNumber, $total);
            $rowNumber++;
        }

        $writer = new Csv($spreadsheet);
        $name = $this->getFileName();
        $local = public_path('report-xls');
        if (!is_dir($local)) {
            mkdir($local, 0777, true);
        }
        if (file_exists($local . '/' . $name)) {
            return '/report-xls/' . $name;
        }

        $writer->save($this->file->base_path);

        ($this->observablePosExportFile)();
        return $this->file;
    }

    public function pdf()
    {
        $query = $this->getQueryReady();
        $results = $query->get();
        $total = counT($results);
        ($this->observablePreExportFile)($total);

        ($this->observable)(1, 2);
        $data = compact('results', 'total');
        $data['callback'] = $this->observableCurrentExportFile;
        $pdf = PDF::loadView('patrim.pdf', $data);
        ($this->observable)(2, 2);

        $name = $this->getFileName();
        $local = public_path('report-pdf');
        if (!is_dir($local)) {
            mkdir($local, 0777, true);
        }
        if (file_exists($local . '/' . $name)) {
            return '/report-pdf/' . $name;
        }

        $pdf->save($this->file->base_path);

        ($this->observablePosExportFile)();
        return $this->file;
    }

    public function pdfTable()
    {
        $query = $this->getQueryReady();
        $results = $query->get();
        $cols = $this->getCollsVisible();
        $titles = array_values($cols);
        $names = array_keys($cols);

        $total = counT($results);
        ($this->observablePreExportFile)($total);


        $data = compact('results', 'names', 'titles', 'total');
        $data['callback'] = $this->observableCurrentExportFile;
        $pdf = PDF::loadView('patrim.pdf-table',
            $data
        )->setPaper('a4', 'landscape');
        ($this->observable)(2, 2);

        $name = $this->getFileName();
        $local = public_path('report-pdf');

        if (!is_dir($local)) {
            mkdir($local, 0777, true);
        }


        $pdf->save($this->file->base_path);
        return $this->file;
    }

    public function sumValor()
    {
        $query = $this->getQueryReady();
        $results = $query->sum('siads_ext_patrim.it_va_liq_bem');
        return $results;
    }

    /**
     * @return \closure
     */
    public function getObservablePreExportFile(): \closure
    {
        return $this->observablePreExportFile;
    }

    /**
     * @param \closure $observablePreExportFile
     * @return SiadsPatrimFilter
     */
    public function setObservablePreExportFile(\closure $observablePreExportFile): SiadsPatrimFilter
    {
        $this->observablePreExportFile = $observablePreExportFile;
        return $this;
    }

    /**
     * @return \closure
     */
    public function getObservablePosExportFile(): \closure
    {
        return $this->observablePosExportFile;
    }

    /**
     * @param \closure $observablePosExportFile
     * @return SiadsPatrimFilter
     */
    public function setObservablePosExportFile(\closure $observablePosExportFile): SiadsPatrimFilter
    {
        $this->observablePosExportFile = $observablePosExportFile;
        return $this;
    }

    /**
     * @return \closure
     */
    public function getObservableCurrentExportFile(): \closure
    {
        return $this->observableCurrentExportFile;
    }

    /**
     * @param \closure $observableCurrentExportFile
     * @return SiadsPatrimFilter
     */
    public function setObservableCurrentExportFile(\closure $observableCurrentExportFile): SiadsPatrimFilter
    {
        $this->observableCurrentExportFile = $observableCurrentExportFile;
        return $this;
    }


    public function setObservable($observable)
    {
        $this->observable = $observable;
    }

}
