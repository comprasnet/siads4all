<?php
namespace App\Services;

use App\Repositories\MunicipioRepository;

class MunicipioService
{
    protected $municipioRepository;

    public function __construct(MunicipioRepository $municipioRepository)
    {
        $this->municipioRepository = $municipioRepository;
    }

    public function getMunicipiosByUF($uf)
    {
        return $this->municipioRepository->findByUF($uf);
    }
}