<?php

namespace App\Services;

use App\Repositories\EstadoRepository;

class EstadoService
{
    protected $estadoRepository;

    public function __construct(EstadoRepository $estadoRepository)
    {
        $this->estadoRepository = $estadoRepository;
    }

    public function getAll()
    {
        return $this->estadoRepository->all();
    }
}
