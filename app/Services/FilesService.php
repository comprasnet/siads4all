<?php

namespace App\Services;

use App\Exceptions\ExceptionWithUserMensage;
use App\Jobs\ReportPatrimony;
use App\Models\File;
use App\Models\Job;
use App\Models\User;
use App\Repositories\FilesRepository;
use App\Repositories\JobRepository;

class FilesService
{
    private $typesExtensions = [
        'excel' => 'xls',
        'csv' => 'csv',
        'txt' => 'txt',
        'pdfTable' => 'pdf',
        'pdf' => 'pdf',
    ];

    public function __construct(
        private JobRepository   $jobRepository,
        private NotificationService $notificationService,
        private FilesRepository $filesRepository
    )
    {
    }

    public function createReportPatrimonyFileServiceByDataRequest($type, $user, $request)
    {
        if(is_int($user)){
            $user = User::find($user);
        }
        $request['filename'] = $this->getName($request, $type);

        $job = new ReportPatrimony($type, $user, $request);
        $job->setMetadataFile(
            $request['filename'],
            'report-xls',
        );
        $file = $this->filesRepository->startFileCreate($job->getFileUrl(), $job->getAbsolutePath(), $request);
        $job->setFile($file);

        dispatch($job);


        $jobDb = $this->jobRepository->getJobByUUid($job->getUuid());

        $queuePosition = $this->jobRepository->getJobIdPosition($jobDb->id, $jobDb->queue);

        $file->job_id = $jobDb->id;
        $file->queue_position = $queuePosition;
        $file->queue_start = $queuePosition;
        $file->total_register = 0;
        $file->current_register = 0;
        $file->user_id = $user->id;
        $file->save();

        $fileLocation = explode('/',$file->file_location);
        $this->notificationService->createNotification(
            'Adicionado a fila relatório de patrimonio ' . end($fileLocation),
            '/files',
            [$user]
        );

        return $file;
    }

    private function getName($request, $type)
    {
        $extension = '';

        if (!isset($this->typesExtensions[$type])) {
            throw new ExceptionWithUserMensage('Erro, tipo de arquivo desconhecido');
        }
        if (empty($request['filename'])) {
            unset($request['filename']);
        }
        $extension = $this->typesExtensions[$type];
        $fileName = $request['filename'] ?? $this->generateFriendlyFileName('relatório_patrimonio');
        return $fileName . '.' . $extension;
    }

    public function generateFriendlyFileName($prefix = 'file')
    {
        // Obter timestamp atual
        $timestamp = date('Ymd_His'); // Exemplo: 20240903_143220

        // Gerar uma string aleatória de 8 caracteres
        $randomString = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 8);

        // Combinar o prefixo, timestamp e string aleatória
        $friendlyName = "{$prefix}_{$timestamp}_{$randomString}";

        return $friendlyName;
    }

    public function getPercentageProgressFile($fileId)
    {
        /** @var File $file * */
        /** @var Job $job * */
        $file = File::find($fileId);
        $job = Job::find($file->job_id);
        if ($file->complete or is_null($job)) {
            return 100;
        }
        $pct = 0;
        $currentPosition = $this->jobRepository->getJobIdPosition($job->id, $job->queue);

        if ($currentPosition == 1) {
            $pct = 50;
        } else {
            $walking = $file->queue_start - $currentPosition;
            $pctWalking = (100 * $walking) / $file->queue_start;
            $pct = 50 * ($pctWalking / 100);
        }
        if ($file->total_register == 0) {
            $pct2 = 0;
        } else {
            $pct2 = ($file->current_register * 100) / $file->total_register;
        }
        $pct += 50 * (round($pct2) / 100);
        $pct = ceil($pct*10)/10;
        if($pct>100){
            $pct = 100;
        }
        return $pct;
    }
}
