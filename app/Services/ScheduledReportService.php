<?php
namespace App\Services;

use App\Models\File;
use App\Repositories\ScheduledReportRepository;
use App\Jobs\ReportPatrimony;
use Carbon\Carbon;

class ScheduledReportService
{

    public function __construct(
        private ScheduledReportRepository $scheduledReportRepository,
        private FilesService $filesService
    )
    {
        $this->scheduledReportRepository = $scheduledReportRepository;
    }

    public function scheduleReport($name, $type, $user, $dataRequest, $date)
    {
        return $this->scheduledReportRepository->createSchedule($name, $type, $user, $dataRequest, $date);
    }

    public function processScheduledReports()
    {
        $reports = $this->scheduledReportRepository->getSchedulesForToday();

        foreach ($reports as $report) {
            try {
                $user = $report->user;
                $dataRequest = json_decode($report->data_request,true);
                // Cria e despacha o job
                $this->filesService->createReportPatrimonyFileServiceByDataRequest($report->type,$user->id, $dataRequest);

                // Atualiza o status para 'completed'
                $this->scheduledReportRepository->updateStatus($report, 'completed');

                if($report->file_id) {
                    /** @var File $file */
                    $file = File::find($report->file_id);
                    $file->delete();
                }

            } catch (\Exception $e) {
                // Se ocorrer algum erro, atualiza o status para 'failed'
                $this->scheduledReportRepository->updateStatus($report, 'failed');
            }
        }
    }
}
