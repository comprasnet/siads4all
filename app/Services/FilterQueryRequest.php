<?php

namespace App\Services;

use App\Models\File;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

abstract class FilterQueryRequest
{
    const TYPE_DATE = 'date';
    const TYPE_DATE_BETWEEN = 'data_between';
    const TYPE_DEFAULT = 'default';
    const TYPE_CUSTOM = 'custom';
    const TYPE_LIKE = 'like';
    const TYPE_RAW_DEFAULT = 'raw_default';

    protected $allowedFilters;
    protected $query;
    protected $defaultDbDate = 'Y-m-d';
    protected $filterData = [];
    /**
     * @var File
     */
    protected $file;

    public function __construct(
        private Request $request
    ) {
        $this->filterData = $this->request->all();
    }

    public function setFilterData($data)
    {
        $this->filterData = $data;
    }

    public function addFieldFilter($field, $type = self::TYPE_DEFAULT, $fieldDb = null, $callback = null)
    {
        if (is_null($fieldDb)) {
            $fieldDb = $field;
        }
        $this->allowedFilters[$field] = [
            'type' => $type,
            'field_db' => $fieldDb,
            'callback' => $callback
        ];
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     * @return FilterQueryRequest
     */
    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function getFilteredData()
    {
        $query = $this->query;

        $this->applyFilters($query, $this->request->all());
        $this->applyOrder($query);

        //die($query->toRawSql());

        // Aplicar paginação
        $length = $this->request->length ?? 10;
        $start = $this->request->start ?? 0;

        /* @var Builder $query */
        $data = (clone $query)->offset($start)->limit($length)->get();
        foreach ($data as $key => $line) {
            $line = $this->changeData($line);
            $data[$key] = $line;
        }

        // Retornar dados no formato esperado pelo DataTables
        return [
            'draw' => $this->request->draw,
            'recordsTotal' => $query->count(),
            'recordsFiltered' => $query->count(),
            'data' => $data
        ];
    }

    protected function applyOrder($query)
    {
        if ($this->request->has('order')) {
            foreach ($this->request->order as $order) {
                $columnIdx = $order['column'];
                $columnName = $this->request->columns[$columnIdx]['data'];
                $direction = $order['dir'];
                $query->orderBy($columnName, $direction);
            }
        }
    }

    protected function applyFilters($query)
    {
        /** @var \Illuminate\Database\Query\Builder $query */
        foreach ($this->allowedFilters as $field => $dataField) {
            if (isset($this->filterData[$field])) {
                $nameDb = $dataField['field_db'];
                $type = $dataField['type'];
                $callback = $dataField['callback'];
                $value = $this->filterData[$field];
                switch ($type) {
                    case self::TYPE_DATE:
                        if (!empty($value)) {
                            $query->whereDate($nameDb, '=', Carbon::createFromFormat('Y-m-d', $value)->format($this->defaultDbDate));
                        }
                        break;

                    case self::TYPE_CUSTOM:
                        if (!empty($value)) {

                        }
                        break;

                    case self::TYPE_DATE_BETWEEN:
                        $startField = $field . '_inicio';
                        $endField = $field . '_fim';
                        if (isset($filters[$startField]) && isset($filters[$endField])) {
                            if ($this->request->has($startField)) {
                                $startDate = Carbon::createFromFormat('Y-m-d', $this->request->input($endField))->format($this->defaultDbDate);
                                $query->where($nameDb, '>=', $startDate);
                            }

                            if ($this->request->has($endField)) {
                                $endDate = Carbon::createFromFormat('Y-m-d', $this->request->input($endField))->format($this->defaultDbDate);
                                $query->where($nameDb, '<=', $endDate);
                            }
                        }
                        break;

                    case self::TYPE_LIKE:
                        if (!empty($value)) {
                            $query->where($nameDb, 'ilike', '%' . $value . '%');
                        }
                        break;
                    case self::TYPE_RAW_DEFAULT:
                        if (!empty($value)) {
                            $query->where(DB::raw($nameDb), '=', $value);
                        }
                        break;

                    default:
                        if (!empty($value) or $value === "0") {
                            $query->where($nameDb, '=', $value);
                        }
                        break;
                }
            } elseif (isset($this->filterData[$field . '_inicio']) or isset($this->filterData[$field . '_fim'])) {
                $nameDb = $dataField['field_db'];
                $type = $dataField['type'];
                $startField = $field . '_inicio';
                $endField = $field . '_fim';
                if ($type == self::TYPE_DATE_BETWEEN) {
                    if (isset($this->filterData[$startField]) and !empty($this->filterData[$startField])) {
                        $startDate = Carbon::createFromFormat('Y-m-d', $this->filterData[$startField])->format($this->defaultDbDate);
                        $query->where($nameDb, '>=', $startDate);
                    }

                    if (isset($this->filterData[$endField]) and !empty($this->filterData[$endField])) {
                        $endDate = Carbon::createFromFormat('Y-m-d', $this->filterData[$endField])->format($this->defaultDbDate);
                        $query->where($nameDb, '<=', $endDate);
                    }
                }
            }
        }
    }

    public function getQueryReady()
    {
        $query = $this->getQuery();
        $this->applyFilters($query);
        $this->applyOrder($query);
        return $query;
    }

    protected function changeData($line)
    {
        return $line;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return FilterQueryRequest
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

}
