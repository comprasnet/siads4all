<?php

namespace App\Services;

use App\Exceptions\ExceptionWithUserMensage;
use App\Helper\JsonResponse;
use App\Models\Unidade;
use App\Repositories\UnidadesRepository;
use App\Repositories\UnitAccessRequestRepository;

class UnitAccessRequestService
{
    public function __construct(
        private NotificationService $notificationService,
        private UserService $userService,
        private UnitAccessRequestRepository $unitAccessRequestRepository,
        private UnidadesRepository $unidadesRepository,
        private PermissionsService $permissionsService,
        private RolesService $rolesService,
        private JsonResponse $jsonResponse
    ) {
    }

    public function request($unitCod, $user)
    {
        $unit = Unidade::query()->where('id', '=', $unitCod)->first();
        if (!$unit) {
            throw new ExceptionWithUserMensage('Unidade não encontrada com o seguinte Código: ' . $unitCod);
        }

        $checkExists = $this->unidadesRepository->checkExistsUserByUnit($user->id,$unit->id);
        if ($checkExists) {
            throw new ExceptionWithUserMensage('O Usuário '.$user->name.' já possui acesso a unidade ' . $unitCod);
        }

        $unitAccessRequest = $this->unitAccessRequestRepository->getAccessRequest($user->id, $unit->id);
        if ($unitAccessRequest) {
            $this->jsonResponse->setSuccess(
                'Solicitação de acesso a unidade' .
                $unitCod . ' - ' . $unit->nome . ' já foi registrada em ' .
                $unitAccessRequest->created_at->format('d/m/Y') . ' às ' .
                $unitAccessRequest->created_at->format('H:i') . ' e pendente de análise'
            );
            return $this->jsonResponse;
        }

        $this->unitAccessRequestRepository->createUnitAccessRequest($user->id, $unit->id);

        $this->notificationService->createNotification(
            "Usuário {$user->name} solicitou acesso a " . $unit->nome,
            '/unit/approve',
            $this->userService->getUserWithAdminUserPermissionByUnit($unit->id)
        );
        $this->jsonResponse->setSuccess('Solicitação enviada');
        return $this->jsonResponse;
    }

    public function approve($unitAccessRequestId, $permissions, $roles)
    {
        $unitAccessRequest = $this->unitAccessRequestRepository->find($unitAccessRequestId);
        if (is_null($unitAccessRequest)) {
            throw new ExceptionWithUserMensage('Solicitação não encontrada');
        }

        $unit = Unidade::find($unitAccessRequest->unit_id);
        if (is_null($unitAccessRequest)) {
            throw new ExceptionWithUserMensage('Unidade não encontrada');
        }

        $this->unidadesRepository->approveUser($unitAccessRequest);
        $this->permissionsService->savePermissions(
            $permissions,
            $unitAccessRequest->unit_id,
            $unitAccessRequest->user_id
        );
        $this->rolesService->saveRoles(
            $roles,
            $unitAccessRequest->unit_id,
            $unitAccessRequest->user_id
        );
        $this->unitAccessRequestRepository
            ->updateStatusByUserAndUnitWithPendingStatus(
                $unitAccessRequest->unit_id,
                $unitAccessRequest->user_id,
                UnitAccessRequestRepository::STATUS_APPROVED
            );

        $this->notificationService->createNotification(
            'Solicitação de acesso a unidade ' . $unit->codigo . ' - ' . $unit->nome . ' foi aprovada',
            url('/'),
            [$unitAccessRequest->user_id]
        );
        $this->jsonResponse->setSuccess("Usuário Aprovado com sucesso");
        return $this->jsonResponse;
    }

    public function block($id)
    {
        $unitAccessRequest = $this->unitAccessRequestRepository->find($id);
        if (is_null($unitAccessRequest)) {
            throw new ExceptionWithUserMensage('Solicitação não encontrada');
        }

        $unit = Unidade::find($unitAccessRequest->unit_id);
        if (is_null($unitAccessRequest)) {
            throw new ExceptionWithUserMensage('Unidade não encontrada');
        }

        $this->unitAccessRequestRepository
            ->updateStatusByUserAndUnitWithPendingStatus(
                $unitAccessRequest->unit_id,
                $unitAccessRequest->user_id,
                UnitAccessRequestRepository::STATUS_REFUSED
            );

        $this->notificationService->createNotification(
            'Solicitação de acesso a unidade ' . $unit->codigo . ' - ' . $unit->nome . ' foi recusado',
            '#',
            [$unitAccessRequest->user_id]
        );
        $this->jsonResponse->setSuccess("Usuário Recusado com sucesso");

        return $this->jsonResponse;
    }
}
