<?php

namespace App\Services;

use App\Models\QuickAccess;

class QuickAccessService
{
    public function __construct(
        private PermissionsService $permissionsService,
        private UnidadeChangerService $unidadeChangerService
    )
    {
    }

    public function checkPermissions(QuickAccess $quickAccess){
        if(is_null($quickAccess->permissions) or empty($quickAccess->permissions)){
            return true;
        }
        $permissions = explode(',',$quickAccess->permissions);
        $unit = $this->unidadeChangerService->getCurrentUnidade();
        return $this->permissionsService->hasPermissions($permissions,auth()->id(),$unit->id);
    }
}
