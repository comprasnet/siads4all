<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\PermissionsRepository;

class PermissionsService
{
    const ADMIN_USERS = 'admin_users';

    public function __construct(
        private PermissionsRepository $permissionsRepository,
        private UnidadeChangerService $unidadeChangerService,
    )
    {
    }

    public function getPermissionByRole($roles)
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }
        return $this->permissionsRepository->getPermissionByRole($roles);
    }

    public function savePermissions($permissions, $unit, $user)
    {
        $this->permissionsRepository->setPermissionByUnitAndUser(
            $permissions, $unit, $user
        );
    }

    public function hasPermission($permissio, ?int $userId = null, ?int $unitId = null)
    {
        if (is_null($userId)) {
            $userId = auth()->id();
        }
        if (is_null($unitId)) {
            $unit = $this->unidadeChangerService->getCurrentUnidade();
            $unitId = $unit?$unit->id:null;
        }
        if(is_null($unitId)){
            return false;
        }
        $user = User::find($userId);
        if($user->super_admin){
            return true;
        }
        if(!is_array($permissio)){
            $permissio = [$permissio];
        }
        return $this->permissionsRepository->hasPermission($permissio, (int)$userId, (int)$unitId);
    }

    public function hasPermissions(array $permissions, ?int $userId = null, ?int $unitId = null)
    {
        if (is_null($userId)) {
            $userId = auth()->id();
        }
        if (is_null($unitId)) {
            $unit = $this->unidadeChangerService->getCurrentUnidade();
            $unitId = $unit?$unit->id:null;
        }
        if(is_null($unitId)){
            return false;
        }
        $user = User::find($userId);
        if($user->super_admin){
            return true;
        }
        return $this->permissionsRepository->hasPermissions($permissions, (int)$userId, (int)$unitId);
    }
}
