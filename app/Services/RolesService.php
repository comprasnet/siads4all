<?php

namespace App\Services;

use App\Repositories\PermissionsRepository;
use App\Repositories\RolesRepository;
use Illuminate\Support\Facades\Auth;

class RolesService
{
    public function __construct(
        private RolesRepository $rolesRepository
    )
    {
    }


    public function saveRoles($roles,$unit,$user)
    {
        $this->rolesRepository->saveRoleByUnitAndUser(
            $roles,$unit,$user
        );
    }
    public function getRolesForCurrentUser()
    {
        $userId = Auth::id();
        return $this->rolesRepository->getRolesByIdOrAbove($userId);
    }

}
