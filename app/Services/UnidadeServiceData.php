<?php

namespace App\Services;

class UnidadeServiceData
{
    protected $administracaoOptions = [
        1 => 'ADMINISTRAÇÃO DIRETA',
        2 => 'ESTATAL',
        3 => 'AUTARQUIA',
        4 => 'FUNDAÇÃO',
        5 => 'EMPRESA PÚBLICA COM. E FIN.',
        6 => 'ECONOMIA MISTA',
        7 => 'FUNDOS',
        8 => 'EMPRESA PUBLICA INDUSTRIAL E AGRICOLA',
        11 => 'ADMINISTRACAO DIRETA ESTADUAL',
        12 => 'ADMINISTRACAO DIRETA MUNICIPAL',
        13 => 'ADMINISTRACAO INDIRETA ESTADUAL',
        14 => 'ADMINISTRACAO INDIRETA MUNICIPAL',
        15 => 'EMPRESA PRIVADA'
    ];

    protected $poderOptions = [
        0 => 'Executivo',
        1 => 'Legislativo',
        2 => 'Judiciário'
    ];

    protected $esferaOptions = [
        'F' => 'Federal',
        'E' => 'Estadual',
        'M' => 'Municipal'
    ];

    protected $tipoOptions = [
        0 => 'Executora',
        1 => 'Controle',
        2 => 'Credora'
    ];

    public function getAdministracaoOptions($key = null)
    {
        return $key !== null ? ($this->administracaoOptions[$key] ?? null) : $this->administracaoOptions;
    }

    public function getPoderOptions($key = null)
    {
        return $key !== null ? ($this->poderOptions[$key] ?? null) : $this->poderOptions;
    }

    public function getEsferaOptions($key = null)
    {
        return $key !== null ? ($this->esferaOptions[$key] ?? null) : $this->esferaOptions;
    }

    public function getTipoOptions($key = null)
    {
        return $key !== null ? ($this->tipoOptions[$key] ?? null) : $this->tipoOptions;
    }
}
