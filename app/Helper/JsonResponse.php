<?php

namespace App\Helper;

use Error;

class JsonResponse
{
    const TYPE_ERROR = 'error';
    const TYPE_SUCCESS = 'success';

    private $messageResponse;
    private $typeResponse;

    public function jsonSuccess($message = null)
    {
        $response = ['success' => true];
        if ($message) {
            $response['message'] = $message;
        }
        return $response;
    }

    public function jsonError($message)
    {
        return [
            'success' => false,
            'message' => $message
        ];
    }

    public function jsonDataSuccess($message = null)
    {
        $response = ['success' => true];
        if ($message) {
            $response['data'] = $message;
        }
        return $response;
    }


    public function responseSuccess($message = null)
    {
        $success = $this->jsonSuccess($message);
        return response()->json($success);
    }

    
    public function responseDataSuccess($message = null)
    {
        $success = $this->jsonDataSuccess($message);
        return response()->json($success);
    }

    public function responseJsonError($message)
    {
        $error = $this->jsonError($message);
        return response()->json($error);
    }

    public function setError($message)
    {
        $this->messageResponse = $message;
        $this->typeResponse = self::TYPE_ERROR;
    }

    public function setSuccess($message)
    {
        $this->messageResponse = $message;
        $this->typeResponse = self::TYPE_SUCCESS;
    }

    public function getMessageArray()
    {
        if ($this->messageResponse) {
            if ($this->typeResponse == self::TYPE_ERROR) {
                return $this->jsonError($this->messageResponse);
            }
            return $this->jsonSuccess($this->messageResponse);
        }
        return null;
    }

    public function getMessageResonseJson()
    {
        $messageArray = $this->getMessageArray();
        if ($messageArray) {
            return response()->json($messageArray);
        }
        return null;
    }


    public function getMessageResponse()
    {
        return $this->messageResponse;
    }

    public function getTypeResponse()
    {
        return $this->typeResponse;
    }

    public function setMessageResponse($messageResponse): void
    {
        $this->messageResponse = $messageResponse;
    }

    public function setTypeResponse($typeResponse): void
    {
        $this->typeResponse = $typeResponse;
    }
}
