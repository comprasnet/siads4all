<?php

namespace App\Helper\Routes;

use Illuminate\Support\Facades\Route;

class Resource
{
    public static function resource($route,$controller,$name, $paramIdName='id'){
        return Route::resource($route,$controller)
                ->names($name)
                ->parameters([$name=>$paramIdName]);
    }
}
