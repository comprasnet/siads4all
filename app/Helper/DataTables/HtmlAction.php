<?php

namespace App\Helper\DataTables;

class HtmlAction extends BaseAction
{

    private $dataView = [];
    private $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function render():string
    {
        $data = $this->dataView;
        $data['data'] = $this->data;
        $data['id']='id'.md5(uniqid(time(),true));
        $view = view($this->view, $data);
        $render = $view->render();

        foreach ($this->data as $name => $value) {
            $render = str_replace('@' . $name, $value, $render);
        }
        return $render;
    }

    /**
     * @return array
     */
    public function getDataView(): array
    {
        return $this->dataView;
    }

    /**
     * @param array $dataView
     * @return HtmlAction
     */
    public function setDataView(array $dataView): HtmlAction
    {
        $this->dataView = $dataView;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param mixed $view
     * @return HtmlAction
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

}
