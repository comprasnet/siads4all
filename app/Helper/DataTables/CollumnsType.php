<?php

namespace App\Helper\DataTables;

use Yajra\DataTables\EloquentDataTable;

class CollumnsType
{
    public static function typeDate(EloquentDataTable $eloquentDataTable,string $column){
        return $eloquentDataTable->editColumn($column, function($model) use ($column) {
            try{
                $date = new \DateTime($model->$column);
                return $date->format('d/m/Y');
            }catch (\Exception $e){
                return '';
            }
        });
    }

    public static function typeDateTime(EloquentDataTable $eloquentDataTable,string $column){
        return $eloquentDataTable->editColumn($column, function($model) use ($column) {
            try{
                $date = new \DateTime($model->$column);
                return $date->format('d/m/Y H:i:s');
            }catch (\Exception $e){
                return '';
            }
        });
    }
}
