<?php

namespace App\Helper\DataTables;

use App\Traits\RouterTrait;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BaseDatatables extends DataTable
{
    use RouterTrait;

    const UPDATE = 'update';
    const DELETE = 'trash';

    private $datatableColumns = [];

    protected $defaults = [self::UPDATE, self::DELETE];
    /**
     * @var ButtonAction[]
     */
    protected $actionsButtons = [];

    private $editRouteName = 'edit-item';
    private $removeRouteName = 'remove';

    public function setActionLink(ButtonAction $button)
    {
        $this->actionsButtons[] = $button;
    }

    public function getHtmlActions($id,$data)
    {
        $html = '';
        foreach ($this->actionsButtons as $action) {
            $action2 = clone $action;
            $action2->setId($id);
            $action2->setData($data);
            $html .= $action2->render();
        }
        $html = '<div class="actions-btns">'.$html.'</div>';
        return $html;
    }

    public function setDefauls()
    {
        $editRoute = $this->getRouteResourceByNameOnCurrentRouteOrPath('edit', ['id' => '@id']);
        $deleteRoute = $this->getRouteResourceByNameOnCurrentRouteOrPath('destroy', ['id' => '@id']);
        $viewRoute = $this->getRouteResourceByNameOnCurrentRouteOrPath('show', ['id' => '@id']);

        if (!is_null($editRoute) and in_array(self::UPDATE, $this->defaults)) {
            $this->actionsButtons[] = (new ButtonAction('edit'))
                ->setIcon('fa fa-edit')
                ->setLink($editRoute);
            if (!is_null($editRoute) and in_array(self::UPDATE, $this->defaults)) {
                $this->actionsButtons[] = (new ButtonView($viewRoute,$editRoute));
            }
        }




        if (!is_null($deleteRoute) and in_array(self::UPDATE, $this->defaults)) {
            $this->actionsButtons[] = (new ButtonAction('delete'))
                ->setIcon('fa fa-trash')
                ->setJs('remoteItemTable(@id,this)')
                ->setUrl($deleteRoute);
        }

        return $this;
    }

    /**
     * @return ButtonAction[]
     */
    public function getActionsButtons(): array
    {
        return $this->actionsButtons;
    }

    /**
     * @param ButtonAction[] $actionsButtons
     * @return BaseDatatables
     */
    public function setActionsButtons(array $actionsButtons): BaseDatatables
    {
        $this->actionsButtons = $actionsButtons;
        return $this;
    }

    public function withoutPermission()
    {
        $this->actionsButtons = [];
    }

    public function deleteAndUpdatePermissions()
    {
        $this->actionsButtons = [self::UPDATE, self::DELETE];
    }

    public function deletePermission()
    {
        $this->actionsButtons = [self::DELETE];
    }


    public function updatePermission()
    {
        $this->actionsButtons = [self::UPDATE];
    }

    public function cuntPermissions()
    {
        return count($this->actionsButtons);
    }

    public function setActionCollumn(EloquentDataTable $datatable)
    {
        if (count($this->actionsButtons) > 0) {
            $datatable = $datatable->editColumn('action', function ($data) {
                return $this->getHtmlActions($data->id,$data->toArray());
            });
            return $datatable;
        }
        return $datatable;
    }

    protected function setupColumns()
    {

    }

    public function addColumns(Column $column)
    {
        $this->datatableColumns[] = $column;
    }

    protected function getColumns()
    {
        $this->setupColumns();
        if (count($this->actionsButtons) > 0) {
            $this->addColumns(
                Column::computed('action')->title('Ações')
                    ->exportable( FALSE )
                    ->printable( FALSE )
                    ->addClass('td-data-action')
            );
        }
        return $this->datatableColumns;
    }
}
