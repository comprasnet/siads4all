<?php

namespace App\Helper\DataTables;

abstract class BaseAction
{

    protected $data = [];
    protected $id;

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return BaseAction
     */
    public function setData(array $data): BaseAction
    {
        $this->data = $data;
        return $this;
    }

    public function render():string
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BaseAction
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


}
