<?php

namespace App\Helper\DataTables;

class ButtonAction extends BaseAction
{
    const TYPE_DEFAULT = 'default';
    const TYPE_PRIMARY = 'primary';
    const TYPE_SECONDARY = 'secondary';
    const TYPE_WARNING = 'warning';
    const TYPE_INFORMATION = 'information';
    const TYPE_DANGER = 'danger';

    private $name;
    private $title;
    private $icon;
    private $js;
    private $link = null;
    private $url;
    private $type = self::TYPE_DEFAULT;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function render():string
    {
        if ($this->link) {
            $this->link = str_replace('@id', $this->id, $this->link);
        }
        if ($this->js) {
            $this->js = str_replace('@id', $this->id, $this->js);
        }

        if ($this->url) {
            $this->url = str_replace('@id', $this->id, $this->url);
        }
        $view = view('part.datatables.btn-actions', ['button' => $this]);
        $render = $view->render();

        foreach ($this->data as $name => $value) {
            $render = str_replace('@' . $name, $value, $render);
        }
        return $render;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return ButtonAction
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return ButtonAction
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return ButtonAction
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return ButtonAction
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJs()
    {
        return $this->js;
    }

    /**
     * @param mixed $js
     * @return ButtonAction
     */
    public function setJs($js)
    {
        $this->js = $js;
        return $this;
    }

    /**
     * @return null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param null $link
     * @return ButtonAction
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }



    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ButtonAction
     */
    public function setType(string $type): ButtonAction
    {
        $this->type = $type;
        return $this;
    }

}
