<?php

namespace App\Traits;

trait Mask
{
    protected function maskCPF($cpf){
        $cpf = str_replace(['-','.'],'',$cpf);
        $cpf = substr($cpf,3,6);
        return "***.".substr($cpf,0,3).'.'.substr($cpf,3,3).'-**';
    }

    protected function maskCNPJ($cnpj) {
        // Remove caracteres indesejados
        $cnpj = str_replace(['-', '.', '/'], '', $cnpj);

        // Mascarando a exibição
        return substr($cnpj, 0, 2)."." . substr($cnpj, 2, 3) . "." . substr($cnpj, 5, 3) . "/".substr($cnpj, 8, 4)."-" . substr($cnpj, -2);
    }
}
