<?php

namespace App\Traits;

use Illuminate\Support\Facades\Route;

trait RouterTrait
{
    public function getRouteResourceByNameOnCurrentRoute($name,$params){
        $paths = explode('.',Route::current()->getName());
        $nameRouteBase = $paths[0];
        $nameRouteBase .= '.'.$name;
        if(Route::has($nameRouteBase)) {
            return \route($nameRouteBase, $params);
        }
        return null;
    }

    public function getRouteResourceByNameOnCurrentRouteOrPath($name, $params=[])
    {
        $url = $this->getRouteResourceByNameOnCurrentRoute($name,$params);
        if(is_null($url)){
            if(substr($name,0,1)=='/' or strtolower(substr($name,0,4)) == 'http'){
                $url = $name;
            }
        }
        return $url;
    }
}
