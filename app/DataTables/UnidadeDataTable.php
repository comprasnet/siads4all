<?php

namespace App\DataTables;

use App\Helper\DataTables\BaseDatatables;
use App\Models\Unidade;
use App\Services\UnidadeServiceData;
use Doctrine\DBAL\Query\QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class UnidadeDataTable extends BaseDatatables
{
    public function __construct()
    {
        $this->setDefauls();
        parent::__construct();
    }


    public function dataTable($query): EloquentDataTable
    {

        $unidadeServiceData = new UnidadeServiceData;

        $eloquentDataTable = (new EloquentDataTable($query));
        $eloquentDataTable = $this->setActionCollumn($eloquentDataTable);
        $eloquentDataTable->setRowId('id');

        $eloquentDataTable->filter(function ($query) {
            if (request()->has('search')) {
                $searchValue = request('search')['value'];
                // Customizar a busca aqui
                $query->where('unidades.nome', 'like', "%{$searchValue}%")
                    ->orWhere('unidades.codigosiasg', 'like', "%{$searchValue}%")
                    ->orWhere('unidades.codigosiafi', 'like', "%{$searchValue}%")
                    ->orWhere('unidades.nomeresumido', 'like', "%{$searchValue}%")
                    ->orWhere('unidades.cnpj', 'like', "%{$searchValue}%");
            }
        });
        $eloquentDataTable = $eloquentDataTable->editColumn('esfera', function($unidade) use($unidadeServiceData) {
            return $unidadeServiceData->getEsferaOptions($unidade->esfera);
        });
        $eloquentDataTable = $eloquentDataTable->editColumn('despacho_autorizatorio', function($unidade) {
            return $unidade->despacho_autorizatorio?'Sim':'Não';
        });
        $eloquentDataTable = $eloquentDataTable->editColumn('poder', function($unidade) use($unidadeServiceData) {
            return $unidadeServiceData->getPoderOptions($unidade->poder);
        });
        $eloquentDataTable = $eloquentDataTable->editColumn('tipo_adm', function($unidade) use($unidadeServiceData) {
            return $unidadeServiceData->getAdministracaoOptions($unidade->tipo_adm);
        });
        $eloquentDataTable = $eloquentDataTable->editColumn('tipo', function($unidade) use($unidadeServiceData) {
            return $unidadeServiceData->getTipoOptions($unidade->tipo??'');
        });

        return $eloquentDataTable;
    }

    public function query(Unidade $model)
    {
        return $model->newQuery()
            ->select([
                'unidades.id',
                'unidades.codigo',
                'unidades.gestao',
                'unidades.nome',
                'unidades.despacho_autorizatorio',
                'unidades.codigosiasg',
                'unidades.codigosiafi',
                'unidades.nomeresumido',
                'orgaos.nome as orgao_nome',
                'unidades.cnpj',
                'unidades.telefone',
                'unidades.tipo',
                'unidades.sisg',
                'estados.sigla as municipio_uf',
                'municipios.nome as municipio_nome',
                'unidades.esfera',
                'unidades.poder',
                'unidades.tipo_adm',
                'unidades.aderiu_siasg',
                'unidades.utiliza_siafi',
                'unidades.utiliza_custos'
            ])
            ->join('orgaos', 'unidades.orgao_id', '=', 'orgaos.id')
            ->leftJoin('municipios', 'unidades.municipio_id', '=', 'municipios.id')
            ->leftJoin('estados', 'municipios.estado_id', '=', 'estados.id');
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'order' => [[2, 'asc']],
                'scrollX' => true,
                'scrollY'=>'500px'
            ]);
    }

    public function setupColumns()
    {
        //$this->addColumns(Column::make('id')->title('Id')->visible(false));
        $this->addColumns(Column::make('codigo')->title('UASG SIASG'));
        $this->addColumns(Column::make('gestao')->title('Gestão'));
        $this->addColumns(Column::make('nome')->title('Nome'));
        $this->addColumns(Column::make('despacho_autorizatorio')->title('Despacho Autorizatório'));
        //$this->addColumns(Column::make('codigosiafi')->title('UG SIAFI'));
        $this->addColumns(Column::make('nomeresumido')->title('Nome Resumido'));
        $this->addColumns(Column::make('orgao_nome')->title('Órgão'));
        $this->addColumns(Column::make('cnpj')->title('CNPJ'));
        //$this->addColumns(Column::make('telefone')->title('Telefone'));
        //$this->addColumns(Column::make('tipo')->title('Tipo'));
        $this->addColumns(Column::make('sisg')->title('Sisg')->render('function() { return this.sisg ? "Ativo" : "Inativo"; }'));
        //$this->addColumns(Column::make('municipio_uf')->title('UF'));
        //$this->addColumns(Column::make('municipio_nome')->title('Município'));
        $this->addColumns(Column::make('esfera')->title('Esfera'));
        $this->addColumns(Column::make('poder')->title('Poder'));
        $this->addColumns(Column::make('tipo_adm')->title('Administração'));
        $this->addColumns(Column::make('aderiu_siasg')->title('SIASG')->render('function() { return this.aderiu_siasg ? "Sim" : "Não"; }'));
        $this->addColumns(Column::make('utiliza_siafi')->title('SIAFI')->render('function() { return this.utiliza_siafi ? "Sim" : "Não"; }'));
        $this->addColumns(Column::make('utiliza_custos')->title('Utiliza Custos SIAFI')->render('function() { return this.utiliza_custos ? "Sim" : "Não"; }'));
    }


    protected function filename(): string
    {
        return 'Unidades_' . date('YmdHis');
    }
}
