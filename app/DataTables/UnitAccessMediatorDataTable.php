<?php

namespace App\DataTables;

use App\Helper\DataTables\BaseDatatables;
use App\Helper\DataTables\ButtonAction;
use App\Models\UnitAccessMediator;
use App\Models\UnitAccessRequest;
use App\Services\UnidadesService;
use App\Traits\Mask;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class UnitAccessMediatorDataTable extends BaseDatatables
{
    use Mask;
    public function __construct()
    {
        $this->setTableButton();
        parent::__construct();
    }

    private function setTableButton()
    {
        $this->actionsButtons[] = (new ButtonAction('approve'))
            ->setIcon('fa fa-check')
            ->setJs('approveUser(@id)');

            $this->actionsButtons[] = (new ButtonAction('approve'))
            ->setIcon('fa fa-ban')
            ->setJs('blockUser(@id)');
    }

    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $eloquentDataTable = (new EloquentDataTable($query));
        $eloquentDataTable = $this->setActionCollumn($eloquentDataTable);
        $eloquentDataTable->setRowId('id');
        $eloquentDataTable->editColumn('cpf', function($model){
            return $this->maskCPF($model->cpf);
        });

        return $eloquentDataTable;
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(UnitAccessRequest $model): QueryBuilder
    {
        $user = auth()->user();
        $query = $model->select([
            DB::raw('(SELECT u2.id FROM unit_access_requests as u2 WHERE u2.unit_id = unit_access_requests.unit_id AND u2.user_id = unit_access_requests.user_id ORDER BY u2.id DESC LIMIT 1) as id'),
            'users.name as user',
            'users.cpf as cpf',
            'users.email as email',
            DB::raw("CONCAT(unidades.codigo, ' - ', unidades.nome) as unit")
        ])
            ->join('users', 'users.id', '=', 'unit_access_requests.user_id')
            ->join('unidades', 'unidades.id', '=', 'unit_access_requests.unit_id')
            ->leftJoin('unidadesusers', function ($join) {
                $join->on('unidadesusers.unidade_id', '=', 'unidades.id')
                    ->on('unidadesusers.user_id', '=', 'users.id');
            })
            ->where('unit_access_requests.status',0)
            ->where('users.id','!=',auth()->id())
            ->where('users.super_admin','!=',true)
            ->whereNull('unidadesusers.unidade_id')
            ->whereNull('unit_access_requests.deleted_at');
        if (!$user->super_admin) {
            $query->where(function ($query) use ($user) {
                $query->select(DB::raw('COUNT(*)'))
                    ->from('user_permission')
                    ->whereColumn('user_permission.unit_id', 'unit_access_requests.unit_id')
                    ->where('user_permission.user_id', $user->id);
            }, '>', 0);
        }
        $query->distinct()
            ->get();
        return $query;
    }

    public function count(){
        return $this->query(new UnitAccessRequest())->count();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        $html =  $this->builder()
            ->setTableId('unitaccessmediator-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->dom('Bfrtip')
            ->orderBy(0, 'ASC')
            ->selectStyleSingle()
            ->buttons([
                Button::make('excel'),
                Button::make('csv'),
                Button::make('pdf'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            ]);
        return $html;
    }


    /**
     * Get the dataTable columns definition.
     */
    public function setupColumns()
    {
        $this->addColumns(Column::make('id')->title('Cod.'));
        $this->addColumns(Column::make('user')->title('User'));
        $this->addColumns(Column::make('cpf')->title('CPF'));
        $this->addColumns(Column::make('email')->title('Email'));
        $this->addColumns(Column::make('unit', 'unit')->title('Unit'));
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'UnitAccessMediator_' . date('YmdHis');
    }
}
