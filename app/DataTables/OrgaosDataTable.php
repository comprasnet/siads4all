<?php

namespace App\DataTables;

use App\Helper\DataTables\BaseDatatables;
use App\Models\Orgao;
use App\Repositories\OrgaoSuperiorRepository;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrgaosDataTable extends BaseDatatables
{

    public function __construct(
        private OrgaoSuperiorRepository $orgaoSuperiorRepository
    )
    {
        $this->setDefauls();
        parent::__construct();
    }

    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $eloquentDataTable = (new EloquentDataTable($query));
        $eloquentDataTable = $this->setActionCollumn($eloquentDataTable);
        $eloquentDataTable->setRowId('id');
        $orgaoSuperiorRepository = $this->orgaoSuperiorRepository;

        $eloquentDataTable = $eloquentDataTable->editColumn('situacao', function($model)  {
            return $model->situacao?'Ativa':'Inativa';
        });


        return $eloquentDataTable;
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Orgao $model): QueryBuilder
    {
        $query =  $model->newQuery();
        $query->join('orgaossuperiores','orgaossuperiores.id','=','orgaos.orgaosuperior_id');
        $query->select([
            'orgaos.id',
            'orgaos.codigo',
            'orgaos.codigosiasg',
            'orgaos.nome',
            'orgaossuperiores.nome as orgaosuperior',
            'orgaos.cnpj',
            'orgaos.situacao',
            'orgaos.gestao',
            DB::raw('CASE WHEN COALESCE(CAST(orgaos.utiliza_centro_custo AS INTEGER), 0) = 1 THEN \'Sim\' ELSE \'Não\' END as utiliza_centro_custo')
        ]);

        return $query;
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('orgaos-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0,'asc')
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function setupColumns()
    {
        $this->addColumns(Column::make('id')->hidden());
        $this->addColumns(Column::make('codigo')->title('Código SIAFI'));
        $this->addColumns(Column::make('codigosiasg')->title('Código SIASG'));
        $this->addColumns(Column::make('nome')->title('Nome'));
        $this->addColumns(Column::make('orgaosuperior')->title('Orgão Superior')->searchable(false));
        $this->addColumns(Column::make('cnpj')->title('CNPJ'));
        $this->addColumns(Column::make('situacao')->title('Situação'));
        $this->addColumns(Column::make('gestao')->title('Gestão'));
        $this->addColumns(Column::make('utiliza_centro_custo')->title('Utiliza Centro de custo'));
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Orgaos_' . date('YmdHis');
    }
}
