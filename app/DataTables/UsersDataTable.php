<?php

namespace App\DataTables;

use App\Helper\DataTables\BaseDatatables;
use App\Helper\DataTables\CollumnsType;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends BaseDatatables
{
    public function __construct()
    {
        $this->setDefauls();
        parent::__construct();
    }

    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $eloquentDataTable = (new EloquentDataTable($query));
        $eloquentDataTable = $this->setActionCollumn($eloquentDataTable);
        $eloquentDataTable->setRowId('id');

        return $eloquentDataTable;
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(User $model): QueryBuilder
    {
        $query = $model->newQuery();
        return $query;
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('users-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            //->dom('Bfrtip')
            ->orderBy(0, 'ASC')
            ->selectStyleSingle()
            ->buttons([
                Button::make('excel'),
                Button::make('csv'),
                Button::make('pdf'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function setupColumns()
    {
        $this->addColumns(Column::make('id')->title('Cod.'));
        $this->addColumns(Column::make('name', 'name')->title('Nome'));
        $this->addColumns(Column::make('cpf', 'cpf')->title('CPF'));
        $this->addColumns(Column::make('email', 'email')->title('Email'));
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Users_' . date('YmdHis');
    }
}
