<?php

namespace App\DataTables;

use App\Helper\DataTables\BaseDatatables;
use App\Helper\DataTables\ButtonAction;
use App\Helper\DataTables\HtmlAction;
use App\Models\File;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class FileDataTable extends BaseDatatables
{
    public function __construct()
    {
        $this->setActionBtns();
        parent::__construct();
    }

    public function setActionBtns(){

        $this->actionsButtons = [(new HtmlAction('files.progress'))];
    }

    /**
     * Build the DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $eloquentDataTable = (new EloquentDataTable($query));
        $eloquentDataTable = $this->setActionCollumn($eloquentDataTable);
        $eloquentDataTable->setRowId('id');

        $eloquentDataTable = $eloquentDataTable->editColumn('file_location',function($files){
            $name = explode('/',$files->file_location);
            return end($name);
        });

        $eloquentDataTable = $eloquentDataTable->editColumn('created_at',function($files){
            return $files->created_at->format('d/m/Y');
        });

        $eloquentDataTable = $eloquentDataTable->editColumn('data_agendamento',function($files){
            return is_null($files->data_agendamento)?'-':$files->data_agendamento->format('d/m/Y');
        });


        return $eloquentDataTable;
    }

    /**
     * Get the query source of dataTable.
     *
     * @param \App\Models\File $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(File $model)
    {
        $userId = auth()->user()->id;
        return $model->newQuery()
            ->where('user_id', $userId);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('files-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(2,'desc');
    }


    /**
     * Get the dataTable columns definition.
     */
    public function setupColumns()
    {
        $this->addColumns(Column::make('id')->title('Cod.'));
        $this->addColumns(Column::make('file_location')->title('Nome'));
        $this->addColumns(Column::make('created_at')->title('Data de solicitação'));
        $this->addColumns(Column::make('data_agendamento')->title('Data de agendamento'));
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename():string
    {
        return 'Files_' . date('YmdHis');
    }
}
