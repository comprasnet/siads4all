<?php

namespace App\Providers;

use App\Services\UnidadesService;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Yajra\DataTables\Html\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Builder::useVite();
        if (config('app.protocolo_http') === 'https') {
            \URL::forceScheme('https');
        }

        if ( !app()->environment('testing') &&
            Config::get("app.app_amb") != 'Ambiente Produção' &&
            Config::get("app.app_amb") != 'Ambiente Homologação' &&
            Config::get("app.app_amb") != 'Ambiente Treinamento') {
            $appPath = Config::get("app.app_path");

            $stringfromfile = file("{$appPath}.git/HEAD", FILE_USE_INCLUDE_PATH);
            $firstLine = $stringfromfile[0]; //get the string from the array

            $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string

            $branchname = $explodedstring[2]; //get the one that is always the branch name
            Config::set('app.app_version', $branchname);
        }

    }


}
