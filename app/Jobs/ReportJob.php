<?php

namespace App\Jobs;

use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

abstract class ReportJob
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const BASE_PUBLIC = 'public';
    const BASE_STORAGE = 'storage';
    private $fileName;
    private $dirLocation;
    private $fileUrl;
    private $absolutePath;
    /**
     * @var File
     */
    protected $file = null;
    public $uuid;

    public function  __construct(){
        $this->uuid = Str::uuid()->toString();
    }


    public function setMetadataFile($name,$dirLocation, $baseType=self::BASE_PUBLIC){
        $this->dirLocation = $dirLocation;
        $this->fileName = $name;

        $base = public_path($dirLocation);
        if($baseType==self::BASE_STORAGE){
            $base = storage_path($dirLocation);
        }
        if(!is_dir($base)){
            mkdir($base,0777, true);
        }
        $this->absolutePath = $base.'/'.$name;
        $this->fileUrl = URL::to('/').'/'.$dirLocation.'/'.$name;
        if($baseType == self::BASE_STORAGE){
            $this->fileUrl = URL::to('/').'storage/'.$dirLocation.'/'.$name;
        }
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     * @return ReportJob
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDirLocation()
    {
        return $this->dirLocation;
    }

    /**
     * @param mixed $dirLocation
     * @return ReportJob
     */
    public function setDirLocation($dirLocation)
    {
        $this->dirLocation = $dirLocation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * @param mixed $fileUrl
     * @return ReportJob
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * @param mixed $absolutePath
     * @return ReportJob
     */
    public function setAbsolutePath($absolutePath)
    {
        $this->absolutePath = $absolutePath;
        return $this;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     * @return ReportJob
     */
    public function setFile(File $file): ReportJob
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return ReportJob
     */
    public function setUuid(UuidInterface $uuid): ReportJob
    {
        $this->uuid = $uuid;
        return $this;
    }


}
