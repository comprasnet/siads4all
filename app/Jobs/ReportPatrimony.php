<?php

namespace App\Jobs;

use App\Repositories\FilesRepository;
use App\Services\NotificationService;
use App\Services\SiadsPatrimFilter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Queue;

class ReportPatrimony extends ReportJob implements ShouldQueue
{

    private $type;
    private $user;

    private $request;


    protected $jobId;
    protected $initialPosition;

    /**
     * Create a new job instance.
     */
    public function __construct(
        $type,
        $user,
        $request
    )
    {
        parent::__construct();
        $this->type = $type;
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $queueName = $this->queue ?? config('queue.default');
        $this->initialPosition = $this->getPositionInQueue($queueName);

        $this->updateQueueProgress($queueName);
        try {
            /** @var NotificationService $notificationService */
            /** @var SiadsPatrimFilter $siadsPatrimFilter */
            $notificationService = app(NotificationService::class);
            $siadsPatrimFilter = app(SiadsPatrimFilter::class);

            $siadsPatrimFilter->setFilterData($this->request);
            $siadsPatrimFilter->setFileNameExport($this->request['filename'] ?? '');

            $file = $this->file;
            $siadsPatrimFilter->setObservablePreExportFile(function ($total) use ($file) {
                $file->total_register = $total;
                $file->save();
            });
            $siadsPatrimFilter->setObservableCurrentExportFile(function($total, $current) use ($file){
                $file->total_register = $total;
                $file->current_register = $current+1;
                $file->save();
            });



            $type = $this->type;
            $siadsPatrimFilter->setFile($this->file);
            $siadsPatrimFilter->$type();
            $urlFile = $this->file->file_location;
            $fileName = explode('/',$this->file->file_location);

            $file->current_register = $file->total_register;
            $file->complete = true;
            $file->save();

            $notificationService->createNotification(
                'Relatório de patrimonio ' . end($fileName) . ' pronto',
                $urlFile,
                [$this->user]
            );
        }catch (\ErrorException|\Exception $e){
            dd($e);
        }
    }

    protected function updateQueueProgress($queueName)
    {
        while ($this->initialPosition > 0) {
            $currentPosition = $this->getPositionInQueue($queueName);
            $queueProgress = (($this->initialPosition - $currentPosition) / $this->initialPosition) * 75;

            Cache::put("file_generation_progress_{$this->jobId}", [
                'position' => $currentPosition,
                'progress' => $queueProgress
            ], 600);

            sleep(1); // Aguardar um segundo antes de verificar novamente
        }
    }

    protected function updateExecutionProgress($currentStep, $totalSteps)
    {
        $executionProgress = 75 + (($currentStep / $totalSteps) * 25);
        Cache::put("file_generation_progress_{$this->jobId}", [
            'position' => 0,
            'progress' => $executionProgress
        ], 600);
    }

    protected function getPositionInQueue($queueName)
    {
        return Queue::size($queueName); // Retorna o número de jobs na fila
    }
}
