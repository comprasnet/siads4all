<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('quick_accesses', function (Blueprint $table) {
            $table->string('permissions')->nullable(); // ou qualquer outro tipo de dado desejado
        });

        $access = \App\Models\QuickAccess::query()->where('slug','=', 'analisar_permissao')
        ->first();
        $access->url = '/unit/approve';
        $access->permissions = 'admin_orgaos,admin_unit,Administrar Usuários';
        $access->save();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('quick_accesses', function (Blueprint $table) {
            $table->dropColumn('permission');
        });
    }

};
