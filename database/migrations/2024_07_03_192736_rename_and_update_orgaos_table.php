<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {

        // Atualizar a estrutura da tabela renomeada
        Schema::table('orgaos', function (Blueprint $table) {

            // Adicionar novas colunas
            $table->string('gestao', 255)->nullable();
            $table->boolean('daas')->default(false);
            $table->string('utiliza_centro_custo', 255)->nullable();
            $table->string('tipo_administracao', 255)->nullable();
            $table->string('poder', 255)->nullable();
            $table->string('esfera', 255)->nullable();
            $table->string('id_sistema_origem', 255)->default('SIAFI');

            // Modificar colunas existentes
            $table->string('codigo_siorg', 255)->nullable()->change();
            $table->string('codigosiasg', 255)->nullable()->change();
            $table->boolean('situacao')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Reverter a estrutura da tabela renomeada
        Schema::table('orgaos', function (Blueprint $table) {
            // Reverter modificações
            $table->renameColumn('orgao_superior', 'orgaosuperior_id');
            $table->dropColumn(['gestao', 'daas', 'utiliza_centro_custo', 'tipo_administracao', 'poder', 'esfera', 'id_sistema_origem']);
            $table->text('codigo_siorg')->change();
            $table->string('codigosiasg', 255)->change();
            $table->boolean('situacao')->notNullable()->change();
        });
    }
};
