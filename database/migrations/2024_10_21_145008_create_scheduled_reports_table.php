<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scheduled_reports', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();             // Nome do relatório
            $table->string('type');             // Tipo de relatório
            $table->foreignId('user_id')->constrained()->onDelete('cascade');  // ID do usuário
            $table->text('data_request');       // Request data (salvo como JSON)
            $table->date('date');               // Data de agendamento
            $table->enum('status', ['pending', 'completed', 'failed'])->default('pending'); // Status do agendamento
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scheduled_reports');
    }
};
