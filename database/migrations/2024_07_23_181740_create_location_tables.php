<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


return new class extends Migration
{
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->timestamps();
        });

        Schema::create('estados', function (Blueprint $table) {
            $table->id();
            $table->char('sigla', 2)->unique();
            $table->string('nome');
            $table->unsignedBigInteger('regiao_id')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            // Foreign key constraint removed as requested
        });

        Schema::create('municipios', function (Blueprint $table) {
            $table->id();
            $table->integer('codigo_ibge')->unique();
            $table->string('nome');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('capital')->default(false);
            $table->unsignedBigInteger('estado_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('estado_id')->references('id')->on('estados')->onDelete('no action')->onUpdate('no action');
        });

        // Seed the tables
        $this->seedPaises();
        $this->seedEstados();
        $this->seedMunicipios();
    }

    public function down()
    {
        Schema::dropIfExists('municipios');
        Schema::dropIfExists('estados');
        Schema::dropIfExists('paises');
    }

    private function seedPaises()
    {
        $json = File::get(database_path('migrations/json/paises.json'));
        $data = json_decode($json, true);
        DB::table('paises')->insert($data);
    }

    private function seedEstados()
    {
        $json = File::get(database_path('migrations/json/estados.json'));
        $data = json_decode($json, true);
        DB::table('estados')->insert($data);
    }

    private function seedMunicipios()
    {
        $json = File::get(database_path('migrations/json/municipios.json'));
        $data = json_decode($json, true);
        DB::table('municipios')->insert($data);
    }
};
