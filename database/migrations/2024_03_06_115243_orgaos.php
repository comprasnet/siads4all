<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orgaossuperiores', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->string('nome');
            $table->boolean('situacao');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('orgaos', function (Blueprint $table) {
            $table->id();
            $table->integer('orgaosuperior_id');
            $table->string('codigo')->unique();
            $table->string('codigosiasg')->nullable();
            $table->string('nome');
            $table->boolean('situacao');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            $table->string('cnpj')->nullable();
            $table->text('codigo_siorg')->nullable();
            $table->foreign('orgaosuperior_id')->references('id')->on('orgaossuperiores')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orgaos');
        Schema::dropIfExists('orgaossuperiores');
    }
};
