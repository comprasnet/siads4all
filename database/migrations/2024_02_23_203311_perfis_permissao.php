<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $roles = \App\Enums\RoleEnums::cases();
        foreach ($roles as $role){
            $roleDb = new \Spatie\Permission\Models\Role();
            $roleDb->name = $role->value;
            $roleDb->guard_name = 'web';
            $roleDb->save();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $roles = \App\Enums\RoleEnums::cases();
        foreach ($roles as $role){
            $roleDb = \Spatie\Permission\Models\Role::query()
                ->where('name','=',$role->value);
            if($roleDb){
                $roleDb->delete();
            }
        }
    }
};
