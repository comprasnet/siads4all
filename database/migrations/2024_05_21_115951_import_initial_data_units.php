<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $file = storage_path('imports/unidades.json');
        if(file_exists($file)){
            $json = json_decode(file_get_contents($file), true);
            foreach($json as $record){
                \App\Models\Unidade::create($record);
            }
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
