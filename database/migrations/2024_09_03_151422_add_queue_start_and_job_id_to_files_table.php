<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('files', function (Blueprint $table) {
            $table->integer('queue_start')->nullable()->after('some_existing_column'); // Altere 'some_existing_column' para a coluna após a qual você deseja adicionar 'queue_start'
            $table->string('job_uuid')->nullable()->after('queue_start');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('queue_start');
            $table->dropColumn('job_id');
        });
    }
};
