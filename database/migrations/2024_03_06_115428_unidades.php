<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->id();
            $table->integer('orgao_id');
            $table->string('codigo')->unique();
            $table->string('gestao')->default('00001');
            $table->string('codigosiasg')->nullable();
            $table->string('nome');
            $table->string('nomeresumido');
            $table->string('telefone')->nullable();
            $table->string('tipo');
            $table->boolean('situacao');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            $table->boolean('sisg')->default(false);
            $table->integer('municipio_id')->nullable();
            $table->string('esfera')->nullable();
            $table->string('poder')->nullable();
            $table->string('tipo_adm')->nullable();
            $table->boolean('aderiu_siasg')->default(true);
            $table->boolean('utiliza_siafi')->default(true);
            $table->string('codigo_siorg')->nullable();
            $table->boolean('sigilo')->default(false);
            $table->string('cnpj')->nullable();
            $table->boolean('utiliza_custos')->default(false);
            $table->string('codigosiafi')->nullable();
            $table->boolean('exclusivo_gestao_atas')->nullable();
            $table->foreign('orgao_id')->references('id')->on('orgaos')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unidades');
    }
};
