<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contas', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->string('nome');
            $table->timestamps();
        });

        // Carregar dados do arquivo JSON
        $json = file_get_contents(database_path('migrations/json/contas.json'));
        $data = json_decode($json, true);

        // Inserir dados na tabela
        foreach ($data as $item) {
            DB::table('contas')->insert([
                'codigo' => $item['codigo'],
                'nome' => $item['nome'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contas');
    }
};
