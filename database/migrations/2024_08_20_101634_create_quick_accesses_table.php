<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quick_accesses', function (Blueprint $table) {
            $table->id();
            $table->string('title'); // Título do acesso rápido
            $table->string('subtitle')->nullable(); // Subtítulo ou descrição
            $table->string('icon'); // Ícone (classe FontAwesome, por exemplo)
            $table->string('url'); // URL do acesso rápido
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quick_accesses');
    }
};
