<?php

use App\Enums\EnumPermissions;
use App\Models\UserRoleUnit;
use App\Repositories\PermissionsRepository;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        app(PermissionsRepository::class)->createPermissionAndAddAllAdmins(EnumPermissions::ORGAOS->value);
        app(PermissionsRepository::class)->createPermissionAndAddAllAdmins(EnumPermissions::UNIDADE->value);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        app(PermissionsRepository::class)->removePermission(EnumPermissions::ORGAOS->value);
        app(PermissionsRepository::class)->removePermission(EnumPermissions::UNIDADE->value);
    }
};
