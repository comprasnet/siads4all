<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('quick_accesses', function (Blueprint $table) {
            $table->integer('order')->default(0)->after('badge_class'); // Adiciona o campo 'order' com valor padrão 0
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('quick_accesses', function (Blueprint $table) {
            $table->dropColumn('order'); // Remove o campo 'order'
        });
    }
};
