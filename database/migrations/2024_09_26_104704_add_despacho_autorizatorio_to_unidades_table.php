<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('unidades', function (Blueprint $table) {
            // Adiciona a coluna "despacho_autorizatorio" do tipo boolean
            $table->boolean('despacho_autorizatorio')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidades', function (Blueprint $table) {
            // Remove a coluna "despacho_autorizatorio"
            $table->dropColumn('despacho_autorizatorio');
        });
    }
};
