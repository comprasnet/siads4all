<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_to', function (Blueprint $table) {
            $table->boolean('push_sent')->default(false)->after('read'); // Adiciona a coluna 'push_sent' do tipo booleano após a coluna 'read'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification_to', function (Blueprint $table) {
            $table->dropColumn('push_sent'); // Remove a coluna 'push_sent' se a migration for revertida
        });
    }
};
