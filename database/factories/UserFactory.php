<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'cpf' => $this->gerarCPF(),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => static::$password ??= Hash::make('password'),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }

    private function gerarCPF(){
        $num = array();
        $num[9]=$num[10]=$num[11]=0;
        $cpf = "";
        for ($w=0; $w > -2; $w--){
            for($i=$w; $i < 9; $i++){
                $x=($i-10)*-1;
                $w==0?$num[$i]=rand(0,9):'';
                $cpf .= ($w==0?$num[$i]:'');
                ($w==-1 && $i==$w && $num[11]==0)?
                    $num[11]+=$num[10]*2    :
                    $num[10-$w]+=$num[$i-$w]*$x;
            }
            $num[10-$w]=(($num[10-$w]%11)<2?0:(11-($num[10-$w]%11)));
            $cpf .= $num[10-$w];
        }
        return $num[0].$num[1].$num[2].'.'.$num[3].$num[4].$num[5].'.'.$num[6].$num[7].$num[8].'-'.$num[10].$num[11];
    }
}
