<?php

namespace Database\Seeders;

use App\Models\QuickAccess;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuickAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        QuickAccess::insert([
            [
                'title' => 'Patrimônios',
                'subtitle' => 'Consulta a patrimônio da unidade',
                'icon' => 'fas fa-list',
                'url' => '/patrimony',
                'slug' => 'patrimonis',
            ],
            [
                'title' => 'Arquivos',
                'subtitle' => 'Consulta a arquivo gerado',
                'icon' => 'fas fa-file',
                'url' => '/files',
                'slug' => 'arquivos',
            ],
            [
                'title' => 'Analisar permissão',
                'subtitle' => 'Analisar solicitação de permissão da unidade',
                'icon' => 'fas fa-user',
                'url' => '/unit/approve',
                'slug' => 'analisar_permissao',
            ],
        ]);
    }
}
