<?php

namespace Tests\Unit;

use App\Models\User;
use App\Repositories\JobRepository;
use App\Services\FilesService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;

class FileServiceTest extends TestCase
{
    //use WithoutMiddleware, DatabaseTransactions;
    public function testCreateJob()
    {
        $request = $this->arrRequest();
        $user = User::query()->where('super_admin',1)->first();
        $this->actingAs($user);

        $fileService = app(FilesService::class)->createReportPatrimonyFileServiceByDataRequest(
            'excel',
            $user->id,
            $request
        );

    }

    public function testPosition(){
        $jobId = 60;
        /** @var JobRepository $job **/
        $job = app(JobRepository::class);
        dd($job->getJobIdPosition($jobId,'default'));
    }

    private function arrRequest(){
        $json = '{ "draw": "1", "columns": [ { "data": "uasg", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "uorg", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "gestao", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "conta", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "numero_patrimonial", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "descricao_material", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "valor", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "data_tombamento", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "situacao", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "destinacao", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "endereco", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "responsavel", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "coresponsavel", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "forma_aquisicao", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } }, { "data": "contrato_garantia_compra", "name": null, "searchable": "true", "orderable": "true", "search": { "value": null, "regex": "false" } } ], "order": [ { "column": "0", "dir": "asc", "name": null } ], "start": "0", "length": "10", "search": { "value": null, "regex": "false" }, "uasg": "64594", "situacao": null, "uorg": null, "destinacao": null, "tombamento_inicio": null, "tombamento_fim": null, "descricao": null, "numero_patrimonial": null, "conta_contabil": null, "responsavel": null, "coresponsavel": null, "visible_cols": [ "uasg", "uorg", "gestao", "conta", "numero_patrimonial", "descricao_material", "valor", "data_tombamento", "situacao", "destinacao", "endereco", "responsavel", "coresponsavel", "forma_aquisicao", "contrato_garantia_compra" ], "filename": null }';
        return json_decode($json,true);
    }
}
