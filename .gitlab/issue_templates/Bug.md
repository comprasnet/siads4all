## Relato de Bug
<!-- Bug é uma falha ou erro em alguma parte que antes era funcional, onde é algo que está impedindo um fluxo que anteriormente estava funcionando.-->

### Origem
<!-- Descreva como o problema foi descoberto ou reportado, incluindo a fonte ou circunstâncias em que o bug foi identificado-->

### Passos para reproduzir o problema
<!-- 1. Liste os passos específicos e detalhados para reproduzir o bug -->
<!-- 2.  -->
<!-- 3.  -->

### Qual o resultado obtido
<!-- Descreva o comportamento incorreto observado ao seguir os passos mencionados acima -->

### Qual o resultado esperado
<!-- Especifique qual deveria ser o comportamento correto ou esperado do sistema -->

### Contexto
<!-- Ofereça uma visão de como o problema foi reproduzido com informações úteis, como: Ambiente, Navegador, horário, Usuário, etc... -->

### Solução sugerida
<!-- qual solução sugerida para o desenvolvedor -->

### Solução alternativa
<!-- Caso a solução sugerida não seja imediatamente possível, forneça uma alternativa temporária ou um contorno para contornar o problema -->

### Anexos
<!-- Se aplicável, anexe capturas de tela, logs ou qualquer outra evidência que possa ajudar a entender ou resolver o bug -->

<!--NÃO REMOVER AS LINHAS A SEGUIR-->
/label ~"tipo::Bug"
