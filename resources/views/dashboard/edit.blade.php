@extends(backpack_view('blank'))
@section('content')
    <h2>Acesso Rápido</h2>

    <!-- Botão para adicionar novo card -->
    <button id="add-new-card" class="btn btn-primary mb-4">Adicionar Novo Acesso</button>

    <!-- Lista de cartões existentes -->
    <div id="quick-access-cards" class="row">
        @foreach($quickAccesses as $quickAccess)
            <div class="col-md-4 mb-4 card-item" data-id="{{ $quickAccess->id }}">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" class="form-control mb-2 title" value="{{ $quickAccess->title }}" placeholder="Título">
                        </div>
                        <div class="form-group">
                            <label for="subtitle">Subtítulo</label>
                            <input type="text" class="form-control mb-2 subtitle" value="{{ $quickAccess->subtitle }}" placeholder="Subtítulo">
                        </div>
                        <div class="form-group">
                            <label for="icon">Ícone</label>
                            <select class="form-control mb-2 icon-select">
                                @php
                                    $icons = \App\Helper\FontAwesome::getIcons();
                                @endphp
                                @foreach($icons as $iconClass => $iconLabel)
                                    <option value="{{ $iconClass }}" @if($quickAccess->icon == $iconClass) selected @endif>
                                        <i class="{{ $iconClass }}"></i> {{ $iconLabel }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" class="url" value="{{ $quickAccess->url }}">
                        <button class="btn btn-primary save-btn">Salvar</button>
                        <button class="btn btn-default remove-btn">Remover</button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
@section('after_scripts')

    @php
        $icons = \App\Helper\FontAwesome::getJsonIcons();
    @endphp
    <script>
        let iconsJson = {!! $icons !!} ;
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" defer></script>
    <script src="/assets/js/dashboard.js"></script>
@endsection
