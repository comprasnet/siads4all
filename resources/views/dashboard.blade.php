@extends(backpack_view('blank'))
@section('content')
    @if(is_null($currentUnit))
        <div class="jumbotron mb-2 ">

            <h1 class="display-3 ">Bem vindo!</h1>

            <p class="">
                Você não possui acesso a nenhuma unidade.
                Para solicitar acesso a alguma unidade clique no botão abaixo e digite o código da unidade
                para que o administrador possa liberar seu acesso
            </p>

            <p class="lead">
                <button class="btn btn-primary solicitar-acesso-unidade" role="button">Solicitar acesso</button>
            </p>
        </div>
    @else
        <div>&nbsp;</div>
        <div class="flex text-center w-100">
            <span style="font-size: 22px;">Acesso Rápido</span>
            <p style="font-size: 14px;">Selecione uma das opções abaixo</p>
        </div>
        <p>&nbsp;</p>
        <div class="row w-100 text-center lista-acesso-rapido">
            @foreach ($quickAccesses as $access)
                @if($quickAccessService->checkPermissions($access))
                    <div class="col w-100 acesso-rapido-item {{ $access->slug }}">
                        <a href="{{ $access->url }}" class="acesso-rapido-item-link-icon">
                            <i class="{{ $access->icon }} fa-5x mb-2" style="color: #295aa1;" aria-hidden="true"></i>
                        </a>
                        <br>
                        <a href="{{ $access->url }}" style="text-decoration: none;">
                            <span class="text-acesso-rapido-item">{{ $access->title }}</span>
                            @if ($access->subtitle)
                                <p class="font-xs text-acesso-rapido-subitem">{{ $access->subtitle }}</p>
                            @endif
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
@endsection
@section('after_scripts')
    <script src="/assets/js/dashboard.js"></script>
    <script>
        @if($countUnitAccessRequest>0)
        $(function () {
            addBadge('.analisar_permissao .acesso-rapido-item-link-icon', {{$countUnitAccessRequest}}, badgeTypes.danger);
        })
        @endif
    </script>
@endsection
