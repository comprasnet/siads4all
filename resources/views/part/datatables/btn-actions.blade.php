@if(is_null($button->getLink()))
    <button onclick="{{$button->getJs()}}"  data-url="{{$button->getUrl()}}" @if(!$button->getJs()) onclick="{{$button->getJs()}}" @endif class="btn-datatable-actions @if(!$button->getIcon()) btn btn-{{$button->getType()}} @else btn btn-none @endif">
        @if($button->getIcon())
            <i class="{{$button->getIcon()}}"></i>
        @endif
    </button>
@else
    <a data-url="{{$button->getUrl()}}" @if(!$button->getJs()) onclick="{{$button->getJs()}}" @endif href="{{$button->getLink()}}" class="btn-datatable-actions @if(!$button->getIcon()) btn btn-{{$button->getType()}} @endif">
        @if($button->getIcon())
            <i class="{{$button->getIcon()}}"></i>
        @endif
    </a>
@endif
