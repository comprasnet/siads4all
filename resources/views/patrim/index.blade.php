@extends(backpack_view('blank'))
@section('content')
    @php
        $ug = null;
        $nomeUg = null;
        $idUg = null;
        $unidadeChangerService = app(\App\Services\UnidadeChangerService::class)->getCurrentUnidade();
        if ($unidadeChangerService) {
            $idUg = $unidadeChangerService->id;
            $ug = $unidadeChangerService->codigo;
            $nomeUg = $unidadeChangerService->nome;
        }
    @endphp
    @include('gov-theme.widgets.breadcrumbs')
    <div class="br-card h-fixed">
        <div class="card-header">
            Filtrar
        </div>
        <div class="card-body">
            <form action="" class="filter">
                <div class="row">
                    <div class="col-md-6">
                        <label for="codigo">UG:</label>
                        <select name="uasg" class="selector-unidades form-control">
                            <option value="{{ $idUg }}">{{ $ug }} - {{ $nomeUg }}</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="nome">Situação:</label>

                        <select name="situacao" class="selector form-control">
                            <option value=""></option>
                            @foreach ($situacaoList as $situacao)
                                <option value="{{ $situacao->it_no_situacao }}">{{ $situacao->it_no_situacao }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-6">
                        <label for="cpf">UORG:</label>
                        <select name="uorg" class="selector-uorg form-control">
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="cpf">Destinação:</label>
                        <select name="destinacao" class="selector form-control">
                            <option value=""></option>
                            @foreach ($destinacaoList as $destinacao)
                                <option value="{{ $destinacao->it_co_destinacao }}">{{ $destinacao->it_no_destinacao }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-6">
                        <label for="email">Data de tombamento:</label>
                        <div class="filter-data-between">
                            <input name="tombamento_inicio" type="date" class="form-control" id="email"
                                value="{{ request('email') }}">
                            <span> até </span>
                            <input name="tombamento_fim" type="date" class="form-control" id="email"
                                value="{{ request('email') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="cpf">Descrição do material:</label>
                        <input name="descricao" type="text" class="form-control descricao-material" id="cpf"
                            value="{{ request('cpf') }}">
                    </div>
                    <div class="col-md-12">&nbsp;</div>

                    <div class="col-md-6">
                        <label for="cpf">Número Patrimonial:</label>
                        <input name="numero_patrimonial" type="text" class="form-control" id="cpf"
                            value="{{ request('cpf') }}">
                    </div>

                    <div class="col-md-6">
                        <label for="cpf">Conta Contábil:</label>
                        <select name="conta_contabil" class="conta-contabil form-control">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-md-12">&nbsp;</div>
                    <div class="col-md-6">
                        <label for="cpf">Responsável:</label>
                        <input name="responsavel" type="text" class="form-control" id="cpf">
                    </div>

                    <div class="col-md-6">
                        <label for="cpf">Corresponsável:</label>
                        <input name="coresponsavel" type="text" class="form-control" id="cpf">
                    </div>
                    <div class="col-md-12" style="padding-bottom: 0px; padding-top: 20px">
                    </div>
                    <div class="col-md-12" style="display: flex;justify-content: end">
                        <a href="{{ route('patrimony.index') }}" style="margin-right: 20px;" class="btn btn-default" id="toggleCampos">
                            Limpar Filtros
                        </a>
                        <button type="submit" class="btn btn-primary"  id="toggleCampos">
                            Filtrar <i class="fa fa-filter"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="br-card h-fixed conta-contabil-soma">
        <div class="card-header">
            Conta Contábil <span class="badge badge-info" data-toggle="tooltip" data-placement="right" data-title="Valor apresentado de acordo com os filtros escolhidos">?</span>
        </div>
        <div class="card-body ">
            Valor total na conta <b class="numero-conta"></b> é <b>R$ <span class="valor-conta"></span></b>
        </div>
    </div>
    <div class="patrimony-report" style="overflow-x: auto">

        <table id="patrimonialReport" class="display table">
        </table>
    </div>
@endsection

@section('after_scripts')
    <style>
        .dt-scroll-sizing {
            height: 0;
            visibility: hidden;
        }
    </style>
    <script type="text/javascript">
        let tableReportPatrim;
        $(document).ready(function(result) {

            $('.descricao-material').on('input', function() {
                // Converter o valor para maiúsculas
                $(this).val($(this).val().toUpperCase());
            });

            $('.selector,.selector-uorg').select2({
                placeholder: 'Selecione uma opção',
            }).each(function(){
                $(this).next().find('.select2-selection__clear').trigger('click')
            });
            $('.selector-unidades').select2Unidade({
                allowClear:false
            });



            $('.selector-unidades').change(function(){

                $('.selector-uorg').html('<option value="">Carregando</option>').trigger('change ...');
                let ug = $(this).val();
                $.ajax({
                    url: '/unit/uorgs',
                    dataType: 'json',
                    data:{'ug':$('.selector-unidades').val()},
                    success: function(json){
                        let option;
                        $('.selector-uorg').html('<option value=""></option>');
                        for(let i=0;i<json.length;i++){
                            option = $('<option/>')
                            option.val(json[i].id);
                            option.html(json[i].text);
                            $('.selector-uorg').append(option);
                        }
                        $('.selector-uorg').trigger('change');
                    }
                });
            }).trigger('change');

            $('.conta-contabil').select2({
                language: 'pt-BR',
                ajax: {
                    url: '/patrimony/conta-contabil',
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            search: params.term,
                            ug: $('.selector-unidades').val()
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 0,
                placeholder: 'Procurar Conta Contábil',
            });

            function validarDadosExportacaoAgendada(data) {
                const dataAtual = new Date(); // Data e hora atual
                const dataAgendamento = new Date(data.data_agendamento); // Converte a data de agendamento

                if(data.agendamento_futuro === '1') {
                    // Verificar se o campo data_agendamento está vazio
                    if (!data.data_agendamento || data.data_agendamento.trim() === '') {
                        $.alert('Data de agendamento é obrigatória');
                        return false; // Interrompe a execução se o campo for inválido
                    }

                    // Verificar se a data de agendamento é maior que a data atual
                    if (dataAgendamento <= dataAtual) {
                        $.alert('Data de agendamento deve ser maior que a data atual');
                        return false; // Interrompe a execução se a data for inválida
                    }
                }

                // Se passar todas as verificações, os dados são válidos
                return true;
            }
            function ajaxByTypeAgendar(type) {

                let modal = $.dinamicModal('Exportação do relatório',
                    `
                        <label>Nome do relatório</label><br>
                        <input type="text" id="modalInput" class="br-input form-control"><br>
                        <small style="margin-top: -22px;display: block; margin-bottom: 20px">Não são permitidos os seguintes caracteres: \\ / \\0 *</small>

                        <label>Agendamento futuro</label><br>
                        <div id="agendamento-futuro">
                            <label><input type="radio" value="1" name="agendamento"> Sim</label>
                            <span style="display: inline-block; width: 20px"></span>
                            <label><input checked type="radio" value="0" name="agendamento"> Não</label>
                        </div>
                        <br>
                        <div class="data-agendamento">
                            <label>Data do agendamento</label>
                            <input class="br-input form-control" type="date" id="data-agendamento">
                        </div>`,
                    [{
                        name: 'Cancelar',
                        type: 'secondary',
                        close: true
                    },
                        {
                            name: 'Gerar',
                            type: 'primary',
                            action: () => {
                                const result = document.getElementById('modalInput').value;
                                const dataAgendamento = document.getElementById('data-agendamento').value;
                                $.loader.start()
                                let dataSend = $.table.paramsAndVisibleColumns(tableReportPatrim);
                                dataSend.filename = result;
                                dataSend.data_agendamento = dataAgendamento;
                                dataSend.agendamento_futuro = $('#agendamento-futuro input[type="radio"]:checked').val();
                                let url = '/patrimony/export/' + type;
                                if(dataSend.agendamento_futuro === '1'){
                                    url = '/patrimony/schedule/export/' + type
                                }

                                //'/patrimony/export/' + type
                                if(validarDadosExportacaoAgendada(dataSend)) {
                                    $.ajax({
                                        url: url,
                                        data: dataSend,
                                        type: 'POST',
                                        complete: function () {
                                            $.loader.stop()
                                        },
                                        success: function (data) {
                                            if(dataSend.agendamento_futuro === '0'){
                                                $.alert(
                                                    'Estamos gerando o seu relatório e, em breve, você será notificado.'
                                                )
                                                return;
                                            }
                                            if(data.success) {
                                                $.alert(
                                                    'Seu Relatório será gerado em ' + data.message.date
                                                )
                                            }else{
                                                $.alert(
                                                    data.message
                                                )
                                            }
                                        }
                                    })
                                    $(modal).remove();
                                }
                                return false;
                            },
                            close: false
                        }
                    ]);
                $(modal).find('.br-modal').width(400);
                $(modal).find('#modalInput').focus();
                $(modal).find('#modalInput').on('keyup keydown paste', function() {
                    let val = $(this).val();
                    val = val.replace('/', '');
                    val = val.replace('\\', '');
                    val = val.replace('\\0', '');
                    val = val.replace('*', '');
                    val = val.replace(' ', '_');
                    $(this).val(val);
                })
                let agendamentoRadio = $(modal).find('input[name="agendamento"]');
                $(modal).find('input[name="agendamento"]').change(function(){
                    let val = $(modal).find('input[name="agendamento"]:checked').val();
                    let dataAgendamento = $(modal).find('.data-agendamento');
                    if(val === '1'){
                        dataAgendamento.show()
                    }else{
                        dataAgendamento.hide()
                    }
                }).trigger('change');
                console.log(modal);

            }

            if (!$.fn.DataTable.isDataTable('#patrimonialReport')) {
                tableReportPatrim = $('#patrimonialReport').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "/patrimony/list",
                        "type": "POST",
                        "data": function(d) {
                            // Adicione os seus filtros aqui
                            // Exemplo:
                            let formData = new FormData(document.querySelector('.filter'));
                            for (const [key, value] of formData.entries()) {
                                d[key] = value;
                            }
                        }
                    },
                    "columns": [{
                            "data": "uasg",
                            "title": "UG",
                            width: "100px"
                        },
                        {
                            "data": "uorg",
                            "title": "UORG",
                            width: "100px"
                        },
                        {
                            "data": "gestao",
                            "title": "Gestão",
                            width: "100px"
                        },
                        {
                            "data": "conta",
                            "title": "Conta",
                            width: "100px"
                        },
                        {
                            "data": "numero_patrimonial",
                            "title": "Número Patrimonial",
                            width: "100px"
                        },
                        {
                            "data": "descricao_material",
                            "title": "Descrição do material",
                            width: "100px"
                        },
                        {
                            "data": "valor",
                            "title": "Valor",
                            width: "100px"
                        },
                        {
                            "data": "valor_depreciado",
                            "title": "Valor Depreciado"
                        },
                        {
                            "data": "valor_liquido",
                            "title": "Valor Líquido"
                        },
                        {
                            "data": "data_tombamento",
                            "title": "Data de tombamento",
                            width: "100px"
                        },
                        {
                            "data": "situacao",
                            "title": "Situação",
                            width: "100px"
                        },
                        {
                            "data": "destinacao",
                            "title": "Destinação",
                            width: "100px"
                        },
                        {
                            "data": "endereco",
                            "title": "Endereço",
                            width: "100px"
                        },
                        {
                            "data": "responsavel",
                            "title": "Responsável",
                            width: "100px"
                        },
                        {
                            "data": "coresponsavel",
                            "title": "Corresponsável",
                            width: "100px"
                        },
                        {
                            "data": "forma_aquisicao",
                            "title": "Forma de Aquisição",
                            width: "100px"
                        },
                        {
                            "data": "contrato_garantia_compra",
                            "title": "Garantia de Compra",
                            width: "100px"
                        }
                    ],
                    buttons: [
                        'colvis',
                        {
                            text:"Exportar",
                            action: function(e){
                                $(e.currentTarget).next().trigger('click');
                            },
                            split:[
                                {
                                    'text':"PDF",
                                    action: function(){
                                        ajaxByTypeAgendar('pdfTable')
                                    }
                                },{
                                    'text':"PDF Formulário",
                                    action: function(){
                                        ajaxByTypeAgendar('pdf')
                                    }
                                },{
                                    'text':"Excel",
                                    action: function(){
                                        ajaxByTypeAgendar('excel')
                                    }
                                },{
                                    'text':"CSV",
                                    action: function(){
                                        ajaxByTypeAgendar('csv')
                                    }
                                },{
                                    'text':"TXT",
                                    action: function(){
                                        ajaxByTypeAgendar('txt')
                                    }
                                }
                            ],
                        },
                    ],
                    language: {
                        url: '/assets/js/datatables/pt-br.json'
                    },
                    dom: '<"top"Bi>rt<"bottom"lp><"clear">',
                    autoWidth: true,
                    draw: function() {
                        $.loadingElement.stop(document.querySelector('.patrimony-report'))
                    },
                    scrollX: true,
                    scrollY: '600px',
                    lengthMenu: [ [50, 100, 150], [50, 100, 150] ]
                });
                $('#patrimonialReport')
                    .on('processing.dt', function(e, settings, processing) {
                        setTimeout(
                            function() {
                                $.loadingElement.start(document.querySelector('.patrimony-report'))
                            }, 500
                        )
                    })
                tableReportPatrim.on('draw', function(e, settings, processing) {
                    setTimeout(function() {
                        $.loadingElement.stop(document.querySelector('.patrimony-report'))
                    }, 1000);
                })
                let contaContabilSum = () => {
                    let contaContabil = $('select[name="conta_contabil"]').val();
                    let contaName = $('select[name="conta_contabil"] option:selected').text();
                    let cardContaContabil = $('.conta-contabil-soma');
                    let numeroConta = $('.numero-conta');
                    let valorConta = $('.valor-conta');

                    numeroConta.html('carregando ...');
                    valorConta.html('carregando ...');
                    if (contaContabil == "") {
                        cardContaContabil.hide();
                    } else {
                        cardContaContabil.show();

                        let scrollAtual = $(window).scrollTop();
                        let novoScroll = scrollAtual + 10;
                        $(window).scrollTop(novoScroll);
                        $(window).scrollTop(scrollAtual);

                        numeroConta.html(contaName);
                        $.ajax({
                            url: '/patrimony/conta-contabil/sum',
                            data: $('.filter').serialize(),
                            type:'POST',
                            dataType: 'JSON',
                            success: function(json) {
                                valorConta.html(json.data);
                            },
                            error: function() {
                                valorConta.html('Erro ao carregar o valor da Conta')
                            }
                        })
                    }
                }
                contaContabilSum();
                $('select[name="conta_contabil"]').change(() => {
                    contaContabilSum();
                })
                $('.filter').submit(function() {
                    try {
                        validateDateFilter();
                        contaContabilSum();
                        tableReportPatrim.ajax.reload();
                    } catch (error) {
                        $.alert(error);
                    }
                    return false;
                })

                function validateDateFilter() {
                    const startDateInput = document.querySelector('input[name="tombamento_inicio"]');
                    const endDateInput = document.querySelector('input[name="tombamento_fim"]');
                    const startDateValue = startDateInput.value;
                    const endDateValue = endDateInput.value;
                    const currentDate = new Date();

                    if (startDateValue) {
                        const startDate = new Date(startDateValue);
                        if (startDate > currentDate) throw new Error(
                            'A data de tombamento inicial deve ser menor ou igual à data atual.');
                        if (endDateValue) {
                            const endDate = new Date(endDateValue);
                            if (endDate < startDate) throw new Error(
                                'A Data de tombamento inicial deve ser maior ou igual à data de tombamento inicial.'
                            );
                            if (endDate > currentDate) throw new Error(
                                'A data de tombamento final não deve ser superior à data atual.');
                        }
                    } else if (endDateValue) {
                        throw new Error(
                            'A data de tombamento final só pode ser inserida se a data de tombamento inicial estiver preenchida'
                        );
                    }
                }

            }
        });
    </script>
@endsection
