<!DOCTYPE html>
<html>
<head>
    <title>Formulário de Dados</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .container {
            width: 100%;
            margin: 0 auto;
        }
        .form-section {
            margin-bottom: 20px;
        }
        .form-section h3 {
            margin-bottom: 10px;
            color: #333;
        }
        .form-section p {
            margin: 0;
            padding: 5px 0;
            color: #555;
        }
    </style>
</head>
<body>
<header>
    <img src="{{ public_path('img/report.png') }}" alt="Header Image">
</header>
<div class="container">

    <h3>Formulário de Patrimônios</h3>
    @foreach ($results as $current=>$result)
        {{ $callback($total,$current) }}
        <div class="form-section">
            <table>
                <tr>
                    <td>
                        <p><strong>UG:</strong> {{ $result->uasg }}</p>
                        <p><strong>UORG:</strong> {{ $result->uorg }}</p>
                        <p><strong>Gestão:</strong> {{ $result->gestao }}</p>
                        <p><strong>Conta:</strong> {{ $result->conta }}</p>
                        <p><strong>Número Patrimonial:</strong> {{ $result->numero_patrimonial }}</p>
                        <p><strong>Descrição do material:</strong> {{ $result->descricao_material }}</p>
                        <p><strong>Valor:</strong> {{ $result->valor }}</p>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <p><strong>Data do tombamento:</strong> {{ $result->data_tombamento }}</p>
                        <p><strong>Situação:</strong> {{ $result->situacao }}</p>
                        <p><strong>Destinação:</strong> {{ $result->destinacao }}</p>
                        <p><strong>Endereço:</strong> {{ $result->endereco }}</p>
                        <p><strong>Responsável:</strong> {{ $result->responsavel }}</p>
                        <p><strong>Forma de aquisição:</strong> {{ $result->forma_aquisicao }}</p>
                        <p><strong>Garantia de Compra:</strong> {{ $result->contrato_garantia_compra }}</p>
                    </td>
                </tr>
            </table>


        </div>
        <hr>
    @endforeach
</div>
</body>
</html>
