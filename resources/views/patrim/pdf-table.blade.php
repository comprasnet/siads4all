<!DOCTYPE html>
<html>
<head>
    <title>Tabela de Dados</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 11px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
            text-align: left;
        }
    </style>
</head>
<body>
<header>
    <img src="{{ public_path('img/report.png') }}" alt="Header Image">
</header>
<h2>Patrimônios</h2>
<table>
    <thead>
    <tr>
        @foreach($titles as $k=>$title)
            @if($k<6)
                <th>{{$title}}</th>
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach ($results as $current=>$result)
        {{ $callback($total,$current) }}
        <tr>
            @foreach($names as $k=>$name)
                @if($k<6)
                    <td>{{$result->$name}}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
