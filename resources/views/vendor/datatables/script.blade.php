let dataTableData = %2$s;

dataTableData.ajax.beforeSend = function(){
    const dtElement = LaravelDataTables["%1$s"].table().node();
    const dttable = $(dtElement);
    const containerElement = dttable.parents('.dt-container');

    $.loadingElement.start(containerElement[0])
}
dataTableData.ajax.dataSrc = function(json){
    const dtElement = LaravelDataTables["%1$s"].table().node();
    const dttable = $(dtElement);
    const containerElement = dttable.parents('.dt-container');
    console.log('teste');
    $.loadingElement.stop(containerElement[0])
    return json.data; 
}
dataTableData.dom = '<"top"fi>rt<"bottom"lp><"clear">';

dataTableData.ajax.url = dataTableData.ajax.url.replace('http:',window.location.protocol);

dataTableData.language = {
    url: '/assets/js/datatables/pt-br.json'
};

(function(window,$){window.LaravelDataTables=window.LaravelDataTables||{};window.LaravelDataTables["%1$s"]=$("#%1$s").DataTable(dataTableData);})(window,jQuery);