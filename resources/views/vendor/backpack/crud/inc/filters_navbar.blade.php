

@push('crud_list_scripts')
    @basset('https://unpkg.com/urijs@1.19.11/src/URI.min.js')
    <script>
        function addOrUpdateUriParameter(uri, parameter, value) {
            var new_url = normalizeAmpersand(uri);

            new_url = URI(new_url).normalizeQuery();

            // this param is only needed in datatables persistent url redirector
            // not when applying filters so we remove it.
            if (new_url.hasQuery('persistent-table')) {
                new_url.removeQuery('persistent-table');
            }

            if (new_url.hasQuery(parameter)) {
                new_url.removeQuery(parameter);
            }

            if (value !== '' && value != null) {
                new_url = new_url.addQuery(parameter, value);
            }

            $('#remove_filters_button').toggleClass('invisible', !new_url.query());

            return new_url.toString();

        }

        function updateDatatablesOnFilterChange(filterName, filterValue, update_url = false) {
            // behaviour for ajax table
            var current_url = crud.table.ajax.url();
            var new_url = addOrUpdateUriParameter(current_url, filterName, filterValue);

            new_url = normalizeAmpersand(new_url);

            // add filter to URL
            crud.updateUrl(new_url);
            crud.table.ajax.url(new_url);

            // when we are clearing ALL filters, we would not update the table url here, because this is done PER filter
            // and we have a function that will do this update for us after all filters had been cleared.
            if(update_url) {
                // replace the datatables ajax url with new_url and reload it
                crud.table.ajax.url(new_url).load();
            }

            return new_url;
        }


        function normalizeAmpersand(string) {
            return string.replace(/&amp;/g, "&").replace(/amp%3B/g, "");
        }

        // button to remove all filters
        jQuery(document).ready(function($) {
            $("#remove_filters_button").click(function(e) {
                e.preventDefault();

                // behaviour for ajax table
                var new_url = '{{ url($crud->route.'/search') }}';
                var ajax_table = $("#crudTable").DataTable();

                // replace the datatables ajax url with new_url and reload it
                ajax_table.ajax.url(new_url).load();

                // clear all filters
                $(".navbar-filters li[filter-name]").trigger('filter:clear');

                // remove filters from URL
                crud.updateUrl(new_url);
            });

            // hide the Remove filters button when no filter is active
            $(".navbar-filters li[filter-name]").on('filter:clear', function() {
                var anyActiveFilters = false;
                $(".navbar-filters li[filter-name]").each(function () {
                    if ($(this).hasClass('active')) {
                        anyActiveFilters = true;
                        // console.log('ACTIVE FILTER');
                    }
                });

                if (anyActiveFilters == false) {
                    $('#remove_filters_button').addClass('invisible');
                }
            });
        });
    </script>
@endpush
