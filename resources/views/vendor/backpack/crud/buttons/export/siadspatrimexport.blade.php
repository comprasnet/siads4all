<a href="" id="btnPlanilha" class="btn btn-default ladda-button" data-style="zoom-in">
    <span class="ladda-label"><i class="la la-download"></i> Planilha da UG</span>
</a>

@push('after_scripts')
    <script>
        $(document).ready(function () {
                var table = $('#crudTable').DataTable();

                table.on('draw.dt', function () {
                        var queryString = window.location.search;

                        var urlParams = new URLSearchParams(queryString);

                        var url = '/export/siads/patrim?';

                        if (urlParams.has('orgao')) {
                            url += 'orgao=' + urlParams.get('orgao') + '&';
                        }

                        if (urlParams.has('unidade')) {
                            url += 'unidade=' + urlParams.get('unidade') + '&';
                        }

                        if (urlParams.has('uorg')) {
                            url += 'uorg=' + urlParams.get('uorg') + '&';
                        }

                        if (urlParams.has('numero_patrimonial')) {
                            url += 'numero_patrimonial=' + urlParams.get('numero_patrimonial') + '&';
                        }

                        if (urlParams.has('conta_contabil')) {
                            url += 'conta_contabil=' + urlParams.get('conta_contabil') + '&';
                        }

                        $('#btnPlanilha').attr('href', url);

                    }
                );
            }
        );
    </script>
@endpush
