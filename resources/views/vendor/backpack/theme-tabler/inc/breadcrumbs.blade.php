@if (backpack_theme_config('breadcrumbs') && isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs))

    <div class="br-breadcrumb">
        <ul class="crumb-list">
            @foreach ($breadcrumbs as $label => $link)
                @if ($label == 'Icone')
                    @php
                        $dadosIcone = json_decode($link, true);

                        $label = '<a  href= "' . $dadosIcone['url'] . '"> Voltar</a>';
                        $link = false;
                    @endphp
                @endif
                @if ($link)
                    @if ($label == 'Admin')
                        <li class="crumb home"><a class="br-button circle" href="{{ $link }}"><span
                                    class="sr-only">Página inicial</span><i class="fas fa-home"></i></a></li>
                    @else
                        @if ($label == 'Início')
                        <li class="crumb ">
                            <a class="br-button circle" href="{{ $link }}">
                                <span class="sr-only">Página inicial</span>
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        @else
                            <li class="crumb "><i class="icon fas fa-chevron-right f100"></i><a
                                    href="{{ $link }}">{{ $label }}</a></li>
                        @endif
                    @endif
                @else
                    @if ($label !== 'Inicio')
                        <li class="crumb" data-active="active"><i class="icon fas fa-chevron-right"></i> <span>
                                {!! $label !!} </span> </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>

@endif
