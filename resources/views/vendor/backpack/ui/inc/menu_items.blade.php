{{-- This file is used for menu items by any Backpack v6 theme --}}
<x-backpack::menu-item title="Início" icon="la la-dashboard" :link="backpack_url('dashboard')" />

<x-backpack::menu-dropdown title="Administração" icon="la la-cogs">
    @can('show_adm_autenticacao')
        <x-backpack::menu-dropdown title="Autenticação" icon="la la-user" nested="true">
            @can('show_adm_aut_usuarios')
                <x-backpack::menu-dropdown-item title="Usuários" icon="la la-user" :link="backpack_url('user')" />
            @endcan
            @can('show_adm_aut_grupos')
                <x-backpack::menu-dropdown-item title="Grupos" icon="la la-group" :link="backpack_url('role')" />
            @endcan
            @can('show_adm_aut_permissoes')
                <x-backpack::menu-dropdown-item title="Permissões" icon="la la-key" :link="backpack_url('permission')" />
            @endcan
        </x-backpack::menu-dropdown>
        <x-backpack::menu-dropdown title="Outros" icon="la la-user" nested="true">
            <x-backpack::menu-dropdown-item title="Código Itens" icon="la la-list" :link="backpack_url('code')" />
        </x-backpack::menu-dropdown>
    @endcan
</x-backpack::menu-dropdown>

<x-backpack::menu-item title="Siads patrimonios" icon="la la-question" :link="backpack_url('siads-patrimonio')" />

<x-backpack::menu-item title="Users" icon="la la-question" :link="backpack_url('users')" />