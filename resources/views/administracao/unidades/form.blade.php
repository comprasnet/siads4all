@extends(backpack_view('blank'))

@section('content')
    <div>
        @if (is_null($unidade->id))
            <h2>Cadastrar Unidade</h2>
        @else
            <h2>Editar Unidade</h2>
        @endif
        <form method="POST"
              action="{{ !is_null($unidade->id) ? route('unidades.update', $unidade->id) : route('unidades.store') }}">
            @csrf
            @if (!is_null($unidade->id))
                @method('PUT')
            @endif
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="codigosiasg">UASG SIASG</label>
                    <input class="form-control" name="codigosiasg" id="codigosiasg"  pattern="\d{6}"
                           value="{{ old('codigosiasg', $unidade->codigosiasg ?? '') }}" maxlength="6" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="codigo">UG SIAFI</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" pattern="\d{6}"
                           value="{{ old('codigo', $unidade->codigo ?? '') }}" maxlength="6" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="orgao_id">Órgão</label>
                    <select id="orgao_id" name="orgao_id" class="form-control select2" style="width: 100%;">
                        <option value="">Selecione um item</option>
                        @foreach ($orgaos as $orgao)
                            <option value="{{ $orgao->id }}"
                                {{ (old('orgao_id', $unidade->orgao_id ?? '') == $orgao->id) ? 'selected' : '' }}>
                                {{ $orgao->nome }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" class="form-control cnpj" name="cnpj" id="cnpj" maxlength="14"
                           value="{{ old('cnpj', $unidade->cnpj ?? '') }}" pattern="\d{2}\.\d{3}\.\d{3}/\d{4}-\d{2}">
                </div>
                <div class="form-group col-md-4">
                    <label for="telefone">Telefone</label>
                    <input type="text" class="form-control fone" name="telefone" id="telefone" maxlength="10"
                           value="{{ old('telefone', $unidade->telefone ?? '') }}">
                </div>
                <div class="form-group col-md-4">
                    <label for="nomeresumido">Nome Resumido</label>
                    <input type="text" class="form-control" id="nomeresumido" name="nomeresumido"
                           value="{{ old('nomeresumido', $unidade->nomeresumido ?? '') }}"
                           required>
                </div>
                <div class="form-group col-md-4">
                    <label for="tipo">Tipo</label>
                    <select id="tipo" name="tipo" class="form-control select2" style="width: 100%;">
                        <option value="">Selecione um item</option>
                        @foreach ($tipos as $value => $label)
                            <option value="{{ $value}}"
                                {{ (old('tipo', $unidade->tipo ?? '') == $value) ? 'selected' : '' }}>
                                {{ $label }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="situacao">Situaçao</label>
                    <select name="situacao" id="situacao" class="form-control select2" required>
                        <option value="">Selecione um item</option>
                        <option value="1" {{ (old('situacao', $unidade->situacao ?? '') == 1) ? 'selected' : '' }}>
                            Ativo
                        </option>
                        <option value="0" {{ (old('situacao', $unidade->situacao ?? '') == 0) ? 'selected' : '' }}>
                            Inativo
                        </option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="sisg">Sisg</label>
                    <select id="sisg" name="sisg" class="form-control select2" required>
                        <option value="">Selecione um item</option>
                        <option value="1" {{ (old('situacao', $unidade->situacao ?? '') == 1) ? 'selected' : '' }}>Sim
                        </option>
                        <option value="0" {{ (old('situacao', $unidade->situacao ?? '') == 0) ? 'selected' : '' }}>Não
                        </option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="uf">Estado</label>
                    <select id="uf" name="uf" class="form-control select2" required>
                        <option value="">Selecione um item</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->id }}">{{ $estado->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="municipio">Município</label>
                    <select id="municipio" name="municipio_id" class="form-control select2" required>
                        <!-- Opções serão preenchidas via AJAX -->
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="esfera">Esfera</label>
                    <select id="esfera" name="esfera" class="form-control select2" style="width: 100%;">
                        <option value="">Selecione um item</option>
                        @foreach ($esferas as $value => $label)
                            <option @if(old('esfera', $unidade->esfera ?? '')==$value) selected
                                    @endif value="{{ $value }}">{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="poder">Poder</label>
                    <select id="poder" name="poder" class="form-control select2" style="width: 100%;">
                        <option value="">Selecione um item</option>
                        @foreach ($poderes as $value => $label)
                            <option @if(old('poder', $unidade->poder ?? '')==$value) selected
                                    @endif value="{{ $value }}">{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="tipo_administracao">Administração</label>
                    <select id="tipo_administracao" name="tipo_adm" class="form-control select2"
                            style="width: 100%;">
                        <option value="">Selecione um item</option>
                        @foreach ($administracoes as $value => $label)
                            <option @if(old('tipo_administracao', $unidade->tipo_adm ?? '')==$value) selected
                                    @endif value="{{ $value }}">{{ $label }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="aderiu_siasg">Aderiu SIASG</label>
                    <select class="form-control select2" id="aderiu_siasg" name="aderiu_siasg" required>
                        <option value="">Selecione um item</option>
                        <option value="1" {{ old('aderiu_siasg', $unidade->aderiu_siasg ?? '') == '1' ? 'selected' : '' }}>Sim</option>
                        <option value="0" {{ old('aderiu_siasg', $unidade->aderiu_siasg ?? '') == '0' ? 'selected' : '' }}>Não</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="codigosiafi">SIAFI</label>
                    <select class="form-control select2" id="codigosiafi" name="codigosiafi" required>
                        <option value="">Selecione um item</option>
                        <option value="1" {{ old('aderiu_siasg', $unidade->aderiu_siasg ?? '') == '1' ? 'selected' : '' }}>Sim</option>
                        <option value="0" {{ old('aderiu_siasg', $unidade->aderiu_siasg ?? '') == '0' ? 'selected' : '' }}>Não</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="utiliza_custos">Utiliza Custos SIAFI</label>
                    <select id="utiliza_custos" name="utiliza_custos" class="form-control select2" required>
                        <option value="">Selecione um item</option>
                        <option
                            value="1" {{ (old('utiliza_siafi', $unidade->utiliza_siafi ?? '') == 1) ? 'selected' : '' }}>
                            Sim
                        </option>
                        <option
                            value="0" {{ (old('utiliza_siafi', $unidade->utiliza_siafi ?? '') == 0) ? 'selected' : '' }}>
                            Não
                        </option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="gestao">Gestão</label>
                    <input class="form-control" name="gestao" id="gestao"
                           value="{{ old('gestao', $unidade->gestao ?? '') }}"  maxlength="5"   pattern="\d{5}" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" name="nome" id="nome"
                           value="{{ old('nome', $unidade->nome ?? '') }}" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <div class="br-switch" role="presentation">
                        <input
                            value="1"
                            {{ (old('despacho_autorizatorio', $unidade->despacho_autorizatorio ?? '') == 1) ? 'checked' : '' }} id="despacho_autorizatorio"
                            type="checkbox" name="despacho_autorizatorio"
                            role="switch"/>
                        <label for="despacho_autorizatorio">Despacho Autorizatório</label>
                        <div class="switch-data" data-enabled="Sim" data-disabled="Não"></div>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-primary">
                        @if (is_null($unidade->id))
                            Cadastrar
                        @else
                            Editar
                        @endif
                    </button>
                    <a href="{{ route('unidades.index') }}" class="btn btn-default">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('after_scripts')
    <script>
        $(document).ready(function () {
            $('.select2').select2({
                placeholder:'Selecione um item'
            });
            $(".cnpj").mask('99.999.999/9999-99');

            $('.fone').mask('(00) 0000-00009');
            $('.fone').on('keyup',function(event) {
                if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                    $(this).mask('(00) 00000-0009');
                } else {
                    $(this).mask('(00) 0000-00009');
                }
            });
            $('#uf').change(function () {
                var uf = $(this).val();
                if (uf) {
                    $.ajax({
                        url: '/municipios/get-municipios/' + uf,
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            $('#municipio').empty();
                            $.each(data, function (key, value) {
                                $('#municipio').append('<option value="' + key + '">' +
                                    value + '</option>');
                            });
                            $('#municipio').select2();

                            var municipioSelecionado = "{{ $unidade->municipio_id }}"; // Supondo que você tenha o municipio_id no objeto $unidade
                            $('#municipio').val(municipioSelecionado).trigger('change'); // Seleciona o município correto
                        }
                    });
                } else {
                    $('#municipio').empty();
                }
            });

            var estadoSelecionado = "{{ $estadoId }}"; // Supondo que você tenha o estado_id no objeto $unidade
            $('#uf').val(estadoSelecionado).change();
        });
    </script>
@endsection
