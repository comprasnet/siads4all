@php use App\Services\UnidadesService; @endphp
@php /** @var \App\Models\Unidade $unidade**/ @endphp
<div class="unidade-details">
    <p><strong>UASG SIASG:</strong> {{ $unidade->codigosiasg }}</p>
    <p><strong>UG SIAFI:</strong> {{ $unidade->codigo }}</p>
    <p><strong>Nome Resumido:</strong> {{ $unidade->nomeresumido }}</p>
    <p><strong>Órgão:</strong> {{ $unidade->orgao->codigo }} - {{ $unidade->orgao->nome }}</p>
    <p><strong>CNPJ:</strong> {{ $unidade->cnpj }}</p>
    <p><strong>Telefone:</strong> {{ $unidade->telefone }}</p>
    <p><strong>Tipo:</strong> {{ UnidadesService::TIPO[$unidade->tipo]??'' }}</p>
    <p><strong>Sisg:</strong> {{ $unidade->sisg?'Sim':'Não' }}</p>
    <p><strong>UF:</strong> {{ $uf }}</p>
    <p><strong>Município:</strong> {{ $unidade->municipio }}</p>
    <p><strong>Esfera:</strong> {{ UnidadesService::ESFERAS[$unidade->esfera]??'' }}</p>
    <p><strong>Poder:</strong> {{ UnidadesService::PODER[$unidade->poder]??'' }}</p>
    <p><strong>Administração:</strong> {{ UnidadesService::ADMINISTRACAO[$unidade->tipo_adm]??'' }}</p>
    <p><strong>Utiliza Custos SIAFI:</strong> {{ $unidade->utiliza_custos_siafi ? 'Sim' : 'Não' }}</p>
    <p><strong>SIASG:</strong> {{ $unidade->aderiu_siasg ? 'Sim' : 'Não' }}</p>
    <p><strong>Gestão:</strong> {{ $unidade->gestao }}</p>
    <p><strong>Nome:</strong> {{ $unidade->nome }}</p>
    <p><strong>Despacho Autorizatório:</strong> {{ $unidade->despacho_autorizatorio?'Sim':'Não' }}</p>
</div>
