@extends(backpack_view('blank'))
@section('content')
    @include('gov-theme.widgets.breadcrumbs')
    <div class="list-btns">
        <a href="{{ route('unidades.create') }}" type="submit" class="btn btn-primary" control-id="ControlID-5">
            Adicionar Unidade
        </a>
    </div>
    <div class="scroll-table">
        {!! $dataTable->table() !!}
    </div>
@endsection

@section('after_scripts')
    {!! $dataTable->scripts() !!}
@endsection
