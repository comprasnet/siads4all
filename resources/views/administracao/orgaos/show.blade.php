<div class="orgao-details">
    <p><strong>Código SIASG:</strong> {{ $orgao->codigosiasg }}</p>
    <p><strong>Código SIAFI:</strong> {{ $orgao->codigo }}</p>
    <p><strong>Nome:</strong> {{ $orgao->nome }}</p>
    @if($orgaoSuperior->nome)
        <p><strong>Órgão Superior:</strong> {{ $orgaoSuperior->nome }}</p>
    @endif
    <p><strong>Situação:</strong> {{ $orgao->situacao?'Ativa':'Inativa' }}</p>
    <p><strong>CNPJ:</strong> {{ $orgao->cnpj }}</p>
    <p><strong>Gestão:</strong> {{ $orgao->gestao }}</p>
    <p><strong>Utiliza Centro de Custo:</strong> {{ $orgao->utiliza_centro_custo ? 'Sim' : 'Não' }}</p>
</div>
