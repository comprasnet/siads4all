@extends(backpack_view('blank'))
@section('content')
    @include('gov-theme.widgets.breadcrumbs')

    <div class="list-btns">
        <a href="/administracao/orgaos/create" type="submit" class="btn btn-primary" control-id="ControlID-5">
            Adicionar Orgão
        </a>
    </div>
    {{ $dataTable->table() }}
@endsection

@section('after_scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endsection
