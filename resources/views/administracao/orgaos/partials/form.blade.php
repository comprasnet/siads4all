<form action="{{ $action }}" method="POST">
    @csrf
    @if($method == 'PUT')
        @method('PUT')
    @endif

    <div class="row">
        <!-- Código SIASG -->
        <div class="form-group col-md-4">
            <label for="codigo_siasg">Código SIASG</label>
            <input   name="codigosiasg" class="form-control @error('codigosiasg') is-invalid @enderror"
                   id="codigosiasg" value="{{ old('codigosiasg', $orgao->codigosiasg ?? '') }}" maxlength="6"
                   pattern="\d{6}" required>
            @error('codigosiasg')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <!-- Código SIAFI -->
        <div class="form-group col-md-4">
            <label for="codigo">Código SIAFI</label>
            <input type="text" maxlength="5" name="codigo" class="form-control @error('codigo') is-invalid @enderror" id="codigo"
                   value="{{ old('codigo', $orgao->codigo ?? '') }}"pattern="\d{5}" required>
            @error('codigo')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <!-- Nome -->
        <div class="form-group col-md-4">
            <label for="nome">Nome</label>
            <input type="text" name="nome" class="form-control @error('nome') is-invalid @enderror" id="nome"
                   value="{{ old('nome', $orgao->nome ?? '') }}" required>
            @error('nome')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="row">
        <!-- Órgão Superior -->
        <div class="form-group col-md-4">
        <label for="orgaosuperior_id">Órgão Superior</label>
        <select name="orgaosuperior_id" class="form-control orgao-superior @error('orgaosuperior_id') is-invalid @enderror"
                id="orgaosuperior_id" required>
            <option value="">Selecione um Órgão Superior</option>
            @foreach($orgaosSuperior as $orgaoItem)
                <option
                    value="{{ $orgaoItem->id }}" {{ (old('orgaosuperior_id', $orgao->orgaosuperior_id ?? '') === $orgaoItem->id) ? 'selected' : '' }}>
                    {{ $orgaoItem->codigo . '-' . $orgaoItem->nome }}
                </option>
            @endforeach
        </select>
        @error('orgaosuperior_id')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>

    <!-- Situação -->
    <div class="form-group col-md-4">
        <label for="situacao">Situação</label>
        <select name="situacao" class="form-control @error('situacao') is-invalid @enderror" id="situacao">
            <option value="1" {{ old('situacao', $orgao->situacao ?? 1) == 1 ? 'selected' : '' }}>Ativo</option>
            <option value="0" {{ old('situacao', $orgao->situacao ?? 1) == 0 ? 'selected' : '' }}>Inativo</option>
        </select>
        @error('situacao')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>

    <!-- CNPJ -->
    <div class="form-group col-md-4">
        <label for="cnpj">CNPJ</label>
        <input type="text" name="cnpj" class="form-control cnpj @error('cnpj') is-invalid @enderror" id="cnpj"
               value="{{ old('cnpj', $orgao->cnpj ?? '') }}" maxlength="14" pattern="\d{2}\.\d{3}\.\d{3}/\d{4}-\d{2}">
        @error('cnpj')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    </div>

    <div class="row">
        <!-- Gestão -->
        <div class="form-group col-md-4">
            <label for="gestao">Gestão</label>
            <input type="text" name="gestao" class="form-control @error('gestao') is-invalid @enderror" id="gestao"
                   value="{{ old('gestao', $orgao->gestao ?? '') }}" maxlength="5" pattern="\d{1,6}">
            @error('gestao')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <!-- Utiliza Centro de Custo -->
        <div class="form-group col-md-4">
            <label for="utiliza_centro_custo">Utiliza Centro de Custo</label>
            <select name="utiliza_centro_custo" class="form-control @error('utiliza_centro_custo') is-invalid @enderror"
                    id="utiliza_centro_custo">
                <option
                    value="1" {{ old('utiliza_centro_custo', $orgao->utiliza_centro_custo ?? 0) == 1 ? 'selected' : '' }}>
                    Sim
                </option>
                <option
                    value="0" {{ old('utiliza_centro_custo', $orgao->utiliza_centro_custo ?? 0) == 0 ? 'selected' : '' }}>
                    Não
                </option>
            </select>
            @error('utiliza_centro_custo')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <!-- Campos ocultos para timestamps -->
        <input type="hidden" name="deleted_at" value="{{ old('deleted_at', $orgao->deleted_at ?? '') }}">
        <input type="hidden" name="created_at" value="{{ old('created_at', $orgao->created_at ?? '') }}">
        <input type="hidden" name="updated_at" value="{{ old('updated_at', $orgao->updated_at ?? '') }}">
    </div>


    <button type="submit" class="btn btn-primary">
        @if($method == 'PUT')
            Editar
        @else
            Cadastrar
        @endif
    </button>

    <a href="/administracao/orgaos" class="btn btn-default">Cancelar</a>
</form>
@section('after_scripts')
    <script>
        $(function () {
            $('.orgao-superior').select2({
                placeholder: "Selecione um Órgão Superior",
                allowClear: true
            });
            $(".cnpj").mask('99.999.999/9999-99')
        })
    </script>
@endsection
