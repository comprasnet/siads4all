@extends(backpack_view('blank'))

@section('content')
    <section class="container mt-5">
        <h2>Editar Órgão</h2>
        @include('administracao.orgaos.partials.form', ['action' => route('orgaos.update',['id'=>$orgao->id]), 'method' => 'PUT'])
    </section>
@endsection
