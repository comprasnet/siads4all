@extends(backpack_view('blank'))

@section('content')
    <section class="container mt-5">
        <h2>Cadastrar Órgão</h2>
        @include('administracao.orgaos.partials.form', ['action' => route('orgaos.store'), 'method' => 'POST'])
    </section>
@endsection
