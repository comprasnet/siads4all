@extends(backpack_view('blank'))
@section('content')
<div class="container">
    <h1>Detalhes do Órgão</h1>

    <div class="card">
        <div class="card-header">
            Detalhes do Órgão
        </div>
        <div class="card-body">
            <h5 class="card-title">Nome: {{ $orgao->nome }}</h5>
            <p class="card-text">Código: {{ $orgao->codigo }}</p>
            <p class="card-text">Código SIASG: {{ $orgao->codigosiasg }}</p>
            <p class="card-text">ID Sistema Origem: {{ $orgao->id_sistema_origem }}</p>
            <p class="card-text">CNPJ: {{ $orgao->cnpj }}</p>
            <p class="card-text">Situação: {{ $orgao->situacao ? 'Ativo' : 'Inativo' }}</p>
            <p class="card-text">Gestão: {{ $orgao->gestao }}</p>
            <p class="card-text">Daas: {{ $orgao->daas ? 'Sim' : 'Não' }}</p>
            <p class="card-text">Utiliza Centro Custo: {{ $orgao->utiliza_centro_custo }}</p>
            <p class="card-text">Tipo Administração: {{ $orgao->tipo_administracao }}</p>
            <p class="card-text">Poder: {{ $orgao->poder }}</p>
            <p class="card-text">Esfera: {{ $orgao->esfera }}</p>
            <p class="card-text">Criado em: {{ $orgao->created_at }}</p>
            <p class="card-text">Atualizado em: {{ $orgao->updated_at }}</p>

            <a href="{{ route('orgaos.edit', $orgao->id) }}" class="btn btn-warning">Editar</a>
            <form action="{{ route('orgaos.destroy', $orgao->id) }}" method="POST" style="display:inline;">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Excluir</button>
            </form>
            <a href="{{ route('orgaos.index') }}" class="btn btn-primary">Voltar</a>
        </div>
    </div>
</div>
@endsection
