@if($data['data_agendamento'])
    <div  class="files-center">
        <a title="Cancelar agendamento" onclick="return confirm('Deseja cancelar esse agendamento?')" href="/files/cancela-agendamento/{{$data['id']}}"
           class="btn btn-default btn-small">
            <i style="color: #F00; font-size: 13px !important;" class="fa fa-ban"></i>
        </a>
    </div>
@else
    @if($data['complete'])
        <div class="files-center">
            <a class="btn btn-primary btn-small mr-2" href="/files/download/{{$data['id']}}"><i
                    class="fa fa-download"></i></a>
            @php
                $location = explode('.',$data['file_location']);
                $nome = explode('/',$data['file_location']);
                $ext = end($location);
                $cols = $data;
                $cols['created_at'] = date('d/m/Y H:i',strtotime($cols['created_at']));
                $cols['nome'] = end($nome);
            @endphp
            {{-- <button class="view-file btn btn-primary mr-2" data-cols-name="id,nome,created_at" data-cols-label="Cod,Nome,data de criação"  data-cols="{{json_encode($cols)}}"  data-file="{{$data['file_location']}}"> <i class="fa fa-eye"></i></button> }} --}}
            <a onclick="return confirm('Deseja remover esse registro?')" href="/files/remove/{{$data['id']}}"
               class="btn btn-default btn-small"><i class="fa fa-trash"></i></a>
        </div>
    @else
        <div class="files-center">
            <progress max="100" class="progress-file" data-id="{{$data['id']}}" id="progress{{$id}}"></progress>
        </div>
    @endif
@endif
