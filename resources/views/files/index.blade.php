@extends(backpack_view('blank'))
@section('content')
    @include('gov-theme.widgets.breadcrumbs')
    <div class="br-table files-table" data-search="data-search" data-selection="data-selection" data-collapse="data-collapse"
        data-random="data-random">
        <div class="table-header" style="position: static">
            <div class="top-bar">
                <div class="table-title">Meus Arquivos Gerados de Relatórios</div>
            </div>
        </div>
        {!! $dataTable->table() !!}
    </div>
@endsection
@section('after_scripts')
    {!! $dataTable->scripts() !!}

    <script>
        $(function () {
            let table = LaravelDataTables['files-table'];
            table.on('draw', function() {
                $(".progress-file").each(function(){
                    let id = $(this).data('id');
                    pctItem($(this),id);
                });
                $('.view-file').each(function(){
                    let html = '';
                    let cols = $(this).data("cols");
                    let names = $(this).data("cols-name").split(',');
                    let labels = $(this).data("cols-label").split(',');
                    for(let i=0;i<names.length;i++){
                        html += '<b>'+labels[i]+':</b>';
                        if(names[i]=='nome'){
                            cols[names[i]] = '<a target="_blank" href="/files/download/'+cols['id']+'">'+
                                cols[names[i]]+'</a>';
                        }
                        html += cols[names[i]];
                        html += '<br>';
                    }
                    $(this).click(function(){
                        const modal = $.dinamicModal('', html, [
                            { name: 'Fechar', type: 'default', action: () => resolve(), close: true },
                            { name: 'Abrir arquivo', type: 'primary', action: () => {
                                window.location=cols.file_location
                                },close:true },
                        ]);
                        modal.style.display = 'block';
                    });
                })
            });
        });
        let pctItem = (elemtprogress, id) => {
            let geralpct = 0
            let ajaxProgress = () => {
                $.ajax({
                    url: '/files/progress/'+id,
                    dataType:'json',
                    success: function(json){
                        if(json.pct>0){
                            $(elemtprogress).val(json.pct)
                            if(json.pct<100) {

                                $(elemtprogress).attr('data-label',json.pct+'%');
                                $(elemtprogress).html(json.pct + '%');
                            }else{
                                $(elemtprogress).attr('data-label','Gerando arquivo...');
                                $(elemtprogress).html('Dados carregados, gerando arquivo');
                            }
                        }else{
                            $(elemtprogress).attr('data-label','Carregando dados ...');
                            $(elemtprogress).html('Carregando dados ...');
                            $(elemtprogress).removeAttr('val');
                        }
                        geralpct = json.pct;
                        if(!json.complete){

                            execTimeOut();
                        }else{
                            window.location.reload()
                        }
                    }
                });
            }

            let execTimeOut = () => {
                setTimeout(function(){
                    ajaxProgress();
                },1000);
            }
            ajaxProgress();
        }
    </script>
@endsection
