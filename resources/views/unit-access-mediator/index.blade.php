@extends(backpack_view('blank'))
@section('content')
    @include('gov-theme.widgets.breadcrumbs')
    {{ $dataTable->table() }}

    <!-- Modal -->
    <div class="br-scrim-util foco" id="modal-permissao-usuario" data-scrim="true">
        <div style="width: 400px" class="br-modal" aria-labelledby="titulomodalexemplo">
            <div class="br-modal-header" id="titulomodalexemplo">Permissão de Usuário</div>
            <div class="br-modal-body modalContent">

            </div>
            <div class="br-modal-footer justify-content-center">
                <button class="br-button primary mt-3 mt-sm-0 ml-sm-3 save" type="button">Salvar
                </button>
                <button class="br-button secondary cancel" type="button" data-dismiss="true">Cancelar
                </button>
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    <script>

        // Lógica para salvar o formulário do modal (se necessário)
        $('#saveModal').on('click', function () {
            // Adicione aqui a lógica para salvar os dados do formulário via AJAX, se necessário
            // Por exemplo, você pode pegar os valores dos campos e enviar para uma rota POST
        });
        function approveUser(id){
            $.ajax({
                url: '{{ route("load.modal.content") }}',
                data: {id:id},
                method: 'GET',
                success: function (response) {
                    $('.modalContent').html(response);
                    $('#modal-permissao-usuario').show();
                    $('#group').select2();
                    setPermissionByRole();
                }
            });
        }
        function blockUser(id){
            $.ajax({
                url: '{{ route("load.block.user") }}',
                data: {id:id},
                method: 'GET',
                success: function (response) {
                    alert(response.message)
                }
            });
        }
        function setPermissionByRole(){
            $('#group').change(function(){
                let roles = $(this).val();
                $.ajax({
                    url: '/unit/permission-by-roles',
                    data: {roles: roles},
                    success: function(data){
                        $('input[name="permissions[]"]').each(function(){
                            let val = parseInt($(this).val());
                            if(data.includes(val)){
                                $(this).attr("checked",true);
                            }
                        });
                    }
                })
            })
        }

        function savePermissaoUsuario(){
            $('#modal-permissao-usuario .save').click(function(){
                let data = $('#modal-permissao-usuario form').serialize();
                $.ajax({
                    url:'/unit/save-permission',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function(data){
                        $('#modal-permissao-usuario').hide();
                        alert(data.message);
                        LaravelDataTables["unitaccessmediator-table"].ajax.reload()
                    }
                });
            })
        }
        savePermissaoUsuario();
        $('#modal-permissao-usuario .cancel').click(function () {
            $(this).parents('.br-scrim-util').hide();
        })
    </script>
@endsection
