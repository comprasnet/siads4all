<!-- Formulário dentro do modal -->
<form id="modalForm">
    @csrf
    <input type="hidden" name="unit_access" value="{{$id}}">
    <!-- Campo de seleção múltipla (grupo multi select2) -->
    <div class="form-group">
        <label class="d-block fw-bold" for="group">Grupo</label>
        <select class="form-control" name="roules[]" id="group" multiple>
            @foreach($groups as $group)
                <option value="{{$group->id}}">{{$group->name}}</option>
            @endforeach
        </select>
    </div>

    <!-- Checkboxes de permissões de usuário -->
    <div class="form-group">
        <label>Permissões de Usuário</label>
        @foreach($permissions as $permission)
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="permission{{$permission->id}}" name="permissions[]" value="{{$permission->id}}">
                <label class="form-check-label" for="permission{{$permission->id}}">
                    {{$permission->name}}
                </label>
            </div>
        @endforeach
    </div>
</form>
