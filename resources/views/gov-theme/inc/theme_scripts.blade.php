@basset('/packages/backpack/base/js/bundle.js')
@basset('/packages/backpack/base/js/padrao_gov/core-init.js')
@basset('/packages/backpack/base/js/padrao_gov/jquery.mask.js')
@basset('/packages/select2/dist/js/select2.js')
@basset('/packages/select2/dist/js/i18n/pt-br.js')
@basset('/packages/datatables.net-processing/js/processing.js')
@basset('/packages/datatables.net/js/jquery.dataTables.js')
@basset('/assets/js/datatables/actions.js')
@basset('/assets/js/modal/modal.gov.br.js')
@basset('/assets/js/default.js')
<script>
    $(function() {
        $.ajaxSetup({
            headers : {
                'CSRFToken' : '{{ csrf_token() }}'
            }
        });
    });
</script>
