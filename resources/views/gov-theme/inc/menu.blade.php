@php
    $titulo = null;
    $subTitulo = null;
    if (isset($crud)) {
        $titulo = $crud->getHeading() ?? $crud->entity_name_plural;
        $subTitulo =
            $crud->getSubheading() . ' ' . $crud->entity_name ??
            mb_ucfirst(trans('backpack::crud.preview')) . ' ' . $crud->entity_name;
    }
    $uasgUsuario = session('user_ug');
    $unidadesUsuario = [];
    $unidadeCurrent = app(\App\Services\UnidadeChangerService::class)->getCurrentUnidade();

    /* @var \App\Services\PermissionsService $permissions */
    $permissions = app(\App\Services\PermissionsService::class);
    $isSuper = auth()->user()->super_admin;
@endphp
<div class="container-lg">
    <div class="header-top">
        <div class="header-logo">

            <div class="header-menu-trigger" id="header-navigation">
                <!-- <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class='nav-icon la la-bars'></i>
                </button> -->
            </div>


            <img src="{{ backpack_theme_config('project_logo') }}" alt="Siads">
            <span class="br-divider vertical mx-2"></span>

            <div class="header-sign">
                <div class="header-title">
                    <div class="header-title">
                        {{ backpack_user()->name }} @if (!is_null($unidadeCurrent))
                            - UG: {{ $unidadeCurrent->codigo ?? '' }}
                        @endif
                    </div>
                </div>

            </div>

        </div>
        <div class="header-actions">

            <span class="br-divider vertical mx-half mx-sm-1"></span>

            <div class="header-login">
                <div class="header-sign-in">

                </div>
                <div class="header-avatar"></div>

                <div class="header-info">
                    <div class="scrimutilexemplo" style="display: contents">
                        <button id="btn-altera-unidade" class="br-button circle" type="button">
                            <i class="fas fa-exchange-alt"></i>
                        </button>

                        <div class="br-tooltip" role="tooltip" info="info" place="top">
                            <span class="text" role="tooltip">
                                Alterar Unidade
                            </span>
                        </div>

                    </div>

                    <div id="notification-menu" class="dropdownpure" style="position: relative;">
                        @include('gov-theme.inc.bell-notify')
                    </div>

                    <button class="br-button circle" type="button" onclick="window.location.href='/logout'">
                        <i class="fas fa-sign-out-alt"></i>
                    </button>
                </div>


            </div>

        </div>
    </div>
    <div class="row">
        <div class="br-menu" id="main-navigation">
            <div class="menu-container">
                <div class="menu-panel">
                    <div class="menu-header">
                        <div class="menu-title"><img src="{{ backpack_theme_config('project_logo') }}    "
                                                     alt="Imagem ilustrativa"/><span></span></div>
                        <div class="menu-close">
                            <button class="br-button circle" type="button" aria-label="Fechar o menu"
                                    data-dismiss="menu"><i class="fas fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    @if (!is_null($unidadeCurrent))
                        <nav class="menu-body">
                            <a class="menu-item" href="/patrimony">
                                <span class="icon">
                                    <i class="fas fa-list" aria-hidden="true"></i>
                                </span>
                                <span class="content">Siads Patrimônios</span>
                            </a>
                            <a class="menu-item" href="/files">
                                <span class="icon">
                                    <i class="fas fa-file" aria-hidden="true"></i>
                                </span>
                                <span class="content">Arquivos de Relatórios</span>
                            </a>
                            @if ($permissions->hasPermission(\App\Enums\EnumPermissions::ADMIN_USERS->value))
                                <a class="menu-item" href="/unit/approve">
                                    <span class="icon">
                                        <i class="fas fa-user" aria-hidden="true"></i>
                                    </span>
                                    <span class="content">Pedidos de Permissão</span>
                                </a>
                                <a class="menu-item" href="/users">
                                    <span class="icon">
                                        <i class="fas fa-users" aria-hidden="true"></i>
                                    </span>
                                    <span class="content">Usuários</span>
                                </a>
                            @endif
                            @if (
                            $permissions->hasPermission([
                                \App\Enums\EnumPermissions::ADMIN_ORGAOS->value,
                                \App\Enums\EnumPermissions::ADMIN_UNIT->value,
                            ]))
                                <div class="menu-folder"><a class="menu-item" href="javascript: void(0)"
                                                            role="treeitem"><span class="icon"><i class="fas fa-bell"
                                                                                                  aria-hidden="true"></i></span><span
                                            class="content">Administração</span></a>

                                    <ul>
                                        @if ($permissions->hasPermission([\App\Enums\EnumPermissions::ADMIN_ORGAOS->value]))
                                            <li>
                                                <a class="menu-item" href="/administracao/orgaos">
                                                <span class="icon">
                                                    <i class="fas fa-building" aria-hidden="true"></i>
                                                </span>
                                                    <span class="content">Orgãos</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if ($permissions->hasPermission([\App\Enums\EnumPermissions::ADMIN_UNIT->value]))
                                            <li>
                                                <a class="menu-item" href="/administracao/unidades">
                                                <span class="icon">
                                                    <i class="fas fa-building" aria-hidden="true"></i>
                                                </span>
                                                    <span class="content">Unidades</span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                            @endif
                        </nav>
                    @endif
                    <div class="comment data-ambiente" style="color: var(--interactive);">
                        <i class='la la la-slack'></i> {!! config('app.app_amb') !!} - {{ config('app.server_node') }} |
                        v: {{  config('app.app_version') }}
                    </div>
                </div>

                <div class="menu-scrim" data-dismiss="menu" tabindex="0">

                </div>
            </div>
        </div>
        @if (!is_null($unidadeCurrent))
            <div class="header-menu">
                <div class="header-menu-trigger" id="header-navigation">
                    <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu"
                            data-target="#main-navigation" id="navigation" control-id="ControlID-4"><i
                            class="fas fa-bars" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="header-info">
                    <div class="header-title">{!! $titulo !!}</div>
                    <div class="header-subtitle">{!! $subTitulo !!}</div>
                </div>
            </div>
        @endif
    </div>
</div>
<div class="br-scrim-util foco model-alteracao-unidade" id="alterar-unidade">

    <form action="/change-unidade" class="div br-modal medium" aria-modal="true" data-scrim="true" role="dialog"
          aria-labelledby="modalalerttitle">
        <div class="br-modal-header">
            <div class="modal-title" id="modalalerttitle">Alterar Unidade</div>
            <button class="br-button close circle" type="button" data-dismiss="br-modal" aria-label="Fechar"><i
                    class="fas fa-times" aria-hidden="true"></i>
            </button>
        </div>
        <div class="br-modal-body">
            <form>
                <div>Selecione a unidade</div>
                <select name="unidade" class="select-unidade">
                    @foreach ($unidadesUsuario as $unidade)
                        <option @if (($unidadeCurrent->id ?? '') == ($unidade->id ?? '')) selected
                                @endif value="{{ $unidade->id }}">
                            {{ $unidade->codigo }} - {{ $unidade->nome }}</option>
                    @endforeach
                </select>
                <hr>
                <button class="br-button primary" type="submit">
                    Alterar
                </button>
                <button type="button" class="br-button secondary cancela-alteracao-unidade">
                    Cancelar
                </button>

                @if(!$isSuper)
                    <button type="button" class="br-button secondary solicitar-acesso-unidade">
                        Solicitar acesso
                    </button>
                @endif
            </form>
        </div>
    </form>
</div>
<style>
    .select2-container.select2-container--default.select2-container--open {
        z-index: 99999999999 !important;
    }
</style>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="br-message danger message-fast">
            <div class="icon"><i class="fas fa-times-circle fa-lg" aria-hidden="true"></i>
            </div>
            <div class="content" aria-label="{{ $error }}" role="alert">
                {{ $error }}
            </div>
            <div class="close">
                <button class="br-button circle small" type="button" aria-label="Fechar a messagem alterta"><i
                        class="fas fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    @endforeach
@endif

@if (Session::has('success'))
    @foreach ($errors->all() as $error)
        <div class="br-message success message-fast">
            <div class="icon"><i class="fas fa-times-circle fa-lg" aria-hidden="true"></i>
            </div>
            <div class="content" aria-label="{{ Session::get('message') }}" role="alert">
                {{ Session::get('message') }}
            </div>
            <div class="close">
                <button class="br-button circle small" type="button" aria-label="Fechar a messagem alterta"><i
                        class="fas fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    @endforeach
@endif
<div class="loading-site general">
    <div class="loader">
        <div class="br-loading medium" role="progressbar" aria-label="carregando exemplo medium exemplo"></div>
    </div>
</div>

@push('after_scripts')
    <script src="/assets/js/notification.js"></script>
    <script>
        $.fn.select2.defaults.set("allowClear", true);
        $(function () {
            if ($('.message-fast').length > 0) {
                setTimeout(function () {
                    $('.message-fast').fadeOut('slow');
                }, 5000);
            }
            $('#notification-menu').click(function () {
                $.ajax({
                    url: '/notification/clear',
                    type: 'POST'
                });
                $(this).find('.badge').hide();
                setTimeout(function () {
                    $('#notification-menu').find('.br-item b').each(function () {
                        let a = $(this).parent();
                        let text = $(this).html();
                        a.html(text);
                    })
                }, 2000);
            })
        })
        $('#btn-altera-unidade').click(function () {
            $('.model-alteracao-unidade').show();
        })
        $('.solicitar-acesso-unidade').click(function () {
            $.promptSelect('', 'Digite o código da unidade:', (modal) => {
                $(modal).find('select').select2Unidade();
                $(modal).find('.br-modal').width(400).height(200);
            }).then(cod => {
                if (cod && cod != '') {
                    $.ajax({
                        url: '/unit/solicitar?cod=' + cod,
                        dataType: 'JSON',
                        success: function (response) {
                            $.alert(response.message);
                            if (response.success)
                                $('.model-alteracao-unidade').hide();
                        }
                    });
                }
            })
        })
        $(document).ready(function () {
            $('.select-unidade').select2({
                ajax: {
                    url: '/unit/by-user',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term // termo de busca
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                placeholder: 'Procurar Unidade',
            });
            $('.cancela-alteracao-unidade,.model-alteracao-unidade .close ').click(function () {
                $('.model-alteracao-unidade').hide();
            })
        });

    </script>
    <script>
        var dropdown = document.querySelector('.dropdownpure');
        var dropdownContent = document.querySelector('.dropdown-content');
        dropdown.addEventListener('click', function (event) {
            dropdown.classList.toggle('active');
        });
        document.querySelector('body').addEventListener('click', function (event) {
            if (!dropdown.contains(event.target)) {
                dropdown.classList.remove('active');
            }
        })
    </script>
@endpush
