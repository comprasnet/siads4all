<footer class="d-print-none" style="">
    <footer class=" br-footer pt-3" id="footer">
        <div class="container-lg">
            <div class="info">
                <div class="text-down-01 text-medium pb-3">
                    &nbsp;Copyright © 2024
                    <strong><a href="#">Siads4All</a> </strong>- Todos direitos reservados. Software Livre (GPL)
                </div>
            </div>
        </div>
    </footer>
</footer>
