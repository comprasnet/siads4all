@php
    use App\Services\NotificationService;
    use Illuminate\Support\Facades\Auth;
    $notificationsService = app(NotificationService::class);
    /* @var NotificationService $notificationsService */
    $notifications = $notificationsService->getNoExpiredNotifications(Auth::user());
    $notificationsNumber = $notificationsService->getCountUnreadNotifications(Auth::user());
@endphp
@if ($notificationsNumber > 0)
    <span class="badge badge-danger badge-circle">{{ $notificationsNumber }}</span>
@endif
<span class="br-button circle"><i class="fa fa-bell"></i></span>
<div class="dropdown-content">
    @foreach ($notifications as $notification)
        <a class="br-item" href="{{ $notification->link }}">
            @if (!$notification->read)
                <b>{{ $notification->message }}</b>
            @else
                {{ $notification->message }}
            @endif
        </a>
    @endforeach
    @if (count($notifications) === 0)
        <span class="dropdown-item">Sem notificações</span>
    @endif
</div>
