@php
        $paths = explode('.',Route::current()->getName());
        $main = $paths[0];
        if(!Route::has($paths[0])){
            $main = $paths[0].'.index';
        }
        $nameRoute = config('breadcrumbs.names.'.$main)??$main;
        $defaultBreadcrumbs = [
            trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
            $nameRoute => $main,
            trans('backpack::crud.list') => false,
        ];
        // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
        $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp
<div class="br-breadcrumb">
    <ul class="crumb-list">
        @foreach ($breadcrumbs as $label => $link)
            @if ($label == 'Icone')
                @php
                    $dadosIcone = json_decode($link, true);

                    $label = '<a  href= "' . $dadosIcone['url'] . '"> Voltar</a>';
                    $link = false;
                @endphp
            @endif
            @if ($link)
                @if ($label == 'Admin')
                    <li class="crumb home"><a class="br-button circle" href="{{ $link }}"><span
                                class="sr-only">Página inicial</span><i class="fas fa-home"></i></a></li>
                @else
                    @if ($label == 'Início')
                        <li class="crumb ">
                            <a class="br-button circle" href="{{ $link }}">
                                <span class="sr-only">Página inicial</span>
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                    @else
                        <li class="crumb "><i class="icon fas fa-chevron-right f100"></i><a
                                href="{{ $link }}">{{ $label }}</a></li>
                    @endif
                @endif
            @else
                @if ($label !== 'Inicio')
                    <li class="crumb" data-active="active"><i class="icon fas fa-chevron-right"></i> <span>
                                {!! $label !!} </span> </li>
                @endif
            @endif
        @endforeach
    </ul>
</div>
