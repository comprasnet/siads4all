@extends(backpack_view('blank'))
@section('content')
    @include('gov-theme.widgets.breadcrumbs')
{{--    <div class="br-card h-fixed">
        <div class="card-header">
            Filtrar
        </div>
        <div class="card-body">
            <form action="" class="filter">
                <div class="row">
                    <div class="col-md-3">
                        <label for="codigo">Código:</label>
                        <input type="text" class="form-control" id="codigo" value="{{ request('codigo') }}">
                    </div>
                    <div class="col-md-3">
                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" id="nome" value="{{ request('nome') }}">
                    </div>
                    <div class="col-md-3">
                        <label for="cpf">CPF:</label>
                        <input type="text" class="form-control" id="cpf" value="{{ request('cpf') }}">
                    </div>
                    <div class="col-md-3">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" value="{{ request('email') }}">
                    </div>
                    --}}{{--<div class="col-md-12">
                        <div class="btn-hr">
                            <button type="button" class="btn" id="toggleCampos">
                                <i class="fas fa-arrow-down"></i>
                            </button>
                        </div>
                    </div>--}}{{--
                    <div class="col-md-12" style="display: flex;justify-content: end">
                        <button type="submit" class="btn btn-default" id="toggleCampos">
                            Filtrar <i class="fa fa-filter"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>--}}
    <div class="list-btns">
        <a href="/users/create" type="submit" class="btn btn-primary" control-id="ControlID-5">Cadastrar Usuário</a>
    </div>
    {{ $dataTable->table() }}
@endsection

@section('after_scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    <script>
        $(document).ready(function() {
            $('.select2').select2();

            $('#toggleCampos').click(function() {
                $('#grupoDiv, #uasgDiv, #permissoesDiv').toggle();
                var btnText = $(this).text();
                $(this).find('i').toggleClass('fa-arrow-down fa-arrow-up');
            });
        });
    </script>

@endsection
