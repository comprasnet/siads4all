    @extends(backpack_view('blank'))

@section('content')

    <section class="container mt-5">
        <form action="/users/create" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $user->id ?? '' }}">
            <h2>Cadastro de Usuário</h2>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="dados-pessoais-tab" data-toggle="tab" href="#dados-pessoais"
                       role="tab" aria-controls="dados-pessoais" aria-selected="true">Dados Pessoais</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="outros-tab" data-toggle="tab" href="#outros" role="tab"
                       aria-controls="outros" aria-selected="false">Unidades e Permissões</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="dados-pessoais" role="tabpanel"
                     aria-labelledby="dados-pessoais-tab">

                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cpf">CPF:</label>
                                <input @if(isset($user) && !is_null($user->id)) readonly @endif type="text" class="form-control" id="cpf" name="cpf" placeholder="Digite seu CPF"
                                       value="{{ old('cpf', $user->cpf ?? '') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Nome Completo:</label>
                                <input @if(isset($user) && !is_null($user->id)) readonly @endif  type="text" class="form-control" id="nome" name="name"
                                       placeholder="Digite seu nome completo"
                                       value="{{ old('name', $user->name ?? '') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Digite seu email" value="{{ old('email', $user->email ?? '') }}"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="matricula_siape">Matrícula SIAPE:</label>
                                <input type="number" class="form-control" id="matricula_siape" name="matricula_siape"
                                       placeholder="" value="{{ old('matricula_siape', $user->matricula_siape ?? '') }}"
                                       >
                            </div>
                        </div>
                        
                        <div class="col-md-6 show-super" @if((isset($user) and !$user->super_admin) or !isset($user))style="display:none"@endif>
                            @php
                                $valueUnidadePadrao = isset($user)?$user->unidade_id:false;
                                $labelUnidadePadrao = null;
                                if($valueUnidadePadrao){
                                    $unidadePadrao = \App\Models\Unidade::find($valueUnidadePadrao);
                                    if($unidadePadrao){
                                        $labelUnidadePadrao = $unidadePadrao->codigo.' - '.$unidadePadrao->nome;
                                    }
                                }
                            @endphp
                            <div class="form-group">
                                <label class="control-label" for="default_unit">Unidade Padrão:</label>
                                <select class="form-control" name="default_unit_admin" id="default_unit">
                                    @if($valueUnidadePadrao and $labelUnidadePadrao)
                                        <option value="{{$valueUnidadePadrao}}">{{$labelUnidadePadrao}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 show-super" @if((isset($user) and !$user->super_admin) or !isset($user))style="display:none"@endif></div>
                        @if(auth()->user()->super_admin)
                            <div class="col-md-6" style="display: flex">
                                <div class="br-checkbox" style="display: flex;align-items: center">
                                    <input type="checkbox" id="super_admin" aria-label="super admin" value="1"
                                           name="super_admin" {{ old('super_admin', isset($user) ? ($user->super_admin ? 'checked' : '') : '') }}>
                                    <label for="super_admin">Administrador do Sistema</label>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="tab-pane fade" id="outros" role="tabpanel" aria-labelledby="outros-tab">
                    <div class="adicionar-unidades-title">
                        <button type="button" class="br-button primary">
                            Adicionar Unidade <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <div class="row" id="list-units">

                    </div>
                    <input type="hidden" id="unidades-permitidas" name="unidades">
                </div>
            </div>

            <a href="/users" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>
    </section>
@endsection

<div id="base-unit" style="display: none">
    <div class="card card-user-units">
        <div class="card-body">
            <div class="card-title">
                <b class="unidade-codigo"></b> - <span class="unidade-nome"></span>
                <div class="default-unit">
                    <label>
                        Unidade padrão
                        <input type="radio" name="unit-default" class="unit-default">
                    </label>
                </div>
            </div>
            <div class="card-text">
                <button type="button" class="br-button circle primary change-item">
                    <i class="fa fa-edit"></i>
                </button>
                <button type="button" class="br-button circle secondary remove-item">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="br-scrim-util foco" style="display: none" id="modal-add-unit" data-scrim="true">
    <form class="br-modal" aria-labelledby="titulomodalexemplo">
        <div class="br-modal-header" id="titulomodalexemplo">Permissão de Unidade</div>
        <div class="br-modal-body" style="width: 500px">

            <div class="form-group">
                <label class="d-block fw-bold" for="unit">Unidade</label>
                <select required class="form-control" name="unit_id" id="unit">
                    @foreach($unidades as $unidade)
                        <option value="{{$unidade->id}}">{{$unidade->codigo}} - {{$unidade->nome}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Campo de seleção múltipla (grupo multi select2) -->
            <div class="form-group">
                <label class="d-block fw-bold" for="group">Grupo</label>
                <select class="form-control" required name="roles[]" id="group" multiple>
                    @foreach($groups as $group)
                        <option value="{{$group->id}}">{{$group->name}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Checkboxes de permissões de usuário -->
            <div class="form-group">
                <label>Permissões de Usuário</label>
                @foreach($permissions as $permission)
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="permission{{$permission->id}}"
                               name="permissions[]" value="{{$permission->id}}">
                        <label class="form-check-label" for="permission{{$permission->id}}">
                            {{$permission->name}}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" data-dismiss="true">Cancelar
            </button>
            <button class="br-button primary mt-3 mt-sm-0 ml-sm-3 add-unidade" type="button">Adicionar
            </button>
        </div>
    </form>
</div>
@section('after_scripts')
    <script src="/assets/js/users/users-form.js"></script>
    <script>
        unitRecords.load({!! json_encode($unidadesCadastradas) !!});
        $('#super_admin').click(function () {
            let outros = $("#outros-tab");
            let showSuper = $('.show-super');
            if ($(this).is(':checked')) {
                outros.hide();
                showSuper.show();
                return;
            }
            showSuper.hide();
            outros.show();
        })
        if($('#super_admin').is(':checked')) {
            $("#outros-tab").hide();
        }
    </script>
@endsection
