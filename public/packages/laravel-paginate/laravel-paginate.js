class LaravelPaginate {

    /**
     * Paginator recebido via API JSON
     * @type {{per_page: number, total: number, data: [], last_page: number, next_page_url: string, from: number, to: number, prev_page_url: null, current_page: number}}
     */
    #paginator = {};

    /**
     * Seletor onde será desenhado a paginação
     * @type {string}
     */
    #selector = ""

    /**
     * Numero máximo de links que irá aparecer
     * @type {number}
     */
    #maxNumberPages = 5;

    #currentPage = 1;

    onChangePage = (currentPage) => {
    };

    constructor(selector) {
        this.#selector = selector;
    }

    #getElementNumber(number, isDefault = false) {
        let li = document.createElement('li');
        let a = document.createElement('a');
        a.text = number;

        a.classList.add('page');
        a.setAttribute('href', 'javascript:void(0)');
        if (isDefault) {
            a.classList.add('active');
        }

        a.onclick = () => {
            this.#changePage(number);
        }

        li.append(a);
        return li;
    }

    #getElementButtonNext() {
        let button = document.createElement('button');
        let i = document.createElement('i');
        button.classList.add('br-button', 'circle');
        button.setAttribute('data-next-page', 'data-next-page');
        button.setAttribute('label', 'Página seguinte');

        i.classList.add('fas', 'fa-angle-right');
        i.setAttribute('aria-hidden', 'true');


        button.onclick = () => {
            this.#changePage(this.#currentPage + 1);
        }

        button.append(i);
        return button;
    }


    #getElementButtonPrev() {
        let button = document.createElement('button');
        let i = document.createElement('i');
        button.classList.add('br-button', 'circle');
        button.setAttribute('data-prev-page', 'data-prev-page');
        button.setAttribute('label', 'Página anterior');

        i.classList.add('fas', 'fa-angle-left');
        i.setAttribute('aria-hidden', 'true');


        button.onclick = () => {
            this.#changePage(this.#currentPage - 1);
        }

        button.append(i);
        return button;
    }

    /**
     * Seta o paginador que vem do laravel
     * @param paginator
     * @returns {LaravelPaginate}
     */
    setPaginator(paginator) {
        this.#paginator = paginator;
        this.#currentPage = this.#paginator.current_page;
        return this;
    }

    #changePage(curPage) {
        this.onChangePage(curPage);
    }

    #getNumberByIndexPage(i) {
        if (this.#maxNumberPages === 0) {
            return this.#getElementNumber(i, i === this.#currentPage);
        }
        let max = this.#maxNumberPages - 1;
        if (i > (this.#currentPage - max) && i < (this.#currentPage + max)) {
            return this.#getElementNumber(i, i === this.#currentPage);
        }
        return false;
    }

    setDataSelector() {
        let elementObj = document.querySelector(this.#selector);
        elementObj.classList.add('br-pagination');
        elementObj.setAttribute('aria-label', 'Paginação de resultados');
        elementObj.setAttribute('data-total', this.#paginator.last_page);
        elementObj.setAttribute('data-current', this.#currentPage);
        return elementObj;
    }

    #getEllipsis(start, last) {
        let li = document.createElement('li');
        let button = document.createElement('button');
        let div = document.createElement('div');
        let icon = document.createElement('i');

        let a;

        li.classList.add('pagination-ellipsis', 'dropdown');
        icon.classList.add('fas', 'fa-ellipsis-h');

        let uid = Math.random().toString(36).substr(2, 9);
        button.classList.add('br-button', 'circle');
        button.setAttribute('type', 'button');
        button.setAttribute('data-toggle', 'dropdown');
        button.setAttribute('aria-label', 'Abrir listagem');
        button.append(icon);

        button.addEventListener('click', function (e) {
            e.stopPropagation();
            if (div.hasAttribute('hidden')) {
                div.removeAttribute('hidden');
            } else {
                div.setAttribute('hidden', 'true');
            }
        });
        div.setAttribute('hidden', 'true');


        div.classList.add('br-list');

        for (let i = start; i < last; i++) {
            a = document.createElement('a');
            a.classList.add('br-item');
            a.innerText = i;
            a.setAttribute('href', 'javascript:void(0)');

            a.addEventListener('click', (e) => {
                e.stopPropagation();
                this.#changePage(i);
                div.setAttribute('hidden', 'true');
            });
            div.append(a);
        }

        li.append(button);
        li.append(div);
        return li;
    }

    #getEllipsisNext() {
        let max = this.#maxNumberPages - 1;
        let left = parseInt(this.#maxNumberPages / 2);
        if (max + this.#currentPage < this.#paginator.last_page - 1) {
            let start = (this.#currentPage + left) + 2;
            let last = this.#paginator.last_page;

            return this.#getEllipsis(start, last);
        }
        return false;
    }

    #getEllipsisPrev() {
        let max = this.#maxNumberPages - 1
        let left = parseInt(this.#maxNumberPages / 2);
        if (this.#currentPage - max > 2) {
            let last = (this.#currentPage - left) - 1;
            let start = 2;

            return this.#getEllipsis(start, last);
        }
        return false;
    }

    draw() {
        let element = this.setDataSelector();
        let ul = document.createElement('ul');
        let li;


        //Seta de voltar
        if (this.#currentPage > 1) {
            ul.append(this.#getElementButtonPrev());
        }
        ul.append(this.#getElementNumber(1, 1 === this.#currentPage));

        //Reticencias
        let ellipsisPrev = this.#getEllipsisPrev();
        if (ellipsisPrev) {
            ul.append(ellipsisPrev);
        }

        //números
        for (let i = 2; i < this.#paginator.last_page; i++) {
            li = this.#getNumberByIndexPage(i);
            if (li) {
                ul.append(li);
            }
        }

        //Reticencias
        let ellipsisNext = this.#getEllipsisNext();
        if (ellipsisNext) {
            ul.append(ellipsisNext);
        }
        ul.append(
            this.#getElementNumber(this.#paginator.last_page, this.#paginator.last_page === this.#currentPage)
        );

        //Botão de avançar
        if (this.#paginator.last_page > this.#currentPage) {
            ul.append(this.#getElementButtonNext());
        }

        element.innerHTML = "";
        if (this.#paginator.total > 0) {

            if(this.#paginator.last_page > 1) {
                element.append(ul);
            }
        } else {
            element.innerHTML = "<h4>Nenhum dado encontrado</h4>";
        }
    }

}