(function(){
    let checkDataTable = ()=>{
        if(typeof DataTable == 'undefined'){
            setTimeout(checkDataTable,500);
        }else{
            $.extend($.fn.dataTable.defaults, {
                language: { url: "/Scripts/dataTables.german.json" }
            });
        }
    }
    checkDataTable();
})()
