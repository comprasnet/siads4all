$.fn.modalGov = function(options){

    let defaults = {
        onOpen: function(modal){},
        onClose: function(modal){},
        autoOpen:true
    };

    let settings = $.extend({}, defaults, options);

    let modal = $(this);
    let fns = {
        modal: modal,
        close: function(){
            modal.hide();
            settings.onClose(modal);
        },
        open: function(){
            modal.show()
            settings.onOpen(modal);
        }
    }
    if(settings.autoOpen){
        fns.open();
    }
    modal.find('[data-dismiss]').click(function(){
        fns.close();
        return false;
    })
    return fns;
}
