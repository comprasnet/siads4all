$(function() {
    // Solicita permissão para enviar notificações
    Notification.requestPermission();

    function loadUnsentPushNotifications() {
        $.ajax({
            url: '/notifications/unsent-push', // Sua rota
            method: 'GET',
            dataType: 'json',
            success: function(response) {

                // Verifica se há notificações
                if (response.notification) {
                    // Adiciona as notificações à lista
                    const notification = new Notification('Siads', {
                        body: response.notification.message,
                    });
                    notification.onclick = () => {
                        window.open(response.notification.link);
                    }
                    loadListNotificationBell();
                }
            }
        });
    }

    function loadListNotificationBell(){
        $.ajax({
            url:'/notification/bell',
            success: function(html){
                $('#notification-menu').html(html)
            }
        })
    }

    // Carrega as notificações não enviadas por push quando a página é carregada
    setInterval(function() {
        loadUnsentPushNotifications();
    }, 10000);
})
