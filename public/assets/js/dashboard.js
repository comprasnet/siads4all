$(document).ready(function () {

    $('.icon-select').select2({
        templateResult: formatIcon,
        templateSelection: formatIcon,
        escapeMarkup: function(markup) { return markup; }
    });

    // Função para exibir os ícones dentro do Select2
    function formatIcon(icon) {
        if (!icon.id) {
            return icon.text;
        }
        var $icon = $('<span><i class="' + $(icon.element).val() + '"></i> ' + icon.text + '</span>');
        return $icon;
    }
    if($.fn.sortable) {
        // Ativa o plugin de arrastar e soltar
        $("#quick-access-cards").sortable({
            update: function (event, ui) {
                let order = $(this).sortable('toArray', {attribute: 'data-id'});
                $.ajax({
                    url: '/update-order',
                    method: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        order: order
                    },
                    success: function (response) {
                        console.log('Ordem atualizada com sucesso!');
                    },
                    error: function (response) {
                        console.log('Erro ao atualizar a ordem.');
                    }
                });
            }
        });

        // Salva os dados do card
        $('body').on('click', '.save-btn', function () {
            let card = $(this).closest('.card');
            let id = card.data('id');
            let title = card.find('.title').val();
            let subtitle = card.find('.subtitle').val();
            let icon = card.find('.icon').val();
            let url = card.find('.url').val();

            $.ajax({
                url: '/update-quick-access',
                method: 'POST',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    id: id,
                    title: title,
                    subtitle: subtitle,
                    icon: icon,
                    url: url
                },
                success: function (response) {
                    console.log('Dados atualizados com sucesso!');
                },
                error: function (response) {
                    console.log('Erro ao atualizar os dados.');
                }
            });
        });

        // Remove o card
        $('body').on('click', '.remove-btn', function () {
            let card = $(this).closest('.card');
            let id = card.data('id');

            if (confirm('Tem certeza que deseja remover este acesso rápido?')) {
                $.ajax({
                    url: '/remove-quick-access',
                    method: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id: id
                    },
                    success: function (response) {
                        card.closest('.col-md-4').remove();
                        console.log('Card removido com sucesso!');
                    },
                    error: function (response) {
                        console.log('Erro ao remover o card.');
                    }
                });
            }
        });

        // Adiciona um novo card
        $('#add-new-card').on('click', function () {
            let options = '';
            for (const [iconClass, iconLabel] of Object.entries(iconsJson)) {
                options += '<option value="' + iconClass + '">' +
                    '<i class="+iconClass+"></i> ' + iconLabel + '</option>';
            }
            $('#quick-access-cards').append(`
            <div class="col-md-4 mb-4 new-card">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" class="form-control mb-2 title" placeholder="Título">
                        </div>
                        <div class="form-group">
                            <label for="subtitle">Subtítulo</label>
                            <input type="text" class="form-control mb-2 subtitle" placeholder="Subtítulo">
                        </div>
                        <div class="form-group">
                            <label for="icon">Ícone</label>
                            <select class="form-control mb-2 icon-select">
                                ` + options + `
                            </select>
                        </div>
                        <input type="hidden" class="url" placeholder="URL">
                        <button class="btn btn-primary save-btn">Salvar</button>
                        <button class="btn btn-default remove-btn">Remover</button>
                    </div>
                </div>
            </div>
        `);
            let addedCard = $('#quick-access-cards .new-card').last();
            addedCard.find('.icon-select').select2({
                templateResult: formatIcon,
                templateSelection: formatIcon,
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });
    }
});
