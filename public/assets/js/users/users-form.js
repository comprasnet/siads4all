let modal;
$(function () {
    //cpf mask
    $('#cpf').mask('999.999.999-99')

    let elementModal = $('#modal-add-unit');

    elementModal.find('select').val('').trigger('change');
    elementModal.find('input[type="checkbox"]').removeAttr('checked');
    elementModal.find('#unit').removeAttr('disabled');

    $('#default_unit').select2Unidade();
    modal = elementModal.modalGov({
        autoOpen: false,
        onOpen: function (modal) {
            $(modal).find('#group').select2();
            $(modal).find('#unit').select2Unidade();
        },
        onClose: function (modal) {
            modal.find('select').val('').trigger('change');
            modal.find('input[type="checkbox"]').removeAttr('checked');
            modal.find('#unit').removeAttr('disabled');
        }
    });

    $('.adicionar-unidades-title button').click(function () {
        modal.open();
    });

    elementModal.find('#group').change(function () {
        loadPermissionsByRoles($(this).val(), elementModal.find('[name="permissions[]"]'));
    })

    $('.add-unidade').click(function () {
        if ($(this).parents('form')[0].reportValidity()) {
            recoverFormModal();
            modal.close();
        }
    });

    createUiAllRecordsLoaded();
});

//insere um novo item
let insertNewUnitItem = (unitName, unitId, roles, permission, unitIdDefault) => {
    let baseUnit = $('#base-unit').html();
    let newUnit = $('<div class="col-md-12 item-unit" data-id="' + unitId + '" >' + baseUnit + '</div>');
    let cardUserUnits = newUnit.find('.card-user-units')


    cardUserUnits.attr('data-permissions', JSON.stringify(permission));
    cardUserUnits.attr('data-roles', JSON.stringify(roles));

    let names = unitName.split(' - ');
    let code = names[0].trim();
    let arrName = [];
    for (let i = 1; i < names.length; i++) {
        arrName.push(names[i]);
    }
    let name = arrName.join(' - ').trim();

    cardUserUnits.find('.unidade-codigo').html(code);
    cardUserUnits.find('.unidade-nome').html(name);

    let inputUnitDefault = newUnit.find('input.unit-default');
    inputUnitDefault.val(unitId);

    if ($('#list-units').find('.item-unit').length <= 0 || unitIdDefault == unitId) {
        inputUnitDefault.attr('checked', true);
    }

    $(newUnit).find('button.remove-item').click(function () {
        if (confirm('Deseja remover a permissão para a ' + unitName)) {
            unitRecords.deleteRecord(unitId);
            newUnit.remove();
        }
    });
    $(newUnit).find('button.change-item').click(function () {
        modal.open();
        let modalElement = $('#modal-add-unit');
        let record = unitRecords.get(unitId);
        if (record) {
            let unitElement = modalElement.find('#unit');
            let groupElement = modalElement.find('#group');
            let permissionsElement = modalElement.find('[name="permissions[]"]');

            let rolesRecord = record.roles;
            let permissionRecord = record.permissions;

            //seta os valores
            unitElement.val(record.id).trigger('change');
            unitElement.attr('disabled', true);

            groupElement.val(rolesRecord).trigger('change');
            permissionsElement.each(function () {
                let val = $(this).val();
                if (permissionRecord.includes(val)) {
                    $(this).attr('checked', true);
                    return;
                }
                $(this).removeAttr('checked');
            })
        }
    });
    if ($('#list-units').find('.item-unit[data-id="' + unitId + '"]').length == 0) {
        $('#list-units').append(newUnit);
    }
}

//recupera os dados do forme para adicionar um novo
let recoverFormModal = () => {
    let checkboxes = document.querySelectorAll('#modal-add-unit [name="permissions[]"]:checked');
    let permissions = Array.from(checkboxes).map(function (checkbox) {
        return checkbox.value;
    });
    $('#modal-add-unit').find('#unit').removeAttr('disabled');
    let roles = $('#modal-add-unit').find('[name="roles[]"]').val();
    let unit = $('#modal-add-unit').find('#unit').val();
    let unitName = $('#modal-add-unit').find('#unit option:selected').text();

    insertNewUnitItem(unitName, unit, roles, permissions);
    unitRecords.addOrChangeRecord(unitName, unit, roles, permissions);

}

let createUiAllRecordsLoaded = () => {
    Object.keys(unitRecords.records).forEach(key => {
        const record = unitRecords.records[key];
        insertNewUnitItem(
            record.nome,
            record.id,
            record.roles,
            record.permissions,
            record.default
        );
    });
}

let loadPermissionsByRoles = (roles, permissionField) => {
    $.ajax({
        url: '/unit/permission-by-roles',
        data: { roles: roles },
        success: function (data) {
            permissionField.each(function () {
                let val = parseInt($(this).val());
                if (data.includes(val)) {
                    $(this).attr("checked", true);
                }
            });
        }
    })
}

//manipula os dados do front end
let unitRecords = {
    records: {},
    loadRoles: function (roles) {
        let newRoles = [];
        roles.forEach((role) => {
            newRoles.push(role.role_id.toString());
        })
        return newRoles;
    },
    loadPermissions: function (permissions) {
        let newPermissions = [];
        permissions.forEach((permission) => {
            newPermissions.push(permission.permission_id.toString());
        })
        return newPermissions;
    },
    load: function (json) {
        json.forEach((element) => {
            element.nome = element.codigo + ' - ' + element.nome;
            element.roles = this.loadRoles(element.roles);
            element.default = element.default_unit;
            element.permissions = this.loadPermissions(element.permissions);
            this.records[element.id] = element;
        })
    },
    addOrChangeRecord: function (unitName, unit, roles, permissions) {
        this.records[unit] = {
            id: unit,
            nome: unitName,
            roles: roles,
            permissions: permissions,
            default: null,
        }
        this.saveInputs();
    },
    deleteRecord: function (unit) {
        this.records[unit] = false;
        this.saveInputs();
    },
    get: function (unitId) {
        return this.records[unitId];
    },
    getRecordsString: function () {
        return JSON.stringify(this.records);
    },
    saveInputs: function () {
        $('#unidades-permitidas').val(this.getRecordsString());
    }
}
