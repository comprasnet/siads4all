let remoteItemTable = function(id,button){
    let tableId = $(button).parents('table').attr("id");
    if(confirm('Deseja remover esse registro?')){
        $.ajax({
            url:$(button).data('url'),
            type:'DELETE',
            success: function(){
                LaravelDataTables[tableId].ajax.reload();
            }
        })
    }
}
