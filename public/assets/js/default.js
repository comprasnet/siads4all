$(function(){
    $('[data-toggle="tooltip"]').each(function () {
        const texttooltip = $(this).attr('data-title');
        const config = {
            activator: this,
            place: 'top',
            textTooltip: texttooltip,
        }

        new core.Tooltip(config)
    })
})
setInterval(function () {
    $('.loading-site.general').height(window.innerHeight);
}, 100);

$.loader = {
    start: () => {
        $('.loading-site.general').css({
            'display': 'flex'
        })
    },
    stop: () => {
        $('.loading-site.general').hide();
    }
}
$.table = {

    getCollumns: function (table) {
        let cols = [];
        let tableCols = table.settings().init().columns;
        for (let i = 0; i < tableCols.length; i++) {
            cols.push(tableCols[i].data);
        }
        return cols;
    },

    getVisibleCols: function (table) {
        let cols = this.getCollumns(table);
        let col;
        let visibleColumns = table.columns().visible().toArray();
        let visible = [];
        for (let i = 0; i < cols.length; i++) {
            col = cols[i];
            if (visibleColumns[i]) {
                visible.push(col);
            }
        }
        return visible
    },
    paramsAndVisibleColumns: function (table) {
        let params = table.ajax.params()
        params.visible_cols = this.getVisibleCols(table);
        return params;
    },


}
$.loadingElement = {
    start: (element) => {
        if (!element.dataset.loader) {
            const loadingSiteDiv = document.createElement('div');
            loadingSiteDiv.style.display = 'flex';
            loadingSiteDiv.className = 'loading-site loader-element';
            loadingSiteDiv.id = 'id-' + Math.random().toString(36).substr(2, 9) + '-' + Date.now().toString(36);;

            const loaderDiv = document.createElement('div');
            loaderDiv.className = 'loader';

            const brLoadingDiv = document.createElement('div');
            brLoadingDiv.className = 'br-loading medium';
            brLoadingDiv.setAttribute('role', 'progressbar');
            brLoadingDiv.setAttribute('aria-label', 'carregando exemplo medium exemplo');


            loaderDiv.appendChild(brLoadingDiv);
            loadingSiteDiv.appendChild(loaderDiv);
            document.body.appendChild(loadingSiteDiv);

            const adjustLoadingSitePosition = () => {
                const dataTableRect = element.getBoundingClientRect();
                loadingSiteDiv.style.top = dataTableRect.top + window.scrollY + 'px';
                loadingSiteDiv.style.left = dataTableRect.left + 'px';
                loadingSiteDiv.style.width = dataTableRect.width + 'px';
                loadingSiteDiv.style.height = dataTableRect.height + 'px';
            };

            adjustLoadingSitePosition();
            window.addEventListener('resize', adjustLoadingSitePosition);
            window.addEventListener('scroll', adjustLoadingSitePosition);
            element.loadingPositionHandler = adjustLoadingSitePosition;
            element.dataset.loader = loadingSiteDiv.id;
        }
    },
    stop: (element) => {
        if (element.dataset.loader) {
            const loadingSiteDiv = document.getElementById(element.dataset.loader);
            if (loadingSiteDiv) {
                loadingSiteDiv.parentElement.removeChild(loadingSiteDiv);
                window.removeEventListener('resize', element.loadingResizeHandler);
            }
            delete element.dataset.loader;
        }
    }
}
$.fn.select2Unidade = function (opt) {
    let options = $.extend({
        allowClear:true
    },opt)
    $(this).select2({
        language: 'pt-BR',
        ajax: {
            url: '/unit/by-user',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term // termo de busca
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        placeholder: 'Procurar Unidade',
        allowClear:options.allowClear
    });
}

$.fn.select2Uorg = function (options) {

    let opt = $.extend({
        ug: '.selector-unidades'
    }, options)

    $('.selector-uorg').select2({
        language: 'pt-BR',
        ajax: {
            url: '/unit/uorgs',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term,
                    ug: $(opt.ug).val()
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        placeholder: 'Procurar Unidade',
    });
}

$.dinamicModal = (title, htmlContent, buttons) => {
    const modalId = `modal-${Math.random().toString(36).substr(2, 9)}`;

    const modal = document.createElement('div');
    modal.className = 'br-scrim-util foco';
    modal.id = modalId;
    modal.setAttribute('data-scrim', 'true');

    const modalInner = document.createElement('div');
    modalInner.className = 'br-modal';
    modalInner.setAttribute('aria-labelledby', 'titulomodalexemplo');

    const header = document.createElement('div');
    header.className = 'br-modal-header';
    header.id = 'titulomodalexemplo';
    header.innerHTML = title;

    const body = document.createElement('div');
    body.className = 'br-modal-body';
    body.innerHTML = htmlContent;

    const footer = document.createElement('div');
    footer.className = 'br-modal-footer justify-content-center';

    buttons.forEach(button => {
        const btn = document.createElement('button');
        btn.className = `br-button ${button.type} mt-3 mt-sm-0 ml-sm-3`;
        btn.type = 'button';
        btn.innerHTML = button.name;
        if (button.close) {
            btn.addEventListener('click', function (e) {
                if (button.action && typeof button.action === 'function') {
                    button.action(e);
                }
                modal.remove();
            });
        } else {
            if (button.action && typeof button.action === 'function') {
                btn.addEventListener('click', button.action);
            }
        }
        footer.appendChild(btn);
    });

    modalInner.appendChild(header);
    modalInner.appendChild(body);
    modalInner.appendChild(footer);
    modal.appendChild(modalInner);

    document.body.appendChild(modal);

    const activateModalButton = document.createElement('button');
    activateModalButton.className = 'br-button primary';
    activateModalButton.type = 'button';
    activateModalButton.id = 'buttonactivatemodal';
    activateModalButton.innerHTML = 'Testar o Modal com o Scrim';
    activateModalButton.addEventListener('click', () => {
        modal.style.display = 'block';
    });

    const activateModalDiv = document.createElement('div');
    activateModalDiv.className = 'scrimutilexamplemodal';
    activateModalDiv.appendChild(activateModalButton);

    document.body.appendChild(activateModalDiv);
    modal.style.display = 'block';

    return modal;
}

$.prompt = (title, message) => {
    return new Promise((resolve, reject) => {
        const modal = $.dinamicModal(title, `<p>${message}</p><input type="text" id="modalInput" class="br-input  form-control">`, [
            { name: 'Cancelar', type: 'secondary', action: () => reject(null), close: true },
            {
                name: 'OK', type: 'primary', action: () => {
                    const userInput = document.getElementById('modalInput').value;
                    resolve(userInput);
                }, close: true
            }
        ]);

        // Mostrar o modal
        modal.style.display = 'block';
    });
}

$.promptSelect = (title, message,action = ()=>{}) => {
    return new Promise((resolve, reject) => {
        const modal = $.dinamicModal(title, `<p>${message}</p><select name="modalInput" id="modalInput"></select>`, [
            { name: 'Cancelar', type: 'secondary', action: () => reject(null), close: true },
            {
                name: 'OK', type: 'primary', action: () => {
                    const userInput = document.getElementById('modalInput').value;
                    resolve(userInput);
                }, close: true
            }
        ]);

        action(modal);
        // Mostrar o modal
        modal.style.display = 'block';
    });
}

$.alert = (message, title = '') => {
    return new Promise((resolve) => {
        const modal = $.dinamicModal(title, `<p>${message}</p>`, [
            { name: 'OK', type: 'primary', action: () => resolve(), close: true }
        ]);

        // Mostrar o modal
        modal.style.display = 'block';
    });
}

let badgeTypes = {
    warning:'warning',
    info:'info',
    danger:'danger',
    success:'success',
}
let addBadge = function(element,number, type=badgeTypes.info){
    let span = '<span class="badge badge-'+type+' badge-circle">'+number+'</span>';
    $(element).append($(span));
}
$.fn.numeric = function() {
    return this.each(function() {
        $(this).on('keypress', function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode < 48 || charCode > 57) {
                e.preventDefault();
            }
        });
    });
};
$(function(){
    $('input.numeric').numeric();
})

function btnVisualizar(url,editUrl){
    $.loader.start()
    $.get(url,function(html){
        const modal = $.dinamicModal('', html, [
            { name: 'Cancelar', type: 'secondary', action: () => {}, close: true },
            {
                name: 'Editar', type: 'primary', action: () => {
                    window.location = editUrl;
                }, close: true
            }
        ]);
        $(modal).find(".br-modal").css({
            'width':'800px',
            'max-width':'800px',
            'height':'500px',
            'max-height':'500px',
        });
        $.loader.stop();
    });

}
