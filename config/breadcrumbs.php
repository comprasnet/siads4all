<?php
return [
    'names'=>[
        'users'=>[
            'index'=>'Usuários'
        ],
        'request-permission'=>[
            'index'=>'Permissões de Acesso a Unidade'
        ],
        'patrimony'=>[
            'index'=>'Patrimônio'
        ],
        'files'=>[
            'index'=>'Arquivos gerados de relatórios'
        ],
        'orgaos'=>[
            'index'=>'Orgãos'
        ],
    ]
];
