#!/bin/sh
rm -rf ./storage/app/public

php -f /var/www/html/siads4all/artisan key:generate
php -f /var/www/html/siads4all/artisan migrate --force
php -f /var/www/html/siads4all/artisan storage:link
php -f /var/www/html/siads4all/artisan optimize:clear
php -f /var/www/html/siads4all/artisan cache:clear
php -f /var/www/html/siads4all/artisan config:cache

sudo service cron start
sudo service supervisor start

/usr/sbin/apache2ctl -D FOREGROUND
