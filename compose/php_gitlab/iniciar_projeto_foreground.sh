#!/bin/sh

chmod -R ugo+rw /var/www/html/siads4all/storage
chmod -R ugo+rw /var/www/html/siads4all/bootstrap/cache
chown -R www-data:www-data /var/www/html/siads4all/

#php -f /var/www/html/siads4all/artisan key:generate
php -f /var/www/html/siads4all/artisan migrate --force
#php -f /var/www/html/siads4all/artisan l5-swagger:generate
php -f /var/www/html/siads4all/artisan storage:link
php -f /var/www/html/siads4all/artisan optimize:clear
php -f /var/www/html/siads4all/artisan cache:clear
php -f /var/www/html/siads4all/artisan config:cache
# php -f /var/www/html/siads4all/artisan jwt:secret --force

service cron start
service supervisor start

/usr/sbin/apache2ctl -D FOREGROUND
